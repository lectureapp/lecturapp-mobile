export const mock_response_api = {
  data: {
    data: [
      {
        content: [
          'Se había enamorado una gata de un hermoso joven, y rogó a Afrodita que la hiciera mujer. La diosa,  compadecida de su deseo, la transformó en una bella doncella, y entonces el joven, prendado de ella, la invitó a su casa.',
          'https://sincopyright.com/wp-content/uploads/2016/10/gata-y-raton.gif',
          'Estando ambos descansando en la alcoba nupcial, quiso saber Afrodita si al cambiar de ser a la gata había mudado también de carácter, por lo que soltó un ratón en el centro de la alcoba. Olvidándose la gata de su condición presente, se levantó del lecho y persiguió al ratón para comérselo. Entonces la diosa, indignada, la volvió a su original estado.',
          'Moraleja: El cambio de estado de una persona, no la hace cambiar de sus instintos.',
        ],
        _id: '6070ad5d39898a004dd90787',
        title: 'Afrodita y la gata',
        cover_page: 'https://imagen_portada.png',
        difficulty: 'facil',
        gender: 'drama',
        author: 'chabelos',
        questions: [
          {
            alternative: [
              {
                _id: '6070ad5d39898a004dd90789',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6070ad5d39898a004dd9078a',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6070ad5d39898a004dd9078b',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6070ad5d39898a004dd9078c',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6070ad5d39898a004dd90788',
            question_id: 1,
            question: '¿Quién se había enamorado?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6070ad5d39898a004dd9078e',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6070ad5d39898a004dd9078f',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6070ad5d39898a004dd90790',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6070ad5d39898a004dd90791',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6070ad5d39898a004dd9078d',
            question_id: 2,
            question: '¿Quién se transformó en persona?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6070ad5d39898a004dd90793',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6070ad5d39898a004dd90794',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6070ad5d39898a004dd90795',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6070ad5d39898a004dd90796',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6070ad5d39898a004dd90792',
            question_id: 3,
            question: '¿Quién se indignó?',
            correct_answer: 0,
          },
        ],
        createdAt: '2021-04-09T19:39:09.899Z',
        updatedAt: '2021-04-09T19:39:09.899Z',
        __v: 0,
        times_read: false,
        added_as_favorite: true,
      },
      {
        content: [
          'Se había enamorado una gata de un hermoso joven, y rogó a Afrodita que la hiciera mujer. La diosa,  compadecida de su deseo, la transformó en una bella doncella, y entonces el joven, prendado de ella, la invitó a su casa.',
          'https://sincopyright.com/wp-content/uploads/2016/10/gata-y-raton.gif',
          'Estando ambos descansando en la alcoba nupcial, quiso saber Afrodita si al cambiar de ser a la gata había mudado también de carácter, por lo que soltó un ratón en el centro de la alcoba. Olvidándose la gata de su condición presente, se levantó del lecho y persiguió al ratón para comérselo. Entonces la diosa, indignada, la volvió a su original estado.',
          'Moraleja: El cambio de estado de una persona, no la hace cambiar de sus instintos.',
        ],
        _id: '6085fd0bbcfb53002c3a6e96',
        title: 'Afrodita y la gata2',
        cover_page: 'https://imagen_portada2.png',
        difficulty: 'facil',
        gender: 'drama',
        author: 'chabelos',
        questions: [
          {
            alternative: [
              {
                _id: '6085fd0bbcfb53002c3a6e98',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd0bbcfb53002c3a6e99',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd0bbcfb53002c3a6e9a',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd0bbcfb53002c3a6e9b',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd0bbcfb53002c3a6e97',
            question_id: 1,
            question: '¿Quién se había enamorado?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6085fd0bbcfb53002c3a6e9d',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd0bbcfb53002c3a6e9e',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd0bbcfb53002c3a6e9f',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd0bbcfb53002c3a6ea0',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd0bbcfb53002c3a6e9c',
            question_id: 2,
            question: '¿Quién se transformó en persona?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6085fd0bbcfb53002c3a6ea2',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd0bbcfb53002c3a6ea3',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd0bbcfb53002c3a6ea4',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd0bbcfb53002c3a6ea5',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd0bbcfb53002c3a6ea1',
            question_id: 3,
            question: '¿Quién se indignó?',
            correct_answer: 0,
          },
        ],
        createdAt: '2021-04-25T23:36:43.362Z',
        updatedAt: '2021-04-25T23:36:43.362Z',
        __v: 0,
        times_read: true,
        added_as_favorite: false,
      },
      {
        content: [
          'Se había enamorado una gata de un hermoso joven, y rogó a Afrodita que la hiciera mujer. La diosa,  compadecida de su deseo, la transformó en una bella doncella, y entonces el joven, prendado de ella, la invitó a su casa.',
          'https://sincopyright.com/wp-content/uploads/2016/10/gata-y-raton.gif',
          'Estando ambos descansando en la alcoba nupcial, quiso saber Afrodita si al cambiar de ser a la gata había mudado también de carácter, por lo que soltó un ratón en el centro de la alcoba. Olvidándose la gata de su condición presente, se levantó del lecho y persiguió al ratón para comérselo. Entonces la diosa, indignada, la volvió a su original estado.',
          'Moraleja: El cambio de estado de una persona, no la hace cambiar de sus instintos.',
        ],
        _id: '6085fd13bcfb53002c3a6ea6',
        title: 'Afrodita y la gata3',
        cover_page: 'https://imagen_portada3.png',
        difficulty: 'facil',
        gender: 'drama',
        author: 'chabelos',
        questions: [
          {
            alternative: [
              {
                _id: '6085fd13bcfb53002c3a6ea8',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd13bcfb53002c3a6ea9',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd13bcfb53002c3a6eaa',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd13bcfb53002c3a6eab',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd13bcfb53002c3a6ea7',
            question_id: 1,
            question: '¿Quién se había enamorado?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6085fd13bcfb53002c3a6ead',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd13bcfb53002c3a6eae',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd13bcfb53002c3a6eaf',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd13bcfb53002c3a6eb0',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd13bcfb53002c3a6eac',
            question_id: 2,
            question: '¿Quién se transformó en persona?',
            correct_answer: 1,
          },
          {
            alternative: [
              {
                _id: '6085fd13bcfb53002c3a6eb2',
                label: 'Afrodita',
                value: 0,
              },
              {
                _id: '6085fd13bcfb53002c3a6eb3',
                label: 'Una gata',
                value: 1,
              },
              {
                _id: '6085fd13bcfb53002c3a6eb4',
                label: 'Un hermoso joven',
                value: 2,
              },
              {
                _id: '6085fd13bcfb53002c3a6eb5',
                label: 'Un ratón',
                value: 3,
              },
            ],
            _id: '6085fd13bcfb53002c3a6eb1',
            question_id: 3,
            question: '¿Quién se indignó?',
            correct_answer: 0,
          },
        ],
        createdAt: '2021-04-25T23:36:51.959Z',
        updatedAt: '2021-04-25T23:36:51.959Z',
        __v: 0,
        times_read: false,
        added_as_favorite: false,
      },
    ],
    currentPage: 1,
    lastPage: 1,
    perPage: 4,
  },
};
