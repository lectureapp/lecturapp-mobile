export const buy_video_time_response = {
  data: {
    message: 'Video actualizado y Monedas restadas correctamente',
    data: [
      {
        state: 'ACTIVE',
        _id: '60d7d00d2b112f0020ecf836',
        _videoId: '60d68fbe5d9ebd002237a523',
        _url: 'http://urlvideo5',
        date: '2021-07-03T01:10:37.802Z',
        time_left: '164 horas',
        createdAt: '2021-06-27T01:10:37.803Z',
        updatedAt: '2021-06-27T05:06:59.568Z',
        __v: 0,
      },
      {
        state: 'ACTIVE',
        _id: '60d7d5f72b112f0020ecf8f2',
        _videoId: '6085f966bcfb53002c3a6e7b',
        _url: 'https://youtu.be/J2WCaJjtbsY',
        date: '2021-06-28T01:35:51.597Z',
        time_left: '44 horas',
        createdAt: '2021-06-27T01:35:51.600Z',
        updatedAt: '2021-06-27T05:06:59.568Z',
        __v: 0,
      },
    ],
    wallet: {
      total_coins: 62,
      _id: '60d7cffb2b112f0020ecf834',
      _user: '60d7cffb2b112f0020ecf832',
      createdAt: '2021-06-27T01:10:19.526Z',
      updatedAt: '2021-06-27T05:06:59.547Z',
      __v: 0,
    },
  },
};
