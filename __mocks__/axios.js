// ----------- Probando mock de video de redux con testing -----------
/* export default {
  get: jest.fn(() => Promise.resolve({data: {}})),
  post: jest.fn(() => Promise.resolve({data: {}})),
}; */
// ----------- FIN de Probando mock de video de redux con testing -----------

// ----------- respuesta de un foro -----------
const mockAxios = jest.genMockFromModule('axios');
mockAxios.create = jest.fn(() => mockAxios);

export default mockAxios;
// ----------- FIN respuesta de un foro -----------

// ----------- respuesta del mismo foro -----------
// NO FUNCIONA BIEN, bota error: axios.create is not a function
/* export default jest.mock('axios', () => {
  return {
    create: jest.fn(() => axios),
    post: jest.fn(() => Promise.resolve()),
  };
}); */
// ----------- FIN respuesta del mismo foro -----------
