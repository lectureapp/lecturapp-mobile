import React from 'react';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import {Provider} from 'react-redux';
import {NavigationContainer, NavigationContext} from '@react-navigation/native';

const mockStore = configureStore([thunk]);
export const makeMockStore = (state = {}) => {
  return mockStore({
    ...state,
  });
};

export const NavAndReduxWrapper = ({store, children}: any) => (
  <NavigationContainer>
    <Provider store={store}>{children}</Provider>
  </NavigationContainer>
);

// ---------------------------------------------------
// import React from 'react';
// import {NavigationContainer} from '@react-navigation/native';
// import {createStackNavigator} from '@react-navigation/stack';

// const Stack = createStackNavigator();
// const MockedNavigator = ({component, params = {}}) => {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         <Stack.Screen
//           name="MockedScreen"
//           component={component}
//           initialParams={params}
//         />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// };
// ---------------------------------------------------
