import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
// import * as actions from '../../../src/framework/redux/actions/AuthActions';
import * as AuthActions from '../../src/framework/redux/actions/AuthActions';
import * as AuthTypes from '../../src/framework/redux/types/AuthTypes';

import * as walletTypes from '../../src/framework/redux/types/walletTypes';

import * as availableVideosActions from '../../src/framework/redux/actions/availableVideosActions';
import * as availableVideosTypes from '../../src/framework/redux/types/availableVideosTypes';

import * as clothesActions from '../../src/framework/redux/actions/clothesActions';
import * as clothesTypes from '../../src/framework/redux/types/clothesTypes';

import fetchMock from 'fetch-mock';
import expect from 'expect'; // You can use any testing library

import API from '../../src/framework/axios/axios';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

let access_token: string = '';

describe('async actions', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  // it('real login with user1@gmail.com | 1234567', () => {
  //   // const store = mockStore({ todos: [] })
  //   const store = mockStore({});

  //   const props_for_login = {
  //     username: 'user1@gmail.com',
  //     password: '1234567',
  //   };

  //   // Expected actions in order:
  //   const UPDATE_USER_CLOTHES = expect.objectContaining({
  //     payload: expect.any(Array),
  //   });
  //   const USE_CLOTHES = expect.objectContaining({payload: expect.any(String)});
  //   const UPDATE_COINS_FROM_LOGIN = expect.objectContaining({
  //     payload: expect.any(Number),
  //   });
  //   const LOGIN_SUCCEEDED = expect.objectContaining({
  //     payload: expect.objectContaining({
  //       message: expect.any(String),
  //       avatar: expect.objectContaining({
  //         avatar_sets: expect.any(Array),
  //         current_style: expect.any(String),
  //       }),
  //       wallet: expect.objectContaining({
  //         total_coins: expect.any(Number),
  //       }),
  //       data: expect.objectContaining({
  //         user: expect.objectContaining({
  //           name: expect.any(String),
  //           email: expect.any(String),
  //           access_token: expect.any(String),
  //         }),
  //       }),
  //     }),
  //   });

  //   return store.dispatch(AuthActions.loginUser(props_for_login)).then(() => {
  //     // return of async actions

  //     access_token = store.getActions()[4].payload.data.user.access_token;

  //     // NOTE: store.getActions is an array with 5 dispatchs inside.
  //     expect(store.getActions()[0]).toBeDefined(); // LOGIN_LOADING
  //     expect(store.getActions()[1]).toEqual(UPDATE_USER_CLOTHES); // UPDATE_USER_CLOTHES
  //     expect(store.getActions()[2]).toEqual(USE_CLOTHES); // USE_CLOTHES
  //     expect(store.getActions()[3]).toEqual(UPDATE_COINS_FROM_LOGIN); // UPDATE_COINS_FROM_LOGIN
  //     expect(store.getActions()[4]).toEqual(LOGIN_SUCCEEDED); // LOGIN_SUCCEEDED
  //   });
  // });

  // TODO:  Falta implementar el registro de usuario, para esto debemos tener un endpoint
  // para borrar cuentas tmb.

  // NOTA: este test no esta mockeando las peticiones,
  // realiza una consulta real al backend.
  // Por lo que solo resulta correcto si el backend está prendido.
  // it('mocked login test', () => {
  //   const props_for_login = {
  //     username: 'user1@gmail.com',
  //     password: '1234567',
  //   };

  //   fetchMock.getOnce('/auth/login', {
  //     body: {todos: ['do something']},
  //     headers: {
  //       'content-type': 'application/json',
  //       username: 'user1@gmail.com',
  //       password: '1234567',
  //     },
  //   });

  //   const expectedActions = [
  //     {type: AuthTypes.LOGIN_LOADING},
  //     {type: clothesTypes.UPDATE_USER_CLOTHES, payload: ['DEFAULT', 'COWBOY']},
  //     {type: clothesTypes.USE_CLOTHES, payload: 'COWBOY'},
  //     {type: walletTypes.UPDATE_COINS_FROM_LOGIN, payload: 3},
  //     {
  //       type: AuthTypes.LOGIN_SUCCEEDED,
  //       payload: expect.objectContaining({
  //         message: expect.any(String),
  //         avatar: expect.objectContaining({
  //           avatar_sets: expect.any(Array),
  //           current_style: expect.any(String),
  //         }),
  //         wallet: expect.objectContaining({
  //           total_coins: expect.any(Number),
  //         }),
  //         data: expect.objectContaining({
  //           user: expect.objectContaining({
  //             name: expect.any(String),
  //             email: expect.any(String),
  //             access_token: expect.any(String),
  //           }),
  //         }),
  //       }),
  //     },
  //   ];

  //   const AuthMockstore = mockStore({
  //     account_name: '',
  //     email: '',
  //     session_token: '',

  //     registration_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  //     registration_message: '', // message of status response

  //     login_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  //     login_message: '', // message of status response
  //   });

  //   return AuthMockstore.dispatch(AuthActions.loginUser(props_for_login)).then(
  //     () => {
  //       // return of async actions
  //       expect(AuthMockstore.getActions()).toEqual(expectedActions);
  //     },
  //   );
  // });

  // NOTA:la libreria:  axios mock adapter no funciona bien, creo que por que en su estructura
  // no permite interceptar peticiones de un action creator.
  // Pasaré a probar la libreriá moxios.
  // it('axios mock', () => {
  //   const props_for_login = {
  //     username: 'user1@gmail.com',
  //     password: '1234567',
  //   };

  //   // trying axios mock:
  //   // ---------------------------------------------------
  //   // var axios = require('axios');
  //   var MockAdapter = require('axios-mock-adapter');

  //   // var mock = new MockAdapter(axios);
  //   var mock = new MockAdapter(API);

  //   mock.onGet('/auth/login').reply(200, {
  //     users: [{id: 1, name: 'jon'}],
  //   });
  //   // ---------------------------------------------------

  //   const AuthMockstore = mockStore({
  //     account_name: '',
  //     email: '',
  //     session_token: '',

  //     registration_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  //     registration_message: '', // message of status response

  //     login_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  //     login_message: '', // message of status response
  //   });

  //   return AuthMockstore.dispatch(AuthActions.loginUser(props_for_login)).then(
  //     () => {
  //       // return of async actions
  //       // expect(AuthMockstore.getActions()).toEqual(expectedActions);
  //       console.log(AuthMockstore.getActions());
  //     },
  //   );
  // });
});
