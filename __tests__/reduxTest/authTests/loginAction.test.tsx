// PROBANDO MOXIOS:
import {makeMockStore} from '../../../__mocks__/utils';

import {
  loginUser,
  registerUser,
} from '../../../src/framework/redux/actions/AuthActions';
import * as AuthTypes from '../../../src/framework/redux/types/AuthTypes';
import * as clothesTypes from '../../../src/framework/redux/types/clothesTypes';
import * as walletTypes from '../../../src/framework/redux/types/walletTypes';

import mockAxios from 'axios';

describe('login thunks tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  test('CORRECT loginUser thunk', async () => {
    // mock store:
    const store = makeMockStore({});
    const mocked_api_response = {
      data: {
        message: 'Login exitoso',
        avatar: {
          avatar_name: 'Zorro',
          avatar_sets: ['DEFAULT', 'COWBOY'],
          current_style: 'COWBOY',
          _id: '609d63dced5fa100218fd83c',
          _user: '609d63dced5fa100218fd839',
          createdAt: '2021-05-13T17:37:32.989Z',
          updatedAt: '2021-05-13T22:11:35.055Z',
          __v: 1,
        },
        wallet: {
          total_coins: 3,
          _id: '609d63dced5fa100218fd83b',
          _user: '609d63dced5fa100218fd839',
          createdAt: '2021-05-13T17:37:32.985Z',
          updatedAt: '2021-05-13T17:38:17.277Z',
          __v: 0,
        },
        data: {
          user: {
            name: 'user1',
            email: 'user1@gmail.com',
            access_token: '123',
          },
        },
      },
    };
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve(mocked_api_response),
    );

    const props_for_login = {
      username: 'user1@gmail.com',
      password: '1234567',
    };

    await store.dispatch(loginUser(props_for_login));
    const triggered_actions = store.getActions();

    const expectedActions = [
      {type: AuthTypes.LOGIN_LOADING},
      {type: clothesTypes.UPDATE_USER_CLOTHES, payload: ['DEFAULT', 'COWBOY']},
      {type: clothesTypes.USE_CLOTHES, payload: 'COWBOY'},
      {type: walletTypes.UPDATE_COINS_FROM_LOGIN, payload: 3},
      {
        type: AuthTypes.LOGIN_SUCCEEDED,
        payload: {
          message: 'Login exitoso',
          avatar: {
            avatar_name: 'Zorro',
            avatar_sets: ['DEFAULT', 'COWBOY'],
            current_style: 'COWBOY',
            _id: '609d63dced5fa100218fd83c',
            _user: '609d63dced5fa100218fd839',
            createdAt: '2021-05-13T17:37:32.989Z',
            updatedAt: '2021-05-13T22:11:35.055Z',
            __v: 1,
          },
          wallet: {
            total_coins: 3,
            _id: '609d63dced5fa100218fd83b',
            _user: '609d63dced5fa100218fd839',
            createdAt: '2021-05-13T17:37:32.985Z',
            updatedAt: '2021-05-13T17:38:17.277Z',
            __v: 0,
          },
          data: {
            user: {
              name: 'user1',
              email: 'user1@gmail.com',
              access_token: '123',
            },
          },
        },
      },
    ];
    expect(triggered_actions).toEqual(expectedActions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test('FAILED loginUser thunk', async () => {
    // REAL response:
    // {
    //     "statusCode": 401,
    //     "message": "Unauthorized"
    // }
    const mocked_api_response = {
      statusCode: 401,
      message: 'Unauthorized',
    };

    // mock store:
    const mock_store = makeMockStore({});

    mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(mocked_api_response),
    );

    const props_for_login = {
      username: 'usuarioInexistente@gmail.com',
      password: '1234567',
    };

    await mock_store.dispatch(loginUser(props_for_login));
    const triggered_actions = mock_store.getActions();

    const expectedActions = [
      {type: 'auth/login_loading'},
      {type: 'auth/login_failed', payload: 'Los datos son incorrectos.'},
    ];
    expect(triggered_actions).toEqual(expectedActions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });
});
