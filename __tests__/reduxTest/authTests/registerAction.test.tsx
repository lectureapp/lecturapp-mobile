// PROBANDO MOXIOS:
import {makeMockStore} from '../../../__mocks__/utils';

import {
  loginUser,
  registerUser,
} from '../../../src/framework/redux/actions/AuthActions';
import * as AuthTypes from '../../../src/framework/redux/types/AuthTypes';
import * as clothesTypes from '../../../src/framework/redux/types/clothesTypes';
import * as walletTypes from '../../../src/framework/redux/types/walletTypes';

import mockAxios from 'axios';

describe('registers thunks tests', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('correct registerUser thunk', async () => {
    const mock_store = makeMockStore({});

    const mocked_api_response = {
      name: 'user1',
      email: 'user1@gmail.com',
      access_token: '123',
    };

    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve(mocked_api_response),
    );

    const props_for_register: Object = {
      name: 'user1',
      email: 'user1@gmail.com',
      password: '1234567',
    };
    await mock_store.dispatch(registerUser(props_for_register));
    const triggered_actions = mock_store.getActions();
    // console.log(triggered_actions);

    const expected_actions = [
      {type: AuthTypes.REGISTER_LOADING},
      {
        type: AuthTypes.REGISTER_SUCCEEDED,
        payload: {name: 'user1', email: 'user1@gmail.com', token: '123'},
      },
      {type: clothesTypes.UPDATE_USER_CLOTHES, payload: []},

      {type: clothesTypes.USE_CLOTHES, payload: ''},
      {type: walletTypes.UPDATE_COINS_FROM_LOGIN, payload: 0},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test('failed registerUser thunk: email field must be an email', async () => {
    // REAL server response
    //{
    //     "statusCode": 400,
    //     "message": [
    //         "email must be an email"
    //     ],
    //     "error": "Bad Request"
    // }

    // mock store:
    const mock_store = makeMockStore({});

    const mocked_api_response = {
      statusCode: 400,
      message: ['email must be an email'],
      error: 'Bad Request',
    };

    mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(mocked_api_response),
    );

    const props_for_register: Object = {
      name: 'user1',
      email: 'user1@',
      password: '1234567',
    };
    await mock_store.dispatch(registerUser(props_for_register));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: AuthTypes.REGISTER_LOADING},
      {
        type: AuthTypes.REGISTER_FAILED,
        payload: ['email must be an email'],
      },
    ];
    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test('failed registerUser thunk:  Email arleady exists.', async () => {
    // mock store:
    const mock_store = makeMockStore({});

    const mocked_api_response: Object = {
      status: 401,
      message: 'Email already exists.',
    };

    mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(mocked_api_response),
    );
    const props_for_register: Object = {
      name: 'user1',
      email: 'user1@gmail.com',
      password: '1234567',
    };
    await mock_store.dispatch(registerUser(props_for_register));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: AuthTypes.REGISTER_LOADING},
      {type: AuthTypes.REGISTER_FAILED, payload: 'Email already exists.'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  // TODO:
  // - cuando se registra un usuario con contraseña de 1 dígito el servidor lo registra correctamente. Debería haber
  // un mínimo de caracteres requeridos.
  // - cuando no se pone algún campo al registrar el server bota error: '500 internal server error'
  // - Debido a esto no creé los test de estos casos.
});
