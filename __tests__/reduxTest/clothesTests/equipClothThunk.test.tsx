import {makeMockStore} from '../../../__mocks__/utils';
import mockAxios from 'axios';
// ---------------------------------------------

import {equipCloth} from '../../../src/framework/redux/actions/clothesActions';

describe('equipCloth thunk', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT equipCloth thunk', async () => {
    const mock_store = makeMockStore({});
    mockAxios.patch.mockImplementationOnce(() =>
      Promise.resolve({
        data: {
          avatar_name: 'Zorro',
          avatar_sets: ['DEFAULT', 'TUXEDO', 'COWBOY'],
          current_style: 'TUXEDO',
          _id: '60dd18f88765370020405ef4',
          _user: '60dd18f88765370020405ef1',
          createdAt: '2021-07-01T01:23:04.563Z',
          updatedAt: '2021-07-01T01:48:12.887Z',
          __v: 2,
        },
      }),
    );

    const session_token: string = '123';
    const outfit_id: string = '123';

    await mock_store.dispatch(equipCloth(outfit_id, session_token));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: 'clothes/use_clothes_loading'},
      {type: 'clothes/use_clothes_succeeded', payload: 'TUXEDO'},
      {type: 'clothes/use_clothes_idle'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.patch).toHaveBeenCalledTimes(1);
  });

  test('FAILED equipCloth thunk', async () => {
    const mock_store = makeMockStore({});

    const mocked_response_api: Object = {
      response: {
        statusCode: 401,
        message: 'Unauthorized',
      },
      status: 401,
      message: 'Unauthorized',
    };
    mockAxios.patch.mockImplementationOnce(() =>
      Promise.reject(mocked_response_api),
    );

    const unauthorized_session_token: string = 'wrong_token';
    const outfit_id: string = '123';

    await mock_store.dispatch(
      equipCloth(outfit_id, unauthorized_session_token),
    );
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: 'clothes/use_clothes_loading'},
      {type: 'clothes/use_clothes_failed', payload: 'Unauthorized'},
      {type: 'clothes/use_clothes_idle'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.patch).toHaveBeenCalledTimes(1);
  });
});
