import {makeMockStore} from '../../../__mocks__/utils';
import mockAxios from 'axios';
// ---------------------------------------------

import {buyCloth} from '../../../src/framework/redux/actions/clothesActions';

describe('buyCloth thunk', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT buyCloth thunk', async () => {
    const mock_store = makeMockStore({});
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({
        data: {
          avatar: {
            avatar_name: 'Zorro',
            avatar_sets: ['DEFAULT'],
            current_style: 'DEFAULT',
            _id: '60dd18f88765370020405ef4',
            _user: '60dd18f88765370020405ef1',
            createdAt: '2021-07-01T01:23:04.563Z',
            updatedAt: '2021-07-01T01:23:59.897Z',
            __v: 1,
          },
          wallet: {
            total_coins: 35,
            _id: '60dd18f88765370020405ef3',
            _user: '60dd18f88765370020405ef1',
            createdAt: '2021-07-01T01:23:04.561Z',
            updatedAt: '2021-07-01T01:23:59.896Z',
            __v: 0,
          },
        },
      }),
    );

    const session_token: string = '123';
    const title: string = 'Terno';
    const outfit_id: string = '123';

    await mock_store.dispatch(buyCloth(title, outfit_id, session_token));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: 'clothes/buy_loading'},
      {type: 'clothes/buy_succeeded', payload: ['DEFAULT']},
      {type: 'wallet/update_coins_from_login', payload: 35},
      {type: 'clothes/buy_idle'},
    ];
    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test('FAILED buyCloth thunk', async () => {
    const mock_store = makeMockStore({});

    const mocked_response_api: Object = {
      response: {
        statusCode: 401,
        message: 'Unauthorized',
      },
      status: 401,
      message: 'Unauthorized',
    };

    mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(mocked_response_api),
    );

    const unauthorized_session_token: string = 'wrong_token';
    const title: string = 'Terno';
    const outfit_id: string = '123';

    await mock_store.dispatch(
      buyCloth(title, outfit_id, unauthorized_session_token),
    );
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: 'clothes/buy_loading'},
      {type: 'clothes/buy_failed', payload: 'Unauthorized'},
      {type: 'clothes/buy_idle'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });
});
