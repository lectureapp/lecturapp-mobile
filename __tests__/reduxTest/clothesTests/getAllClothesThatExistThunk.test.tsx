import {makeMockStore} from '../../../__mocks__/utils';
import mockAxios from 'axios';
// ---------------------------------------------

import {getAllClothesThatExist} from '../../../src/framework/redux/actions/clothesActions';

import * as clothesTypes from '../../../src/framework/redux/types/clothesTypes';

describe('getAllClothesThatExist thunk', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT getAllClothesThatExist thunk', async () => {
    const mock_store = makeMockStore({});
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [
          {
            _id: '60822ef1c6f81c0020c51d89',
            outfit_image: 'http:zorro_astronaut',
            outfit_name: 'ASTRONAUT',
            price: 65,
            createdAt: '2021-04-23T02:20:33.490Z',
            updatedAt: '2021-04-23T02:20:33.490Z',
          },
        ],
      }),
    );

    // const session_token: string = '123';
    // await mock_store.dispatch(bringFavoriteTales(session_token));
    await mock_store.dispatch(getAllClothesThatExist());

    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: 'clothes/get_all_clothes_that_exist_loading'},
      {
        type: 'clothes/get_all_clothes_that_exist_succeeded',
        payload: [
          {
            _id: '60822ef1c6f81c0020c51d89',
            outfit_name: 'ASTRONAUT',
            price: 65,
          },
        ],
      },
      {type: 'clothes/get_all_clothes_that_exist_idle'},
    ];
    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });

  // TODO:
  // FAILED REQUEST:
  // The server doesn't request the session token in this endpoint, for that
  // I didn't create the failed case test.
});
