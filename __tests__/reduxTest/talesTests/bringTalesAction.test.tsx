import {makeMockStore} from '../../../__mocks__/utils';

import mockAxios from 'axios';
import {mock_response_api} from '../../../__mocks__/redux/broughtTales';

import {bringTales} from '../../../src/framework/redux/actions/talesActions';

import * as talesTypes from '../../../src/framework/redux/types/talesTypes';

describe('bringTales thunks tests', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT bringTales thunk', async () => {
    const mock_store = makeMockStore({});

    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve(mock_response_api),
    );

    const access_token: string = '123';
    const current_page: number = 1;
    const activated_by_other_tales_button: boolean = true;
    await mock_store.dispatch(
      bringTales(access_token, current_page, activated_by_other_tales_button),
    );
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: talesTypes.TRAER_CUENTOS_LOADING},
      {
        type: talesTypes.TRAER_CUENTOS_SUCCEEDED,
        payload: {
          tales: mock_response_api.data.data,
          currentPage: 1,
          perPage: 4,
        },
      },
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });

  test('FAILED bringTales thunk', async () => {
    const mock_store = makeMockStore({});
    const mocked_response_api: Object = {
      statusCode: 401,
      message: 'Unauthorized',
    };

    mockAxios.get.mockImplementationOnce(() =>
      Promise.reject(mocked_response_api),
    );

    const unauthorized_access_token: string = '123';
    const current_page: number = 1;
    const activated_by_other_tales_button: boolean = true;
    await mock_store.dispatch(
      bringTales(
        unauthorized_access_token,
        current_page,
        activated_by_other_tales_button,
      ),
    );

    const triggered_actions: Array<Object> = mock_store.getActions();
    const expected_actions: Array<Object> = [
      {type: talesTypes.TRAER_CUENTOS_LOADING},
      {type: talesTypes.TRAER_CUENTOS_FAILED, payload: 'Unauthorized'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });
});
