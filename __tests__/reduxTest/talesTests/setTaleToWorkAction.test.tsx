import {makeMockStore} from '../../../__mocks__/utils';
import * as talesTypes from '../../../src/framework/redux/types/talesTypes';
import {setTaleToWork} from '../../../src/framework/redux/actions/talesActions';

import {mock_response_api} from '../../../__mocks__/redux/broughtTales';

describe('setTaleToWork thunks tests: ', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT setTaleToWork', () => {
    const mock_store = makeMockStore({});

    const tale_to_work: any = mock_response_api.data.data[0];

    mock_store.dispatch(setTaleToWork(tale_to_work));
    const triggered_actions = mock_store.getActions();
    // console.log('TRIGGERED: ', tale_to_work);

    const expected_actions: Array<Object> = [
      {
        type: talesTypes.SET_TALE_TO_WORK,
        payload: {
          content: [
            'Se había enamorado una gata de un hermoso joven, y rogó a Afrodita que la hiciera mujer. La diosa,  compadecida de su deseo, la transformó en una bella doncella, y entonces el joven, prendado de ella, la invitó a su casa.',
            'https://sincopyright.com/wp-content/uploads/2016/10/gata-y-raton.gif',
            'Estando ambos descansando en la alcoba nupcial, quiso saber Afrodita si al cambiar de ser a la gata había mudado también de carácter, por lo que soltó un ratón en el centro de la alcoba. Olvidándose la gata de su condición presente, se levantó del lecho y persiguió al ratón para comérselo. Entonces la diosa, indignada, la volvió a su original estado.',
            'Moraleja: El cambio de estado de una persona, no la hace cambiar de sus instintos.',
          ],
          _id: '6070ad5d39898a004dd90787',
          title: 'Afrodita y la gata',
          cover_page: 'https://imagen_portada.png',
          difficulty: 'facil',
          gender: 'drama',
          author: 'chabelos',
          questions: [
            {
              alternative: [
                {
                  _id: '6070ad5d39898a004dd90789',
                  label: 'Afrodita',
                  value: 0,
                },
                {
                  _id: '6070ad5d39898a004dd9078a',
                  label: 'Una gata',
                  value: 1,
                },
                {
                  _id: '6070ad5d39898a004dd9078b',
                  label: 'Un hermoso joven',
                  value: 2,
                },
                {
                  _id: '6070ad5d39898a004dd9078c',
                  label: 'Un ratón',
                  value: 3,
                },
              ],
              _id: '6070ad5d39898a004dd90788',
              question_id: 1,
              question: '¿Quién se había enamorado?',
              correct_answer: 1,
            },
            {
              alternative: [
                {
                  _id: '6070ad5d39898a004dd9078e',
                  label: 'Afrodita',
                  value: 0,
                },
                {
                  _id: '6070ad5d39898a004dd9078f',
                  label: 'Una gata',
                  value: 1,
                },
                {
                  _id: '6070ad5d39898a004dd90790',
                  label: 'Un hermoso joven',
                  value: 2,
                },
                {
                  _id: '6070ad5d39898a004dd90791',
                  label: 'Un ratón',
                  value: 3,
                },
              ],
              _id: '6070ad5d39898a004dd9078d',
              question_id: 2,
              question: '¿Quién se transformó en persona?',
              correct_answer: 1,
            },
            {
              alternative: [
                {
                  _id: '6070ad5d39898a004dd90793',
                  label: 'Afrodita',
                  value: 0,
                },
                {
                  _id: '6070ad5d39898a004dd90794',
                  label: 'Una gata',
                  value: 1,
                },
                {
                  _id: '6070ad5d39898a004dd90795',
                  label: 'Un hermoso joven',
                  value: 2,
                },
                {
                  _id: '6070ad5d39898a004dd90796',
                  label: 'Un ratón',
                  value: 3,
                },
              ],
              _id: '6070ad5d39898a004dd90792',
              question_id: 3,
              question: '¿Quién se indignó?',
              correct_answer: 0,
            },
          ],
          createdAt: '2021-04-09T19:39:09.899Z',
          updatedAt: '2021-04-09T19:39:09.899Z',
          __v: 0,
          times_read: false,
          added_as_favorite: true,
        },
      },
    ];

    expect(triggered_actions).toEqual(expected_actions);
  });
});
