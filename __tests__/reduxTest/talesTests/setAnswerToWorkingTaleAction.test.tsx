import mockAxios from 'axios';
import {makeMockStore} from '../../../__mocks__/utils';

import {setAnswerToWorkingTale} from '../../../src/framework/redux/actions/talesActions';

import {taleToWorkWithAnswers} from '../../../__mocks__/redux/taleToWorkWithAnswers';

import {
  SET_ANSWERS_TO_WORKING_TALE,
  UPDATE_24H_AGO_BOOLEAN,
} from '../../../src/framework/redux/types/talesTypes';

import {ONLY_SET_CURRENT_VIDEO_TO_SHOW} from '../../../src/framework/redux/types/availableVideosTypes';

describe('setAnswerToWorkingTales thunk tests', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT setAnswerToWorkingTale thunk', async () => {
    const mock_store = makeMockStore({});

    const mock_response_api = {
      data: {
        message: 'Cuento terminado anteriormente',
        tale_title: 'Afrodita y la gata',
        obtained_video: {
          state: 'ACTIVE',
          _id: '60b943137dd20f002123c928',
          _videoId: '608210f7fd3fdd002c108d2a',
          _url: 'https://youtu.be/lGkgix6aQN4',
          date: '2021-06-03T21:01:07.397Z',
          time_left: '24 horas',
          createdAt: '2021-06-03T21:01:07.405Z',
          updatedAt: '2021-06-03T21:01:07.405Z',
          __v: 0,
        },
        user_wallet: {
          total_coins: 12,
          _id: '609d63dced5fa100218fd83b',
          _user: '609d63dced5fa100218fd839',
          createdAt: '2021-05-13T17:37:32.985Z',
          updatedAt: '2021-06-03T23:52:25.436Z',
          __v: 0,
        },
      },
    };

    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve(mock_response_api),
    );

    const session_token: string = '123';
    await mock_store.dispatch(
      setAnswerToWorkingTale(taleToWorkWithAnswers, session_token),
    );

    const triggered_actions: Array<Object> = mock_store.getActions();
    const expected_actions = [
      {
        // type: 'available_videos/only_set_current_video_to_show',
        type: ONLY_SET_CURRENT_VIDEO_TO_SHOW,
        payload: {
          state: 'ACTIVE',
          _id: '60b943137dd20f002123c928',
          _videoId: '608210f7fd3fdd002c108d2a',
          _url: 'https://youtu.be/lGkgix6aQN4',
          date: '2021-06-03T21:01:07.397Z',
          time_left: '24 horas',
          createdAt: '2021-06-03T21:01:07.405Z',
          updatedAt: '2021-06-03T21:01:07.405Z',
          __v: 0,
        },
      },
      {
        type: UPDATE_24H_AGO_BOOLEAN,
        payload: true,
      },
      {
        type: SET_ANSWERS_TO_WORKING_TALE,
        payload: {
          taleToWorkWithAnswers: {
            __v: 0,
            _id: '6070ad5d39898a004dd90787',
            added_as_favorite: true,
            author: 'chabelos',
            content: [
              'Se había enamorado una gata de un hermoso joven, y rogó a Afrodita que la hiciera mujer. La diosa,  compadecida de su deseo, la transformó en una bella doncella, y entonces el joven, prendado de ella, la invitó a su casa.',
              'https://sincopyright.com/wp-content/uploads/2016/10/gata-y-raton.gif',
              'Estando ambos descansando en la alcoba nupcial, quiso saber Afrodita si al cambiar de ser a la gata había mudado también de carácter, por lo que soltó un ratón en el centro de la alcoba. Olvidándose la gata de su condición presente, se levantó del lecho y persiguió al ratón para comérselo. Entonces la diosa, indignada, la volvió a su original estado.',
              'Moraleja: El cambio de estado de una persona, no la hace cambiar de sus instintos.',
            ],
            cover_page: 'https://imagen_portada.png',
            createdAt: '2021-04-09T19:39:09.899Z',
            difficulty: 'facil',
            gender: 'drama',
            questions: [
              {
                _id: '6070ad5d39898a004dd90788',
                alternative: [Array],
                answer_mark: 1,
                correct_answer: 1,
                question: '¿Quién se había enamorado?',
                question_id: 1,
              },
              {
                _id: '6070ad5d39898a004dd9078d',
                alternative: [Array],
                answer_mark: 1,
                correct_answer: 1,
                question: '¿Quién se transformó en persona?',
                question_id: 2,
              },
              {
                _id: '6070ad5d39898a004dd90792',
                alternative: [Array],
                answer_mark: 1,
                correct_answer: 0,
                question: '¿Quién se indignó?',
                question_id: 3,
              },
            ],
            times_read: true,
            title: 'Afrodita y la gata',
            updatedAt: '2021-04-09T19:39:09.899Z',
          },
          num_of_correct_answers: 2,
          num_of_incorrect_answers: 1,
        },
      },
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  // TODO: Este thunk no tiene un dispatch cuando la petición es incorrecta.
});
