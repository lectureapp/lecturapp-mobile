import {makeMockStore} from '../../../__mocks__/utils';
import mockAxios from 'axios';
// ---------------------------------------------

import {bringFavoriteTales} from '../../../src/framework/redux/actions/talesActions';

import * as talesTypes from '../../../src/framework/redux/types/talesTypes';

import {
  bringFavoriteTalesResponse,
  tales_with_favorites_field,
} from '../../../__mocks__/redux/bringFavoriteTalesResponse';

describe('bringFavoriteTales thunk', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT bringFavoriteTales thunk', async () => {
    const mock_store = makeMockStore({});
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve(bringFavoriteTalesResponse),
    );

    const session_token: string = '123';

    await mock_store.dispatch(bringFavoriteTales(session_token));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: talesTypes.BRING_FAVORITE_TALES_LOADING},
      {
        type: talesTypes.BRING_FAVORITE_TALES_SUCCEEDED,
        payload: tales_with_favorites_field,
      },
    ];
    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });

  test('FAILED bringFavoriteTales thunk', async () => {
    const mock_store = makeMockStore({});

    const mocked_response_api: Object = {
      statusCode: 401,
      message: 'Unauthorized',
    };
    mockAxios.get.mockImplementationOnce(() =>
      Promise.reject(mocked_response_api),
    );

    const unauthorized_session_token: string = 'wrong';

    await mock_store.dispatch(bringFavoriteTales(unauthorized_session_token));
    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: talesTypes.BRING_FAVORITE_TALES_LOADING},
      {
        type: talesTypes.BRING_FAVORITE_TALES_FAILED,
        payload: 'Unauthorized',
      },
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });
});
