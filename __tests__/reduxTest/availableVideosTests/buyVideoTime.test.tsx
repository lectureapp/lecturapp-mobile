import {makeMockStore} from '../../../__mocks__/utils';
import mockAxios from 'axios';
// ---------------------------------------------
import {buy_video_time_response} from '../../../__mocks__/redux/buyVideTimeMocks';
import {buyVideoTime} from '../../../src/framework/redux/actions/availableVideosActions';

import * as availableVideosTypes from '../../../src/framework/redux/types/availableVideosTypes';

describe('buyVideoTime thunk', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('CORRECT buyVideoTime thunk', async () => {
    const mock_store = makeMockStore({});

    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve(buy_video_time_response),
    );

    const session_token: string = '123';
    const video_mongoID: string = '123';
    await mock_store.dispatch(buyVideoTime(session_token, video_mongoID));

    const triggered_actions = mock_store.getActions();
    // console.log('triggered: ', JSON.stringify(triggered_actions, null, 4));

    const expected_actions: Array<Object> = [
      {
        type: 'available_videos/buy_video_time_loading',
      },
      {
        type: 'available_videos/buy_video_time_succeeded',
        payload: {
          message: 'Video actualizado y Monedas restadas correctamente',
          data: [
            {
              state: 'ACTIVE',
              _id: '60d7d00d2b112f0020ecf836',
              _videoId: '60d68fbe5d9ebd002237a523',
              _url: 'http://urlvideo5',
              date: '2021-07-03T01:10:37.802Z',
              time_left: '164 horas',
              createdAt: '2021-06-27T01:10:37.803Z',
              updatedAt: '2021-06-27T05:06:59.568Z',
              __v: 0,
            },
            {
              state: 'ACTIVE',
              _id: '60d7d5f72b112f0020ecf8f2',
              _videoId: '6085f966bcfb53002c3a6e7b',
              _url: 'https://youtu.be/J2WCaJjtbsY',
              date: '2021-06-28T01:35:51.597Z',
              time_left: '44 horas',
              createdAt: '2021-06-27T01:35:51.600Z',
              updatedAt: '2021-06-27T05:06:59.568Z',
              __v: 0,
            },
          ],
          wallet: {
            total_coins: 62,
            _id: '60d7cffb2b112f0020ecf834',
            _user: '60d7cffb2b112f0020ecf832',
            createdAt: '2021-06-27T01:10:19.526Z',
            updatedAt: '2021-06-27T05:06:59.547Z',
            __v: 0,
          },
        },
      },
      {
        type: 'available_videos/only_set_current_video_to_show',
      },
      {
        type: 'wallet/update_coins',
        payload: 62,
      },
      {type: 'available_videos/buy_video_time_return_idle'},
    ];
    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test('FAILED buyVideoTime thunk', async () => {
    const mock_store = makeMockStore({});

    const mocked_response_api: Object = {
      statusCode: 401,
      message: 'error message',
    };
    mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(mocked_response_api),
    );

    const unauthorized_session_token: string = 'wrong';
    const video_mongoID: string = '123';
    await mock_store.dispatch(
      buyVideoTime(unauthorized_session_token, video_mongoID),
    );

    const triggered_actions = mock_store.getActions();

    const expected_actions: Array<Object> = [
      {type: availableVideosTypes.BUY_VIDEO_TIME_LOADING},
      {
        type: availableVideosTypes.BUY_VIDEO_TIME_FAILED,
        payload: 'error message',
      },
      {type: 'available_videos/buy_video_time_return_idle'},
    ];

    expect(triggered_actions).toEqual(expected_actions);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });
});
