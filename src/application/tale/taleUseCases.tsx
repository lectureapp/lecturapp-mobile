import {Tale} from '../../domain/tale/tale';
import {TaleRepository} from '../../domain/tale/taleRepository';

export interface TaleService {
  getSampleTale(): Tale;
}

export class TaleServiceImpl implements TaleService {
  bookRepo: TaleRepository;

  constructor(bookRepo: TaleRepository) {
    this.taleRepo = taleRepo;
  }

  getSampleTale(): Tale {
    return this.bookRepo.getTale();
  }
}
