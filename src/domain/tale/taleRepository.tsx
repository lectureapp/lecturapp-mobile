import {Tale} from './tale';

export interface TaleRepository {
  getTales(): [Tale];
}
