import question from '../question/question';

export class Tale {
  title: string;
  content: [];
  question: [question];

  constructor(title: string, content: []) {
    this.title = title;
    this.content = content;
  }
}
