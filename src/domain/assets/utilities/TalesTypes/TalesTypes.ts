import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

export interface Question {
  id: number;
  question: string;
  alternatives: Alternative[];
  correct_answer: number;
}

export interface Alternative {
  label: string;
  value: number;
}

export interface ITales {
  id?: number;
  titulo: string;
  contenido: string[];
  dificultad: string;
  genero: string;
  autor: string;
  preguntas: Question[];
}

export type TalesParamList = {
  DailyView: undefined;
  TaleView: {item: ITales} | undefined;
  QuestionsView: {item: ITales} | undefined;
  QualificationView: undefined;
  RewardView: undefined;
};

export type TalesProps<T extends keyof TalesParamList> = {
  navigation: StackNavigationProp<TalesParamList, T>;
  route: RouteProp<TalesParamList, T>;
};
