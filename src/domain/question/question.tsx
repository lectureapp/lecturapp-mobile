// TODO: cambiar modelo question según lo que responderá el backend
export class Question {
  id: string;
  statement: string;
  alternatives: [];
  right_answer: number;

  constructor(
    id: string,
    statement: string,
    alternatives: [],
    right_answer: number,
  ) {
    this.id = id;
    this.statement = statement;
    this.alternatives = alternatives;
    this.right_answer = right_answer;
  }
}
