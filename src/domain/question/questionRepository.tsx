import {Question} from './question';

export interface QuestionRepository {
  getQuestionsList(taleId: string): [Question]; // trae todas las preguntas asociadas a un cuento
}
