// importaciones para redux:
import {createStore, applyMiddleware} from 'redux';
import reducers from '../redux/reducers/index';
import reduxThunk from 'redux-thunk';

// Importación para redux-persist:
import {persistStore} from 'redux-persist';

// import logger from 'redux-logger';

// Be sure to ONLY add this middleware in development!
const middleware_list =
  process.env.NODE_ENV !== 'production'
    ? [require('redux-immutable-state-invariant').default(), reduxThunk]
    : [reduxThunk];

const store = createStore(
  reducers, // Todos los reducers
  {}, // Estado inicial
  //applyMiddleware(reduxThunk, logger), // reduxThunk sirve para que redux pueda hacer peticiones asincronas.
  applyMiddleware(reduxThunk),
  // applyMiddleware(...middleware_list),
);

const persistor = persistStore(store);

//export default store;
//export default {store, persistor}; // Se debe borrar el default para que no bote error
export {store, persistor};
