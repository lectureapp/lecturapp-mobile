export const REGISTER_LOADING = 'auth/register_loading';
export const REGISTER_FAILED = 'auth/register_failed';
export const REGISTER_SUCCEEDED = 'auth/register_succeeded';

export const LOGIN_LOADING = 'auth/login_loading';
export const LOGIN_FAILED = 'auth/login_failed';
export const LOGIN_SUCCEEDED = 'auth/login_succeeded';

export const LOGOUT = 'auth/logout';
