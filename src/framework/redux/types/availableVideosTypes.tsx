export const GET_AVAILABLE_VIDEOS_LOADING =
  'available_videos/get_available_videos_loading';
export const GET_AVAILABLE_VIDEOS_FAILED =
  'available_videos/get_available_videos_failed';
export const GET_AVAILABLE_VIDEOS_SUCCEEDED =
  'available_videos/get_available_videos_succeeded';

export const ADD_NEW_OBTAINED_VIDEO = 'available_videos/add_new_obtained_video';
export const ONLY_SET_CURRENT_VIDEO_TO_SHOW =
  'available_videos/only_set_current_video_to_show';

export const BUY_VIDEO_TIME_LOADING = 'available_videos/buy_video_time_loading';
export const BUY_VIDEO_TIME_FAILED = 'available_videos/buy_video_time_failed';
export const BUY_VIDEO_TIME_SUCCEEDED =
  'available_videos/buy_video_time_succeeded';

export const BUY_VIDEO_TIME_RETURN_IDLE =
  'available_videos/buy_video_time_return_idle';
