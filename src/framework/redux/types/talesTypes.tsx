//export const TRAER_CUENTOS = 'traer_cuentos';

export const TRAER_CUENTOS_LOADING = 'tales/traer_cuentos_loading';
export const TRAER_CUENTOS_FAILED = 'tales/traer_cuentos_failed';
export const TRAER_CUENTOS_SUCCEEDED = 'tales/traer_cuentos_succeeded';

export const SET_TALE_TO_WORK = 'tales/set_tale_to_work';
export const SET_ANSWERS_TO_WORKING_TALE = 'tales/set_answers_to_working_tale';
export const PUBLISH_ANSWERED_TALE = 'tales/publish_answered_tale';
export const ERASE_WORKING_TALE = 'tales/erase_working_tale';

export const FINISH_TALE = 'finish_tale';

export const SET_FAVORITE_TALE = 'tales/set_favorite_tale';
export const REMOVE_FAVORITE_TALE = 'tales/remove_favorite_tale';

// BRING FAVORITES TALES:
// ---------------------------------------------------
export const BRING_FAVORITE_TALES_LOADING =
  'tales/bring_favorites_tales_loading';
export const BRING_FAVORITE_TALES_FAILED = 'tales/bring_favorites_tales_failed';
export const BRING_FAVORITE_TALES_SUCCEEDED =
  'tales/bring_favorites_tales_succeeded';

export const CHANGE_PAGINATION_OF_DAILY_TALES =
  'tales/change_pagination_of_daily_tales';
// ---------------------------------------------------

export const UPDATE_24H_AGO_BOOLEAN = 'tales/update_24h_ago_boolean';
