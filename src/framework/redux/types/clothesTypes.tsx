export const GET_ALL_CLOTHES_LOADING = 'clothes/get_all_clothes_loading';
export const GET_ALL_CLOTHES_SUCCEEDED = 'clothes/get_all_clothes_succeeded';
export const GET_ALL_CLOTHES_FAILED = 'clothes/get_all_clothes_failed';

export const BUY_CLOTH_LOADING = 'clothes/buy_loading';
export const BUY_CLOTH_SUCCEEDED = 'clothes/buy_succeeded';
export const BUY_CLOTH_FAILED = 'clothes/buy_failed';
export const BUY_CLOTH_NOT_ENOUGH_COINS = 'clothes/not_enough_coins';
export const BUY_CLOTH_IDLE = 'clothes/buy_idle';

export const USE_CLOTHES_LOADING = 'clothes/use_clothes_loading';
export const USE_CLOTHES_SUCCEEDED = 'clothes/use_clothes_succeeded';
export const USE_CLOTHES_FAILED = 'clothes/use_clothes_failed';
export const USE_CLOTHES_IDLE = 'clothes/use_clothes_idle';

export const UPDATE_USER_CLOTHES = 'clothes/update_user_clothes';

export const USE_CLOTHES = 'clothes/use_clothes';

export const GET_ALL_CLOTHES_THAT_EXIST_LOADING =
  'clothes/get_all_clothes_that_exist_loading';
export const GET_ALL_CLOTHES_THAT_EXIST_SUCCEEDED =
  'clothes/get_all_clothes_that_exist_succeeded';
export const GET_ALL_CLOTHES_THAT_EXIST_FAILED =
  'clothes/get_all_clothes_that_exist_failed';
export const GET_ALL_CLOTHES_THAT_EXIST_IDLE =
  'clothes/get_all_clothes_that_exist_idle';
