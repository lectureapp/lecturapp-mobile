import {
  GET_STADISTICS_SUCCEEDED,
  GET_STADISTICS_LOADING,
  GET_STADISTICS_FAILED,
} from '../types/stadisticsTypes';

const INITIAL_STATE = {
  // Traer estadisticas estado:
  traer_estadisticas_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  traer_estadisticas_message: '', // message of status response

  // today:
  today_tales_readed: 0,
  hit_percentaje_today: 0,

  // week:
  hit_percentaje_week: 0,
  week_tales_readed: 0,

  // total:
  total_tales: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // TRAER ESTADISTICAS:
    // ---------------------------------------------------
    case GET_STADISTICS_SUCCEEDED:
      return {
        ...state,
        today_tales_readed: action.payload.today_tales_readed,
        hit_percentaje_today: action.payload.hit_percentaje_today,

        hit_percentaje_week: action.payload.hit_percentaje_week,
        week_tales_readed: action.payload.week_tales_readed,
        total_tales: action.payload.total_tales,

        traer_estadisticas_state: 'idle',
      };

    case GET_STADISTICS_LOADING:
      return {...state, traer_estadisticas_state: 'loading'};

    case GET_STADISTICS_FAILED:
      return {
        ...state,
        traer_estadisticas_state: 'failed',
        traer_estadisticas_message: action.payload,
      };
    //
    // ---------------------------------------------------

    default:
      return state;
  }
};
