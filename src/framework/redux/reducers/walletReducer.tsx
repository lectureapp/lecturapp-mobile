import {UPDATE_COINS_FROM_LOGIN, UPDATE_COINS} from '../types/walletTypes';

//import APIBackend from '../../axios/axios';

const INITIAL_STATE = {
  total_coins: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_COINS_FROM_LOGIN:
      return {
        ...state,
        total_coins: action.payload,
      };

    case UPDATE_COINS:
      return {
        ...state,
        total_coins: action.payload,
      };

    default:
      return state;
  }
};
