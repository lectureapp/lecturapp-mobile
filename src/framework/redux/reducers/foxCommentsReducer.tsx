import {SET_CURRENT_FOX_COMMENT} from '../types/foxCommentsTypes';

const INITIAL_STATE = {
  fox_comments: [
    'Bienvenido a lecturapp, ¿ya estas listo para seguir leyendo?',
    'Espero que estes bien hoy :)',
    'Completa los cuentos para ganar videos y monedas.',
  ],

  current_fox_comment: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CURRENT_FOX_COMMENT:
      //return {random_comment};
      return {...state, current_fox_comment: action.payload};

    default:
      return state;
  }
};
