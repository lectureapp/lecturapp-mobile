import {
  UPDATE_USER_CLOTHES,
  //
  // buy cloth:
  BUY_CLOTH_LOADING,
  BUY_CLOTH_SUCCEEDED,
  BUY_CLOTH_FAILED,
  BUY_CLOTH_NOT_ENOUGH_COINS,
  USE_CLOTHES,
  //
  GET_ALL_CLOTHES_THAT_EXIST,
  GET_ALL_CLOTHES_THAT_EXIST_LOADING,
  GET_ALL_CLOTHES_THAT_EXIST_SUCCEEDED,
  GET_ALL_CLOTHES_THAT_EXIST_FAILED,
  GET_ALL_CLOTHES_THAT_EXIST_IDLE,
  USE_CLOTHES_SUCCEEDED,
  USE_CLOTHES_LOADING,
  USE_CLOTHES_FAILED,
  USE_CLOTHES_IDLE,
  BUY_CLOTH_IDLE,
} from '../types/clothesTypes';

const INITIAL_STATE = {
  all_user_clothes: [],

  current_cloth: '',

  // BUYING STATE:
  // ---------------------------------------------------
  buy_cloth_state: 'idle', // possibles: 'idle', 'loading', 'not_coins', 'failed'
  buy_cloth_message: '', // message of status response
  // ---------------------------------------------------

  all_clothes_that_exist: [],

  // GET ALL CLOTHES THAT EXISTS STATE:
  // ---------------------------------------------------
  get_all_clothes_that_exist_state: 'idle', // possibles: 'idle', 'loading', 'not_coins', 'failed'
  get_all_clothes_that_exist_message: '', // message of status response
  // ---------------------------------------------------

  // USE CLOTHES STATE:
  // ---------------------------------------------------
  use_clothes_state: 'idle', // possibles: 'idle', 'loading', 'not_coins', 'failed'
  use_clothes_message: '', // message of status response
  // ---------------------------------------------------
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case UPDATE_USER_CLOTHES: {
      // Este update se llama cuando se loguea.
      return {
        ...state,
        all_user_clothes: action.payload,
      };
    }

    // BUYING:
    // ---------------------------------------------------
    case BUY_CLOTH_SUCCEEDED: {
      return {
        ...state,
        //...action.payload,
        all_user_clothes: action.payload,
        buy_cloth_state: 'idle',
        buy_cloth_message: 'Compra correcta.',
      };
    }

    case BUY_CLOTH_LOADING: {
      return {
        ...state,
        buy_cloth_state: 'loading',
      };
    }

    case BUY_CLOTH_FAILED: {
      return {
        ...state,
        buy_cloth_state: 'failed',
        buy_cloth_message: action.payload,
      };
    }

    case BUY_CLOTH_NOT_ENOUGH_COINS: {
      return {
        ...state,
        buy_cloth_state: 'not_coins',
        buy_cloth_message: action.payload,
      };
    }

    case BUY_CLOTH_IDLE: {
      return {
        ...state,
        buy_cloth_state: 'idle',
        //Si actualizo y borro el mensaje cuando termina el request el modal no lo captura:
        // buy_cloth_message: '',
      };
    }

    // ---------------------------------------------------

    // WEARING:
    // ---------------------------------------------------
    case USE_CLOTHES: {
      return {
        ...state,
        current_cloth: action.payload,
      };
    }

    case USE_CLOTHES_SUCCEEDED: {
      return {
        ...state,
        current_cloth: action.payload,
        use_clothes_state: 'succeeded',
        // Note: the server doesn't send any message when it's succeeded.
        // use_clothes_message: action.payload.message,
      };
    }

    case USE_CLOTHES_LOADING: {
      return {
        ...state,
        use_clothes_state: 'loading',
      };
    }

    case USE_CLOTHES_FAILED: {
      return {
        ...state,
        use_clothes_state: 'failed',
        use_clothes_message: action.payload,
      };
    }

    case USE_CLOTHES_IDLE: {
      return {
        ...state,
        use_clothes_state: 'idle',
        use_clothes_message: '',
      };
    }

    // ---------------------------------------------------

    // FETCHING ALL CLOTHES THAT EXISTS:
    // ---------------------------------------------------

    case GET_ALL_CLOTHES_THAT_EXIST_SUCCEEDED: {
      return {
        ...state,
        all_clothes_that_exist: action.payload,
        get_all_clothes_that_exist_state: 'succeeded',
        // Note: the server doesn't send any message when it's succeeded.
        // get_all_clothes_that_exist_message: action.payload.message,
      };
    }

    case GET_ALL_CLOTHES_THAT_EXIST_LOADING: {
      return {
        ...state,
        get_all_clothes_that_exist_state: 'loading',
      };
    }

    case GET_ALL_CLOTHES_THAT_EXIST_FAILED: {
      return {
        ...state,
        get_all_clothes_that_exist_state: 'failed',
        get_all_clothes_that_exist_message: action.payload,
      };
    }

    case GET_ALL_CLOTHES_THAT_EXIST_IDLE: {
      return {
        ...state,
        get_all_clothes_that_exist_state: 'idle',
        get_all_clothes_that_exist_message: '',
      };
    }

    // ---------------------------------------------------

    default:
      return state;
  }
};
