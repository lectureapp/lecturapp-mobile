import {combineReducers} from 'redux';
import talesReducer from './talesReducer';
import configTalesReducer from './configTaleReducer';
import AuthReducer from './AuthReducer';
import clothesReducer from './clothesReducer';
import walletReducer from './walletReducer';
import availableVideosReducer from './availableVideosReducer';
import foxCommentsReducer from './foxCommentsReducer';
import stadisticsReducer from './stadisticsReducer';

// Este archivo sirve para combinar todos los reducers que
// tengamos en la aplicación.
//export default combineReducers({
//talesReducer,
//});

// ---------------------------------------------
// implementando redux-persist:

import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import reduxThunk from 'redux-thunk';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  //whitelist: ['talesReducer', 'configTalesReducer'], //whitelist: Lista de todos los reducers que queremos persistir
  whitelist: [
    'configTalesReducer',
    'AuthReducer',
    'talesReducer',
    'clothesReducer',
    'walletReducer',
  ],
};

const rootReducer = combineReducers({
  talesReducer,
  configTalesReducer,
  AuthReducer,
  clothesReducer,
  walletReducer,
  availableVideosReducer,
  foxCommentsReducer,
  stadisticsReducer,
});

export default persistReducer(persistConfig, rootReducer);

// ---------------------------------------------
