import {
  REGISTER_LOADING,
  REGISTER_FAILED,
  REGISTER_SUCCEEDED,
  LOGIN_LOADING,
  LOGIN_FAILED,
  LOGIN_SUCCEEDED,
  LOGOUT,
} from '../types/AuthTypes';

const INITIAL_STATE = {
  account_name: '',
  email: '',
  session_token: '',

  registration_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  registration_message: '', // message of status response

  login_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  login_message: '', // message of status response
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // Registration:
    // ---------------------------------------------------
    case REGISTER_SUCCEEDED: {
      return {
        ...state,
        //...action.payload,
        registration_state: 'idle',
        registration_message: 'Registro correcto.',

        name: action.payload.name,
        email: action.payload.email,
        session_token: action.payload.token,

        // TODO: El backend todavía no envía los mensajes de los resultados.
      };
    }

    case REGISTER_LOADING: {
      return {
        ...state,
        registration_state: 'loading',
      };
    }

    case REGISTER_FAILED: {
      return {
        ...state,
        registration_state: 'failed',
        registration_message: action.payload,
      };
    }

    // Login:
    // ---------------------------------------------------
    case LOGIN_SUCCEEDED: {
      return {
        ...state,
        // ...action.payload,
        session_token: action.payload.data.user.access_token,
        email: action.payload.data.user.email,
        account_name: action.payload.data.user.name,

        login_state: 'idle',
        login_message: action.payload.message,
      };
    }

    case LOGIN_LOADING: {
      return {
        ...state,
        login_state: 'loading',
      };
    }

    case LOGIN_FAILED: {
      return {
        ...state,
        login_state: 'failed',
        login_message: action.payload,
      };
    }
    // ---------------------------------------------------

    case LOGOUT: {
      return {
        ...state,

        account_name: '',
        email: '',
        session_token: '',
      };
    }

    default:
      return state;
  }
};
