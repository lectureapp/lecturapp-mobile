import {
  SET_DOTS_PER_LINE,
  CHANGE_DOTS_ACTIVATED,
  CHANGE_DOTS_SIZE,
} from '../types/configTalesTypes';

const INITIAL_STATE = {
  dotsIsActivated: false,
  dotsPerLine: 3,
  sizeOfDots: 10,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_DOTS_PER_LINE:
      return {...state, dotsPerLine: action.payload};

    case CHANGE_DOTS_ACTIVATED:
      return {...state, dotsIsActivated: !state.dotsIsActivated};

    case CHANGE_DOTS_SIZE:
      return {...state, sizeOfDots: action.payload};

    default:
      return state;
  }
};
