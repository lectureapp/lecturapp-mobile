import {
  GET_AVAILABLE_VIDEOS_LOADING,
  GET_AVAILABLE_VIDEOS_FAILED,
  GET_AVAILABLE_VIDEOS_SUCCEEDED,
  ADD_NEW_OBTAINED_VIDEO,
  ONLY_SET_CURRENT_VIDEO_TO_SHOW,
  BUY_VIDEO_TIME_LOADING,
  BUY_VIDEO_TIME_FAILED,
  BUY_VIDEO_TIME_SUCCEEDED,
  BUY_VIDEO_TIME_RETURN_IDLE,
} from '../types/availableVideosTypes';

const INITIAL_STATE = {
  // Get available videos:
  // ---------------------------------------------------
  get_available_videos_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  get_available_videos_message: '', // message of status response
  // ---------------------------------------------------

  all_videos: [],

  current_video_to_show: {},

  // Buy video:
  // ---------------------------------------------------
  buy_video_time_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  buy_video_time_message: '', // message of status response
  // ---------------------------------------------------
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    // Get available videos:
    // ---------------------------------------------------
    case GET_AVAILABLE_VIDEOS_SUCCEEDED: {
      return {
        ...state,
        all_videos: action.payload,
        // get_available_videos_state: 'idle',
      };
    }

    case GET_AVAILABLE_VIDEOS_LOADING: {
      return {
        ...state,
        get_available_videos_state: 'loading',
      };
    }

    case GET_AVAILABLE_VIDEOS_FAILED: {
      return {
        ...state,
        get_available_videos_state: 'failed',
        get_available_videos_message: action.payload,
      };
    }

    //
    // ---------------------------------------------------

    case ADD_NEW_OBTAINED_VIDEO: {
      return {
        ...state,
        all_videos: [...state.all_videos, action.payload],
        current_video_to_show: action.payload,
        //all_videos: state.all_videos.concat(action.payload), // Otra forma de hacer lo mismo
      };
    }

    case ONLY_SET_CURRENT_VIDEO_TO_SHOW: {
      return {
        ...state,
        current_video_to_show: action.payload,
      };
    }

    // Buy video:
    // ---------------------------------------------------
    case BUY_VIDEO_TIME_SUCCEEDED: {
      return {
        ...state,
        all_videos: action.payload.data, // action.payload.data is an array
        buy_video_time_message: action.payload.message,
        buy_video_time_state: 'succeeded',
      };
    }

    case BUY_VIDEO_TIME_LOADING: {
      return {
        ...state,
        buy_video_time_state: 'loading',
      };
    }

    case BUY_VIDEO_TIME_FAILED: {
      return {
        ...state,
        buy_video_time_state: 'failed',
        buy_video_time_message: action.payload,
      };
    }

    case BUY_VIDEO_TIME_RETURN_IDLE: {
      return {
        ...state,
        // all_videos: action.payload.data,
        buy_video_time_state: 'idle',
        buy_video_time_message: '',
      };
    }

    // ---------------------------------------------------

    default:
      return state;
  }
};
