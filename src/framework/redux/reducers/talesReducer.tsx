import {
  SET_ANSWERS_TO_WORKING_TALE,
  TRAER_CUENTOS_LOADING,
  TRAER_CUENTOS_FAILED,
  TRAER_CUENTOS_SUCCEEDED,
  //
  SET_TALE_TO_WORK,
  SET_FAVORITE_TALE,
  REMOVE_FAVORITE_TALE,

  // Bring favorite tales:
  BRING_FAVORITE_TALES_LOADING,
  BRING_FAVORITE_TALES_FAILED,
  BRING_FAVORITE_TALES_SUCCEEDED,
  //
  UPDATE_24H_AGO_BOOLEAN,
} from '../types/talesTypes';
import APIBackend from '../../axios/axios';

// library for update inmutables:
import produce from 'immer';

const INITIAL_STATE = {
  tales: [],

  // Traer cuentos state:
  // ---------------------------------------------------
  traer_cuentos_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  traer_cuentos_message: '', // message of status response
  // ---------------------------------------------------

  // WORKING TALE:
  // ---------------------------------------------------
  workingTale: {},
  num_of_correct_answers: 0,
  num_of_incorrect_answers: 0,

  resolved_less_than_24h_ago: false,
  // ---------------------------------------------------

  // FAVORITES TALES:
  // ---------------------------------------------------
  favorites_tales: [],
  set_favorite_tale_message: '',

  bring_favorites_tales_state: 'idle', // possibles: 'idle', 'loading', 'succeeded', 'failed'
  bring_favorites_tales_message: '', // message of status response
  // ---------------------------------------------------
  //added_as_favorite: false,

  currentPage: 1, // I'm not using this variable.
  perPage: 4,
  isLastPage: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // TRAER CUENTOS:
    // ---------------------------------------------------
    case TRAER_CUENTOS_SUCCEEDED:
      return {
        ...state,
        tales: action.payload.tales,
        currentPage: action.payload.currentPage,
        perPage: action.payload.perPage,
        traer_cuentos_state: 'idle',
        isLastPage: action.payload.isLastPage,
      };

    case TRAER_CUENTOS_LOADING:
      return {...state, traer_cuentos_state: 'loading'};

    case TRAER_CUENTOS_FAILED:
      return {
        ...state,
        traer_cuentos_state: 'failed',
        traer_cuentos_message: action.payload,
      };
    //
    // ---------------------------------------------------

    case SET_TALE_TO_WORK:
      return {
        ...state,
        workingTale: action.payload,
      };

    case SET_ANSWERS_TO_WORKING_TALE:
      // Nota: El produce de immer debería estar antes del switchcase
      // pero por alguna razón en este CASE si funciona.
      // 	  const working_tale_with_answers = produce(state, (draft) => {
      //   draft['workingTale'] = action.payload.taleToWorkWithAnswers;
      //   draft['num_of_correct_answers'] = action.payload.num_of_correct_answers;
      //   draft['num_of_incorrect_answers'] = action.payload.num_of_incorrect_answers;
      // });

      const working_tale_with_answers = {
        ...state,
        workingTale: action.payload.taleToWorkWithAnswers,
        num_of_correct_answers: action.payload.num_of_correct_answers,
        num_of_incorrect_answers: action.payload.num_of_incorrect_answers,
      };

      return working_tale_with_answers;

    case SET_FAVORITE_TALE:
      // Nota: si uso immer en esta parte la app no marca las repuestas
      // del examen. Al parecer el producer del immer debe estar antes del switch.
      const newState2 = {
        ...state,
        set_favorite_tale_message: action.payload,
        workingTale: {
          ...state.workingTale,
          added_as_favorite: true,
        },
      };
      return newState2;

    case REMOVE_FAVORITE_TALE:
      // Nota: si uso immer en esta parte la app no marca las repuestas
      // del examen. Al parecer el porducer del immer debe estar antes del switch.
      const working_tale_removed_of_favorites = {
        ...state,
        set_favorite_tale_message: action.payload,
        workingTale: {
          ...state.workingTale,
          added_as_favorite: false,
        },
      };
      return working_tale_removed_of_favorites;

    // BRING FAVORITE TALES :
    // ---------------------------------------------------
    case BRING_FAVORITE_TALES_SUCCEEDED:
      // TODO:  el backend debe enviar tambien la variable added_as_favorite
      // en cada objeto en la petición de traer cuentos favoritos.
      return {
        ...state,
        favorites_tales: action.payload,
        bring_favorites_tales_state: 'idle',
      };

    case BRING_FAVORITE_TALES_LOADING:
      return {...state, bring_favorites_tales_state: 'loading'};

    case BRING_FAVORITE_TALES_FAILED:
      return {
        ...state,
        bring_favorites_tales_state: 'failed',
        bring_favorites_tales_message: action.payload,
      };
    //
    // ---------------------------------------------------

    case UPDATE_24H_AGO_BOOLEAN:
      return {
        ...state,
        resolved_less_than_24h_ago: action.payload,
      };

    default:
      return state;
  }
};
