import {
  SET_TALE_TO_WORK,
  SET_ANSWERS_TO_WORKING_TALE,
  // traer cuentos:
  TRAER_CUENTOS_LOADING,
  TRAER_CUENTOS_FAILED,
  TRAER_CUENTOS_SUCCEEDED,
  SET_FAVORITE_TALE,
  REMOVE_FAVORITE_TALE,
  BRING_FAVORITE_TALES_LOADING,
  BRING_FAVORITE_TALES_SUCCEEDED,
  BRING_FAVORITE_TALES_FAILED,
  CHANGE_PAGINATION_OF_DAILY_TALES,
  UPDATE_24H_AGO_BOOLEAN,
} from '../types/talesTypes';

import APIBackend from '../../axios/axios';
//import getStorage from 'redux-persist/es/storage/getStorage';

import {
  ADD_NEW_OBTAINED_VIDEO,
  ONLY_SET_CURRENT_VIDEO_TO_SHOW,
} from '../types/availableVideosTypes';
import {UPDATE_COINS} from '../types/walletTypes';

export const bringTales =  // no se esta usando, se ha reemplazado por otras 2 funciones.
  (session_token, currentPage, activated_by_other_tales_button) =>
  async (distpatch, getState) => {
    // const {pagination_for_daily_tales} = getState().talesReducer;

    distpatch({type: TRAER_CUENTOS_LOADING});

    // TODO: El backend no devuele el currentPage correcto, siempre devuelve 1,
    // el frontend podría manejar esta variable por su cuenta.
    // LE comenté a tux y según su respuesta lo implementaré.

    try {
      let new_current_page: number = currentPage;
      if (activated_by_other_tales_button) new_current_page++;

      const response = await APIBackend.get(
        // `/tales/tales_completed/${currentPage}`,
        `/tales/tales_completed/${new_current_page}`,
        {
          headers: {Authorization: 'Bearer ' + session_token},
        },
      );

      if (activated_by_other_tales_button) {
        const num_of_brought_tales: number = response.data.data.length;
        if (num_of_brought_tales < 4) {
          new_current_page = 1;
        } else {
          // new_current_page = currentPage + 1;
        }
      }

      distpatch({
        type: TRAER_CUENTOS_SUCCEEDED,
        payload: {
          tales: response.data.data,
          currentPage: new_current_page,
          perPage: response.data.perPage,
        },
      });
    } catch (error) {
      distpatch({type: TRAER_CUENTOS_FAILED, payload: error.message});
    }
  };

export const bringTalesInDailyView =
  (session_token, currentPage) => async (distpatch, getState) => {
    distpatch({type: TRAER_CUENTOS_LOADING});

    try {
      const response = await APIBackend.get(
        `/tales/tales_completed/${currentPage}`,
        {
          headers: {Authorization: 'Bearer ' + session_token},
        },
      );

      distpatch({
        type: TRAER_CUENTOS_SUCCEEDED,
        payload: {
          tales: response.data.data,
          currentPage: currentPage,
          perPage: response.data.perPage,
        },
      });
    } catch (error) {
      // console.log('bringTalesInDailyView thunk error: ', error);
      distpatch({type: TRAER_CUENTOS_FAILED, payload: error.message});
    }
  };

export const bringTalesFromOtherTalesButton =
  (session_token, currentPage, oldIsLastPage) =>
  async (distpatch, getState) => {
    distpatch({type: TRAER_CUENTOS_LOADING});

    try {
      let new_current_page: number = currentPage + 1;
      if (oldIsLastPage) new_current_page = 1;

      const response = await APIBackend.get(
        `/tales/tales_completed/${new_current_page}`,
        {
          headers: {Authorization: 'Bearer ' + session_token},
        },
      );

      const num_of_brought_tales: number = response.data.data.length;
      let isLastPage: boolean = false;
      if (num_of_brought_tales < 4) {
        isLastPage = true;
      }
      distpatch({
        type: TRAER_CUENTOS_SUCCEEDED,
        payload: {
          tales: response.data.data,
          currentPage: new_current_page,
          perPage: response.data.perPage,
          isLastPage: isLastPage,
        },
      });
    } catch (error) {
      distpatch({type: TRAER_CUENTOS_FAILED, payload: error.message});
    }
  };

export const setTaleToWork = (taleToWork: any) => async (distpatch: any) => {
  distpatch({
    type: SET_TALE_TO_WORK,
    payload: taleToWork,
  });
};

export const setAnswerToWorkingTale =
  // NOTA: a parte de calificar las respuestas, este thunk
  // tambien envía el cuento resuelto al backend.

    (taleToWorkWithAnswers: any, session_token: string) =>
    async (distpatch: any, getState: any) => {
      //
      let num_of_correct_answers: number = 0;
      let num_of_incorrect_answers: number = 0;

      // Calificando preguntas:
      taleToWorkWithAnswers.questions.forEach((element: any) => {
        if (element.answer_mark == element.correct_answer) {
          num_of_correct_answers++;
        } else {
          num_of_incorrect_answers++;
        }
      });

      // Enviando cuento resuelto al backend:
      // ---------------------------------------------------
      // TODO: Se debe añadir los 3 tipos de resultados:
      // success, error, loading.
      // Lo ideal sería que cuando no haya internet se guarde
      // los resultados en el frontend y cuando se conecte a internet
      // se sincronicen los datos.

      try {
        const response = await APIBackend.post(
          'tales/add_tale_completed',
          {
            tale_id: taleToWorkWithAnswers._id + '',
            answered_correctly: num_of_correct_answers + '',
            answered_incorrectly: num_of_incorrect_answers + '',
          },
          {
            headers: {
              Authorization: 'Bearer ' + session_token,
              'Content-Type': 'application/json',
            },
          },
        );
        // TODO:  creo que el backend no está respondiendo bien con esta petición,
        // solo funcionó una vez y ahora el backend no responde la petición ni en
        // postman, le consulté a tux, esperaré a ver que me dice para seguir con este problema.

        if (response.data.message == 'Cuento terminado anteriormente') {
          distpatch({
            type: ONLY_SET_CURRENT_VIDEO_TO_SHOW,
            // payload: response.data.obtained_video,
            payload: response.data.obtained_video,
          });

          // update resolved_less_than_24h_ago boolean:
          distpatch({
            type: UPDATE_24H_AGO_BOOLEAN,
            payload: true,
          });
        } else {
          distpatch({
            type: ADD_NEW_OBTAINED_VIDEO,
            payload: response.data.obtained_video,
          });

          distpatch({
            type: UPDATE_COINS,
            payload: response.data.user_wallet.total_coins,
          });
        }

        // Este distpatch tambien envía al backend el número de respuestas
        // correctas e incorrectas:
        distpatch({
          type: SET_ANSWERS_TO_WORKING_TALE,
          payload: {
            taleToWorkWithAnswers,
            num_of_correct_answers,
            num_of_incorrect_answers,
          },
        });
      } catch (error) {
        // console.log('setAnswerToWorkingTale() Error: ', error);
      }
      // ---------------------------------------------------
    };

export const setFavoriteTale = () => async (distpatch, getState) => {
  const talesState = getState().talesReducer;
  const {session_token} = getState().AuthReducer;

  try {
    const response = await APIBackend.post(
      '/tales/add_favorite_tale?',
      {
        tale_id: talesState.workingTale._id + '',
      },
      {
        headers: {
          Authorization: 'Bearer ' + session_token,
          'Content-Type': 'application/json',
        },
      },
    );
    distpatch({
      type: SET_FAVORITE_TALE,
      payload: response.data.message,
    });
  } catch (error) {
    // console.log('setFavoriteTale Error: ', error);
    distpatch({
      type: SET_FAVORITE_TALE,
      payload: error.message,
    });
  }
};

export const removeFavoriteTale = () => async (distpatch, getState) => {
  // TODO:  este thunk no está funcionando bien.
  // He probado el endpoint con postman y tampoco está funcionando,
  // al parecer la ruta ha cambiado.
  const talesState = getState().talesReducer;
  const {session_token} = getState().AuthReducer;

  try {
    // const response = await APIBackend.post(
    // NOTA: la estructura de un request delete es diferente a la de post
    const response = await APIBackend.delete('/tales/remove_favorite_tale?', {
      data: {
        tale_id: talesState.workingTale._id + '',
      },
      headers: {
        Authorization: 'Bearer ' + session_token,
        'Content-Type': 'application/json',
      },
    });
    distpatch({
      type: REMOVE_FAVORITE_TALE,
      payload: response.data.message,
    });
  } catch (error) {
    // console.log('removeFavoriteTale Error: ', error.message);
    distpatch({
      type: REMOVE_FAVORITE_TALE,
      payload: error.message,
    });
  }
};

export const bringFavoriteTales =
  (session_token) => async (distpatch, getState) => {
    distpatch({type: BRING_FAVORITE_TALES_LOADING});

    try {
      const response = await APIBackend.get('/tales/favorite_tales', {
        headers: {Authorization: 'bearer ' + session_token},
      });

      const new_array = response.data.map((element: any) => {
        return {...element, added_as_favorite: true};
      });
      distpatch({
        type: BRING_FAVORITE_TALES_SUCCEEDED,
        payload: new_array,
      });
    } catch (error) {
      //console.log('bringFavoriteTales() error: ', error);
      distpatch({type: BRING_FAVORITE_TALES_FAILED, payload: error.message});
    }
  };

export const set24hBooleanToFalse = () => async (distpatch) => {
  distpatch({
    type: UPDATE_24H_AGO_BOOLEAN,
    payload: false,
  });
};
