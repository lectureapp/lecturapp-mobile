import {
  GET_STADISTICS_SUCCEEDED,
  GET_STADISTICS_LOADING,
  GET_STADISTICS_FAILED,
} from '../types/stadisticsTypes';

import APIBackend from '../../axios/axios';

export const getStadistics = () => async (distpatch, getState) => {
  const {session_token} = getState().AuthReducer;

  distpatch({type: GET_STADISTICS_LOADING});

  try {
    const response = await APIBackend.get('/profile/stadistics', {
      headers: {Authorization: 'bearer ' + session_token},
    });

    console.log('response stadistics: ', response.data);

    distpatch({
      type: GET_STADISTICS_SUCCEEDED,
      payload: {
        today_tales_readed: response.data.stadistics.today.today_tales_readed,
        hit_percentaje_today:
          response.data.stadistics.today.hit_percentaje_today,

        hit_percentaje_week: response.data.stadistics.week.hit_percentaje_week,
        week_tales_readed: response.data.stadistics.week.week_tales_readed,
        total_tales: response.data.stadistics.total_tales,
      },
    });
  } catch (error) {
    distpatch({type: GET_STADISTICS_FAILED, payload: error.message});
    console.log('EL ERROR ES: ', error);
  }
};
