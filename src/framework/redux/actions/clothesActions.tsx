//import APIBackend from '../../axios/axios';
import API from '../../axios/axios';

import {
  // get all clothes:
  GET_ALL_CLOTHES_LOADING,
  GET_ALL_CLOTHES_SUCCEEDED,
  GET_ALL_CLOTHES_FAILED,

  // Buy cloth
  BUY_CLOTH_LOADING,
  BUY_CLOTH_SUCCEEDED,
  BUY_CLOTH_FAILED,
  BUY_CLOTH_IDLE,

  // use clothes:
  USE_CLOTHES_LOADING,
  USE_CLOTHES_SUCCEEDED,
  USE_CLOTHES_FAILED,
  USE_CLOTHES_IDLE,

  //
  BUY_CLOTH_NOT_ENOUGH_COINS,
  GET_ALL_CLOTHES_THAT_EXIST,
  //
  GET_ALL_CLOTHES_THAT_EXIST_FAILED,
  GET_ALL_CLOTHES_THAT_EXIST_IDLE,
  GET_ALL_CLOTHES_THAT_EXIST_SUCCEEDED,
  GET_ALL_CLOTHES_THAT_EXIST_LOADING,
} from '../types/clothesTypes';

import {UPDATE_COINS_FROM_LOGIN} from '../types/walletTypes';

// TODAVIA NO ESTA IMPLEMENTADO:
//export const getAllClothes = ({}) => async (distpatch) => {
////
//distpatch({type: GET_ALL_CLOTHES_LOADING});

//try {
//const response = await API.post('/outfit', {
////name,
////email,
////password,
//});

//distpatch({
//type: GET_ALL_CLOTHES_SUCCEEDED,
//payload: {
////name: response.data.name,
////email: response.data.email,
////token: response.data.access_token,
//},
//});
//} catch (error) {
//distpatch({
//type: GET_ALL_CLOTHES_FAILED,
//payload: error.message,
//});
//}
//};

export const buyCloth =
  (title: any, outfit_id: string, session_token: string) =>
  async (distpatch, getState) => {
    //
    // const {session_token} = getState().AuthReducer;
    distpatch({type: BUY_CLOTH_LOADING});

    let cloth_name: string = '';
    if (title == 'Vaquero') cloth_name = 'COWBOY';
    else if (title == 'Terno') cloth_name = 'TUXEDO';
    else if (title == 'Astronauta') cloth_name = 'ASTRONAUT';

    try {
      const response = await API.post(
        '/avatar/buy_outfit',
        {
          set_name: cloth_name,
          outfitId: outfit_id,
        },
        {
          headers: {
            Authorization: 'bearer ' + session_token,
            'content-type': 'application/json',
          },
        },
      );
      //response.data.avatar.avatar_sets; // todos los sets del usuario
      //response.data.avatar.current_style; // estilo actual
      //response.data.wallet.total_coins; // dinero despues de comprar
      if (response.data.message) {
        // the user doesn't have enough coins:
        distpatch({
          type: BUY_CLOTH_NOT_ENOUGH_COINS,
          payload: response.data.message,
        });
      } else {
        distpatch({
          type: BUY_CLOTH_SUCCEEDED,
          payload: response.data.avatar.avatar_sets,
        });
        distpatch({
          type: UPDATE_COINS_FROM_LOGIN,
          payload: response.data.wallet.total_coins,
        });
      }
    } catch (error) {
      // console.log(error.message);
      distpatch({
        type: BUY_CLOTH_FAILED,
        payload: error.message,
      });
    } finally {
      distpatch({
        type: BUY_CLOTH_IDLE,
      });
    }
  };

export const getAllClothesThatExist = () => async (distpatch, getState) => {
  //
  try {
    // TODO: el backend debe modificar este endpoint para que requiera
    // que le enviemos el session_token.

    distpatch({
      type: GET_ALL_CLOTHES_THAT_EXIST_LOADING,
    });
    const response = await API.get('/outfit');

    const new_list_of_all_cloth: [{}] = response.data.map((element) => {
      return {
        _id: element._id,
        outfit_name: element.outfit_name,
        price: element.price,
      };
    });

    distpatch({
      type: GET_ALL_CLOTHES_THAT_EXIST_SUCCEEDED,
      payload: new_list_of_all_cloth,
    });
  } catch (error) {
    // console.log(error.message);
    distpatch({
      type: GET_ALL_CLOTHES_THAT_EXIST_FAILED,
      payload: error.message,
    });
  } finally {
    distpatch({
      type: GET_ALL_CLOTHES_THAT_EXIST_IDLE,
    });
  }
};

export const equipCloth =
  (outfit_id: string, session_token: string) => async (distpatch, getState) => {
    //
    // const {session_token} = getState().AuthReducer;

    distpatch({
      type: USE_CLOTHES_LOADING,
    });

    try {
      const response = await API.patch(
        '/avatar/equip_outfit',
        {
          outfitId: outfit_id,
        },
        {
          headers: {
            Authorization: 'bearer ' + session_token,
            'content-type': 'application/json',
          },
        },
      );
      distpatch({
        // type: USE_CLOTHES,
        type: USE_CLOTHES_SUCCEEDED,
        payload: response.data.current_style,
      });
    } catch (error) {
      //console.log(error.message);
      distpatch({
        type: USE_CLOTHES_FAILED,
        payload: error.message,
      });
    } finally {
      distpatch({
        type: USE_CLOTHES_IDLE,
      });
    }
  };
