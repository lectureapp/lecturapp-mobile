import {
  SET_DOTS_PER_LINE,
  CHANGE_DOTS_ACTIVATED,
  CHANGE_DOTS_SIZE,
} from '../types/configTalesTypes';

export const setDotsPerLineAction = (dotsPerLine) => (distpatch) => {
  distpatch({
    type: SET_DOTS_PER_LINE,
    payload: dotsPerLine,
  });
};

export const changeDotsActivated = () => (distpatch) => {
  distpatch({
    type: CHANGE_DOTS_ACTIVATED,
  });
};

export const changeSizeDots = (size) => (distpatch) => {
  distpatch({
    type: CHANGE_DOTS_SIZE,
    payload: size,
  });
};
