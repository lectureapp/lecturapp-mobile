import {
  GET_AVAILABLE_VIDEOS_LOADING,
  GET_AVAILABLE_VIDEOS_FAILED,
  GET_AVAILABLE_VIDEOS_SUCCEEDED,
  BUY_VIDEO_TIME_LOADING,
  BUY_VIDEO_TIME_FAILED,
  BUY_VIDEO_TIME_SUCCEEDED,
  ONLY_SET_CURRENT_VIDEO_TO_SHOW,
  BUY_VIDEO_TIME_RETURN_IDLE,
} from '../types/availableVideosTypes';

import {UPDATE_COINS} from '../types/walletTypes';

import API from '../../axios/axios';
import {current} from 'immer';

export const getAvailableVideos = (session_token) => async (distpatch: any) => {
  //
  distpatch({type: GET_AVAILABLE_VIDEOS_LOADING});

  try {
    const response = await API.get('/profile/videos', {
      headers: {
        Authorization: 'Bearer ' + session_token,
        'content-type': 'application/json',
      },
    });

    //console.log('get available videos: ', response.data);
    // response.data es un array de objetos

    distpatch({
      type: GET_AVAILABLE_VIDEOS_SUCCEEDED,
      payload: response.data,
    });
  } catch (error) {
    // console.log('get available videos: ', error.message);
    distpatch({
      type: GET_AVAILABLE_VIDEOS_FAILED,
      payload: error.message,
    });
  }
};

export const buyVideoTime =
  (session_token, video_mongoID) => async (dispatch: any) => {
    //
    dispatch({type: BUY_VIDEO_TIME_LOADING});

    try {
      const response = await API.post(
        '/profile/updatevideo',
        {
          videoId: video_mongoID,
          coins: 5,
        },
        {
          headers: {
            Authorization: 'Bearer ' + session_token,
            'content-type': 'application/json',
          },
        },
      );

      const video_being_watching_now = response.data.data.find(
        (element: any) => element._videoId == video_mongoID,
      );

      dispatch({
        type: BUY_VIDEO_TIME_SUCCEEDED,
        payload: response.data,
      });

      dispatch({
        type: ONLY_SET_CURRENT_VIDEO_TO_SHOW,
        payload: video_being_watching_now,
      });

      dispatch({
        type: UPDATE_COINS,
        payload: response.data.wallet.total_coins,
      });
    } catch (error) {
      // console.log('buyVideoTime thunk error: ', error.message);
      dispatch({
        type: BUY_VIDEO_TIME_FAILED,
        payload: error.message,
      });
    } finally {
      dispatch({type: BUY_VIDEO_TIME_RETURN_IDLE});
    }
  };
// BUY_VIDEO_TIME_RETURN_IDLE
// export const buyVideoTimeReturnIdle = () => async (dispatch: any) => {
//   dispatch({type: BUY_VIDEO_TIME_RETURN_IDLE});
// };

export const setCurrentVideoToShow =
  (current_video) => async (dispatch: any) => {
    dispatch({
      type: ONLY_SET_CURRENT_VIDEO_TO_SHOW,
      payload: current_video,
    });
  };
