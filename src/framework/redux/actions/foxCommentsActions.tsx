import {SET_CURRENT_FOX_COMMENT} from '../types/foxCommentsTypes';

export const setCurrentFoxComment = () => (distpatch, getState) => {
  const {fox_comments} = getState().foxCommentsReducer;

  var random_comment =
    fox_comments[Math.floor(Math.random() * fox_comments.length)];
  //console.log('random comment: ', random_comment);

  distpatch({
    type: SET_CURRENT_FOX_COMMENT,
    payload: random_comment,
  });
};
