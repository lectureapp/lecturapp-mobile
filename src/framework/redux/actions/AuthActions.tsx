import {
  //login:
  REGISTER_SUCCEEDED,
  REGISTER_LOADING,
  REGISTER_FAILED,

  //login:
  LOGIN_SUCCEEDED,
  LOGIN_LOADING,
  LOGIN_FAILED,

  // logout:
  LOGOUT,
} from '../types/AuthTypes';

import {UPDATE_USER_CLOTHES, USE_CLOTHES} from '../types/clothesTypes';
import {UPDATE_COINS_FROM_LOGIN} from '../types/walletTypes';

import API from '../../axios/axios';

export const registerUser =
  ({email, password, name}: any) =>
  async (distpatch: any) => {
    //
    distpatch({type: REGISTER_LOADING});

    try {
      const response = await API.post('/user', {
        name,
        email,
        password,
      });

      if (response.name) {
        distpatch({
          type: REGISTER_SUCCEEDED,
          payload: {
            name: response.name,
            email: response.email,
            token: response.access_token,
          },
        });

        distpatch({
          type: UPDATE_USER_CLOTHES,
          payload: [],
        });

        distpatch({
          type: USE_CLOTHES,
          payload: '',
        });

        distpatch({
          type: UPDATE_COINS_FROM_LOGIN,
          payload: 0,
        });
      }

      // TODO: creo que falta actualizar los cuentos favoritos.
    } catch (error) {
      distpatch({
        type: REGISTER_FAILED,
        payload: error.message,
      });
    }
  };

export const loginUser = (props: any) => async (distpatch: any) => {
  //
  distpatch({type: LOGIN_LOADING});

  try {
    const response = await API.post('/auth/login', {
      username: props.username,
      password: props.password,
    });

    if (response.data.data.user.access_token) {
      // if (response.data.user.access_token) {
      distpatch({
        type: UPDATE_USER_CLOTHES,
        payload: response.data.avatar.avatar_sets,
        // payload: response.avatar.avatar_sets,
      });

      distpatch({
        type: USE_CLOTHES,
        payload: response.data.avatar.current_style,
        // payload: response.avatar.current_style,
      });

      distpatch({
        type: UPDATE_COINS_FROM_LOGIN,
        payload: response.data.wallet.total_coins,
        // payload: response.wallet.total_coins,
      });

      //navigation.navigate('DrawerContainer');
      distpatch({
        type: LOGIN_SUCCEEDED,
        payload: response.data,
      });
    }
  } catch (error) {
    distpatch({
      type: LOGIN_FAILED,
      //TODO: Se debería distinguir varios tipos de errores,
      //como: conoxión o credenciales incorrectas.
      //payload: error.message,
      payload: 'Los datos son incorrectos.',
    });
  }
};

export const logout = () => async (distpatch) => {
  //
  distpatch({type: LOGOUT});
  // TODO: Tendríamos que mandar una petición
  // al backend para que elimine el token que nos
  // dió, pero ese endpoint todavía no está implementado.
};
