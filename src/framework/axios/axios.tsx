import axios from 'axios';

//axios.get('https://pokeapi.co/api/v2/pokemon/ditto').then(({result}: any) => {
//console.log(result);
//});

export default axios.create({
  //export const APIBackend = axios.create({
  //baseURL: 'http://3.223.63.194:3000/',

  // url para testear en local:
  //baseURL: 'http://localhost:3000/', // *Con local host no funciona.
  baseURL: 'http://192.168.0.106:3000/',
  headers: {
    //Authorization: 'Basic xxxxxxxxxxxxxxxxxxx',
    accept: 'application/json',
    'Content-type': 'application/json',
    //'Content-type': 'text/plain',
  },
});
