import {DripsyProvider} from 'dripsy';
import React from 'react';

// TODO:  dripsy todavía está amarrado a react, sin embargo, estamos
// tratando de cumplir el principio S de SOLID, este archivo solo
// se encarga de implementar dripsy.

const DripsyImplementation = ({children}: any) => {
  const theme: any = {
    colors: {
      textPrimary: '#000',
      textSecondary: '#000',
      background: '#fff',
      primary: 'green',
      menu: '#1982C4',
    },
    fonts: {
      body: 'Satisfy-Regular',
      heading: '"Avenir Next", sans-serif',
    },
    sizes: [150, 240, 300, 350],
    fontSizes: [18, 22, 27],
    space: [60, 12, 10, 20],
    text: {
      primary: {
        fontSize: [28, 31, 35],
        color: 'textPrimary',
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
      },
      secondary: {
        fontSize: [20, 23, 28],
        color: 'textPrimary',
        textAlign: 'center',
        margin: 7,
      },
      normal: {
        fontSize: [15, 18, 23],
        color: 'textPrimary',
        textAlign: 'center',
        margin: 7,
      },
    },
  };

  return <DripsyProvider theme={theme}>{children}</DripsyProvider>;
};

export default DripsyImplementation;
