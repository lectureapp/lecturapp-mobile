import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {StyleSheet} from 'react-native';

//Navigation TS
import {
  ITales,
  //} from '../../../../../assets/utilities/TalesTypes';
} from '../../../../../../assets/utilities/TalesTypes';

import {buttonColor} from '../globalStyles/globalStyles';
import {View, Text} from 'dripsy';

interface TaleItemProps {
  item: ITales;
  goTaleView: (item: ITales) => void;
}

const TaleListItem = ({item, goTaleView}: TaleItemProps) => {
  return (
    <TouchableOpacity
      style={styles.taleButton}
      testID="tale_list_item_id"
      onPress={() => {
        goTaleView(item);
      }}>
      <Text
        variant="secondary"
        style={{color: 'white', flex: 6, textAlign: 'left', marginLeft: 20}}>
        {item.title}
      </Text>

      {item.times_read && ( // esto permitirá indicar que cuentos diarios están terminados
        <View style={styles.completeTag}>
          <Text variant="normal" style={{color: 'white'}}>
            Completado
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  completeTag: {
    flex: 3,
    backgroundColor: '#469A51',
    height: 35,
    borderRadius: 8,
    marginRight: 15,
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  taleButton: {
    marginHorizontal: 15,
    marginVertical: 15,
    backgroundColor: buttonColor,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    height: 72,
  },
});

export default TaleListItem;
