export const menuColor = '#1982C4'; // color azul oscuro
export const buttonColor = '#6A4C93'; // color morado

export const buttonStyle = {
  backgroundColor: '#6A4C93',
  margin: 10,
  borderRadius: 10,
  padding: 4,
  alignItems: 'center',
  justifyContent: 'center',
  // shadow's config:
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.3,
  shadowRadius: 7.65,
  elevation: 4,
};

export const buttonStyle2 = {
  backgroundColor: '#1982C4',
  margin: 10,
  borderRadius: 10,
  padding: 4,
  // shadow's config:
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.3,
  shadowRadius: 7.65,
  elevation: 4,
};

export const coinColor = '#DCE000'; // color morado
