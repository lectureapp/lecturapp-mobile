import React from 'react';
import 'react-native-gesture-handler';
import {createDrawerNavigator} from '@react-navigation/drawer';

//import ProfileView from '../../../screen/DrawerScreens/ProfileView';
import ProfileView from '../../../screens/DrawerScreens/ProfileView';
//import StadisticsView from '../../../screen/DrawerScreens/StadisticsScreens/StadisticsView';
import StadisticsView from '../../../screens/DrawerScreens/StadisticsScreens/StadisticsView';
import TabMenuContainer from '../../TabMenuNavigation/TabMenuContainer';
import {HeaderBackground} from '@react-navigation/stack';
import StadisticsStack from '../StadisticsStack/StadisticsStack';

//import PaymentScreen from '../../../screen/DrawerScreens/PaymentScreen';
import PaymentScreen from '../../../screens/DrawerScreens/PaymentScreen';

import LogOutScreen from '../../../screens/DrawerScreens/LogOutScreen';
import {menuColor} from '../../../globalStyles/globalStyles';
import IndexDrawerNavigator from '../../../screens/DrawerScreens/IndexDrawerNavigator';

import Icon from 'react-native-vector-icons/FontAwesome';

const Drawer = createDrawerNavigator();

const DrawerContainer = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerStyle: {backgroundColor: menuColor},
        headerTitleStyle: {color: 'white'},
      }}
      drawerContent={(props) => <IndexDrawerNavigator {...props} />}>
      <Drawer.Screen
        name="TabMenuContainer"
        component={TabMenuContainer}
        options={{
          headerShown: true,
          title: 'LecturApp',
          drawerIcon: ({focused, size}) => (
            <Icon name="book" size={30} color="black" style={{width: 30}} />
          ),
        }}
      />
      <Drawer.Screen
        name="StadisticsStack"
        component={StadisticsStack}
        options={{
          headerShown: true,
          title: 'Estadisticas',
          drawerIcon: ({focused, size}) => (
            <Icon
              name="bar-chart-o"
              size={25}
              color="black"
              style={{width: 30}}
            />
          ),
        }}
      />
      <Drawer.Screen
        name="ProfileView"
        component={ProfileView}
        options={{
          headerShown: true,
          title: 'Perfil',
          drawerIcon: ({focused, size}) => (
            <Icon name="user" size={30} color="black" style={{width: 30}} />
          ),
        }}
      />
      {/* <Drawer.Screen
        name="PaymentScreen"
        component={PaymentScreen}
        options={{
          headerShown: true,
          title: 'Comprar',
          drawerIcon: ({focused, size}) => (
            <Icon name="user" size={30} color="black" />
          ),
        }}
          />*/}
      {/*<Drawer.Screen
        name="LogOutScreen"
        component={LogOutScreen}
        options={{
          headerShown: true,
          title: 'Cerrar sesión',
          drawerIcon: ({focused, size}) => (
            <Icon name="close" size={30} color="black" style={{width: 30}} />
          ),
        }}
      />*/}
    </Drawer.Navigator>
  );
};

export default DrawerContainer;
