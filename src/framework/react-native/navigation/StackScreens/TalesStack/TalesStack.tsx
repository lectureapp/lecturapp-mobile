import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';

import DailyView from '../../../screens/tabMenu/home/DailyView/DailyView';
import TaleView from '../../../screens/tabMenu/home/TaleView/TaleView';

import IndexQuestionsView from '../../../screens/tabMenu/home/QuestionsView/IndexQuestionsView';
import IndexQualificationView from '../../../screens/tabMenu/home/QualificationView/IndexQualificationView';
import RewardView from '../../../screens/tabMenu/home/RewardView/RewardView';

import React from 'react';

import {menuColor} from '../../../globalStyles/globalStyles';

const TalesStack = () => {
  const StackTales = createStackNavigator();
  return (
    <StackTales.Navigator>
      <StackTales.Screen
        name="DailyView"
        component={DailyView}
        options={{
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackTales.Screen
        name="TaleView"
        component={TaleView}
        options={{
          title: '',
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackTales.Screen
        name="QuestionsView"
        component={IndexQuestionsView}
        options={{
          title: '',
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackTales.Screen
        name="IndexQualificationView"
        component={IndexQualificationView}
        options={{
          headerShown: false,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackTales.Screen
        name="RewardView"
        component={RewardView}
        options={{
          headerShown: false,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
    </StackTales.Navigator>
  );
};

export default TalesStack;
