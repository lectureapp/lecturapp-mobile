import React from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';

import {menuColor} from '../../../globalStyles/globalStyles';

// importing views:
import IndexAvailableVideosView from '../../../screens/tabMenu/inventory/AvailableVideos/IndexAvailableVideosView';
import IndexDresserView from '../../../screens/tabMenu/inventory/DresserView/IndexDresserView';
// import WatchingVideoView from '../../../screens/tabMenu/inventory/WatchingVideoView';
import WatchingVideoView from '../../../screens/tabMenu/inventory/WatchingVideo/WatchingVideoView';

const StackInventory = createStackNavigator();

const InventoryStack = () => {
  return (
    <StackInventory.Navigator>
      <StackInventory.Screen
        name="Videos disponibles"
        component={IndexAvailableVideosView}
        options={{
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackInventory.Screen
        name="Vestidor"
        component={IndexDresserView}
        options={{
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
      <StackInventory.Screen
        name="Video"
        component={WatchingVideoView}
        options={{
          headerShown: true,
          headerStyle: {backgroundColor: menuColor},
          headerTintColor: 'white',
        }}
      />
    </StackInventory.Navigator>
  );
};

export default InventoryStack;
