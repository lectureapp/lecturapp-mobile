import React from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';

//import LoginView from '../../../screen/AuthenticationScreens/LoginView';
import LoginView from '../../../screens/AuthenticationScreens/LoginView';

//import RegisterView from '../../../screen/AuthenticationScreens/RegisterView';
import RegisterView from '../../../screens/AuthenticationScreens/RegisterView';

const StackAuthentication = createStackNavigator();

const AuthenticationStack = () => {
  return (
    <StackAuthentication.Navigator>
      <StackAuthentication.Screen
        name="LoginView"
        component={LoginView}
        options={{
          headerShown: false,
        }}
      />
      <StackAuthentication.Screen
        name="RegisterView"
        component={RegisterView}
        options={{
          headerShown: true,
          headerTitle: '',
          headerStyle: {
            backgroundColor: '#1982C4',
          },
        }}
      />
    </StackAuthentication.Navigator>
  );
};

export default AuthenticationStack;
