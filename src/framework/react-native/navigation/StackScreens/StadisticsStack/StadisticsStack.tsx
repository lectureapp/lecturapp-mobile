import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

//import StadisticsView from '../../../screen/DrawerScreens/StadisticsScreens/StadisticsView';
//import StadisticsDetailsView from '../../../screen/DrawerScreens/StadisticsScreens/StadisticsDetailsView';
//import StadisticsRateView from '../../../screen/DrawerScreens/StadisticsScreens/StadisticsRateView';

import IndexStadisticsView from '../../../screens/DrawerScreens/StadisticsScreens/IndexStadisticsView';

import StadisticsDetailsView from '../../../screens/DrawerScreens/StadisticsScreens/StadisticsDetailsView';
import StadisticsRateView from '../../../screens/DrawerScreens/StadisticsScreens/StadisticsRateView';

//Import Views

const Stack = createStackNavigator();

const StadisticsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="StadisticsView"
        component={IndexStadisticsView}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="StadisticsDetailView"
        component={StadisticsDetailsView}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="StadisticsRateView"
        component={StadisticsRateView}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default StadisticsStack;
