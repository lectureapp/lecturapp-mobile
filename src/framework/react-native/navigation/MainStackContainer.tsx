import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import AuthenticationStack from './StackScreens/AuthenticationStack/AuthenticationStack';

import TalesStack from './StackScreens/TalesStack/TalesStack';
import InventoryStack from './StackScreens/InventoryStack/InventoryStack';

import DrawerContainer from './StackScreens/DrawerMenuNavigation/DrawerContainer';

import StadisticsStack from './StackScreens/StadisticsStack/StadisticsStack';

const Stack = createStackNavigator();

const MainStackContainer = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="AuthenticationStack"
          component={AuthenticationStack}
          options={{
            headerShown: false,
          }}
        />

        {/*<Stack.Screen
          name="TabMenuContainer"
          component={TabMenuContainer}
          options={({navigation}: any) => ({
            title: 'Lectura App',
            headerTintColor: 'white',
            headerStyle: {
              backgroundColor: '#3376CE',
            },
            //headerLeft: () => (
            //<Button
            //onPress={() => navigation.openDrawer()}
            //title="Info"
            //color="#fff"
            ///>
            //),
          })}
          />*/}

        <Stack.Screen
          name="DrawerContainer"
          component={DrawerContainer}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="TalesStack"
          component={TalesStack}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="StadisticsStack"
          component={StadisticsStack}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="InventoryStack"
          component={InventoryStack}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainStackContainer;
