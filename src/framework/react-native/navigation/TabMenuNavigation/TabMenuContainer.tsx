import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

// Tab Menu Options (Screens)
import IndexHomeView from '../../screens/tabMenu/home/IndexHomeView';
import FavoritesView from '../../screens/tabMenu/favorites/FavoritesView';
import IndexInventoryView from '../../screens/tabMenu/inventory/IndexInventoryView';
import MarketView from '../../screens/tabMenu/market/MarketView';
import {menuColor} from '../../globalStyles/globalStyles';

import {View, Text, createThemedComponent} from 'dripsy';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';

const Tab = createMaterialTopTabNavigator();

const TabMenuContainer = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: 'black',
        inactiveTintColor: 'white',
        indicatorStyle: {
          backgroundColor: 'white',
        },
        style: {
          backgroundColor: menuColor,
        },
      }}
      // screenOptions={{
      //   activeTintColor: 'blue',
      // }}>
    >
      <Tab.Screen
        name="Home"
        options={{
          tabBarLabel: () => (
            <MaterialCommunityIcons name="home" size={30} color="white" />
          ),
          lazy: true,
        }}
        component={IndexHomeView}
      />
      <Tab.Screen
        name="Favorites"
        component={FavoritesView}
        options={{
          tabBarLabel: () => <Icon name="book" size={30} color="white" />,
        }}
      />
      <Tab.Screen
        name="Inventory"
        component={IndexInventoryView}
        options={{
          tabBarLabel: () => (
            <MaterialCommunityIcons name="archive" size={30} color="white" />
          ),
        }}
      />
      {/* <Tab.Screen name="Mercado" component={MarketView} />*/}
    </Tab.Navigator>
  );
};

export default TabMenuContainer;
