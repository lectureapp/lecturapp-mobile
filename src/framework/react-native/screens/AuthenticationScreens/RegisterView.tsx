import React, {useState, useEffect} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Alert,
  Modal,
  Button,
  TouchableOpacity,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {View, Text, ScrollView} from 'dripsy';

import {useDispatch, useSelector} from 'react-redux';
import {registerUser} from '../../../redux/actions/AuthActions';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {Formik, useFormikContext, useField} from 'formik';
import * as Yup from 'yup';

interface LoginViewProps {}

const RegisterView: React.FC<LoginViewProps> = ({navigation}: any) => {
  const AuthState = useSelector((state: any) => state.AuthReducer);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible]: any = useState(false);

  return (
    <View
      sx={{
        paddingTop: [60, 20, 50],
      }}
      style={LoginStyle.Container}>
      <Text variant="primary">Registrar cuenta nueva</Text>
      <Text variant="normal">Complete los siguientes datos</Text>

      <Formik
        initialValues={{nombre: '', email: '', password1: '', password2: ''}}
        onSubmit={(values) => {
          if (values.password1 == values.password2) {
            dispatch(
              registerUser({
                email: values.email,
                password: values.password1,
                name: values.nombre,
              }),
            );
            setModalVisible(true);
          }
        }}
        validationSchema={Yup.object({
          nombre: Yup.string()
            .required('Requerido')
            .min(2, 'El nombre es muy corto'),
          email: Yup.string().email('Correo inválido').required('Requerido'),
          password1: Yup.string()
            .required('La contraseña es requerida')
            .min(5, 'La contraseña debe tener al menos 5 caracteres.'),
          password2: Yup.string()
            .required('La contraseña es requerida')
            .min(5, 'La contraseña debe tener al menos 5 caracteres.'),
        })}>
        <RegistrationForm navigation={navigation} />
      </Formik>

      <View style={{flexDirection: 'row', margin: 15}}>
        <Text variant="normal">¿Ya tiene una cuenta?</Text>
        <Text
          variant="normal"
          style={{color: '#368BFB'}}
          onPress={() => {
            navigation.navigate('LoginView');
          }}>
          ¡Iniciar sesión aquí!
        </Text>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <ModalPopUp
          setModalVisible={setModalVisible}
          modalVisible={modalVisible}
          AuthState={AuthState}
          navigation={navigation}
        />
      </Modal>
    </View>
  );
};

const RegistrationForm = ({navigation}: any) => {
  const {submitForm} = useFormikContext();

  return (
    <>
      <MyInput
        fieldName="nombre"
        placeholder="Ingrese su nombre"
        style={LoginStyle.inputs}
      />
      <MyInput
        fieldName="email"
        placeholder="Ingrese su email"
        style={LoginStyle.inputs}
      />

      <MyPassInput
        fieldName="password1"
        placeholder="Confirme su contraseña"
        style={{flex: 6}}
      />
      <MyPassInput
        fieldName="password2"
        placeholder="Confirme su contraseña"
        style={{flex: 6}}
      />

      <TouchableOpacity onPress={submitForm} style={LoginStyle.button}>
        <Text style={{fontSize: 20, color: 'white'}}>Crear cuenta</Text>
      </TouchableOpacity>
    </>
  );
};

const MyInput = ({fieldName, ...props}) => {
  const [field, meta] = useField(fieldName);
  return (
    <>
      <TextInput
        value={field.value}
        onChangeText={field.onChange(fieldName)}
        onBlur={field.onBlur(fieldName)}
        {...props}
      />
      {meta.error && meta.touched && (
        <Text style={{color: '#fc4737', marginTop: -5, marginBottom: 5}}>
          {meta.error}
        </Text>
      )}
    </>
  );
};

const MyPassInput = ({fieldName, ...props}) => {
  const [field, meta] = useField(fieldName);

  const [isHiddenPass, setIsHiddenPass] = useState(true);
  const [eyeColor, setEyeColor] = useState('gray');

  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#F4F3F3',
          width: 300,
          height: 40,
          borderRadius: 15,
          paddingStart: 15,
          margin: 10,
        }}>
        <TextInput
          value={field.value}
          onChangeText={field.onChange(fieldName)}
          onBlur={field.onBlur(fieldName)}
          secureTextEntry={isHiddenPass}
          {...props}
        />
        <TouchableOpacity
          style={{alignSelf: 'center', flex: 1}}
          onPress={() => {
            setIsHiddenPass(!isHiddenPass);
            if (eyeColor == 'black') setEyeColor('gray');
            else setEyeColor('black');
          }}>
          <MaterialCommunityIcons name="eye" size={30} color={eyeColor} />
        </TouchableOpacity>
      </View>
      {meta.error && meta.touched && (
        <Text style={{color: '#fc4737', marginBottom: 10}}>{meta.error}</Text>
      )}
    </>
  );
};

const ModalPopUp = ({AuthState, navigation, modalVisible, setModalVisible}) => {
  return (
    <View style={LoginStyle.centeredView}>
      <View style={LoginStyle.modalView}>
        {AuthState.registration_state == 'loading' && <Text>CARGANDO...</Text>}
        <Text style={LoginStyle.modalText}>
          {AuthState.registration_message}
        </Text>
        <TouchableOpacity
          style={[LoginStyle.button2, LoginStyle.buttonClose]}
          onPress={() => {
            setModalVisible(!modalVisible);
            if (AuthState.registration_state == 'idle')
              navigation.navigate('DrawerContainer');
          }}>
          <Text style={LoginStyle.textStyle}>Aceptar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterView;

const LoginStyle = StyleSheet.create({
  ImageBackground: {
    flex: 1,
    alignItems: 'center',
  },
  Container: {
    alignItems: 'center',
    backgroundColor: 'white',
    flex: 1,
  },
  inputs: {
    backgroundColor: '#F4F3F3',
    width: 300,
    height: 40,
    borderRadius: 15,
    paddingStart: 15,
    margin: 10,
  },
  text: {
    paddingRight: 35,
    color: 'red',
    fontSize: 19,
    fontFamily: 'Satisfy-Regular',
  },
  button: {
    borderRadius: 15,
    backgroundColor: '#31E981',
    alignItems: 'center',
    padding: 6,
    margin: 7,
    marginTop: 30,
    height: 40,
    width: 300,
  },

  // MODAL:
  // -----------------------------------------------------
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
