import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {View, Text} from 'dripsy';

import {useDispatch, useSelector} from 'react-redux';
import {loginUser} from '../../../redux/actions/AuthActions';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {Formik, useFormikContext, useField} from 'formik';
import * as Yup from 'yup';

interface LoginViewProps {}

const LoginView: React.FC<LoginViewProps> = ({navigation}: any) => {
  const AuthState = useSelector((state: any) => state.AuthReducer);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    // console.log(
    //   'authstate en pantalla login: ',
    //   JSON.stringify(AuthState, null, 4),
    // );

    if (AuthState.session_token != '') {
      navigation.navigate('DrawerContainer');
    }
  }, []);

  return (
    <View
      sx={{
        padding: [0, 1, 2, 3],
      }}
      style={LoginStyle.Container}>
      <Formik
        // TODO: dejar en blanco los initialValues:
        initialValues={{email: 'user1@gmail.com', password: '1234567'}}
        onSubmit={(values) => {
          const username: string = values.email;
          const password: string = values.password;

          dispatch(loginUser({username, password}));
          setModalVisible(true);
        }}
        validationSchema={Yup.object({
          email: Yup.string().email('Correo inválido').required('Requerido'),
          password: Yup.string()
            .required('La contraseña es requerida')
            .min(5, 'La contraseña debe tener al menos 5 caracteres.'),
        })}>
        <LoginForm navigation={navigation} />
      </Formik>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={LoginStyle.centeredView}>
          <View style={LoginStyle.modalView}>
            {AuthState.login_state == 'loading' && <Text>CARGANDO...</Text>}

            <Text style={LoginStyle.modalText}>{AuthState.login_message}</Text>
            <TouchableOpacity
              style={[LoginStyle.button2, LoginStyle.buttonClose]}
              onPress={() => {
                setModalVisible(!modalVisible);
                if (AuthState.login_state == 'idle')
                  navigation.navigate('DrawerContainer');
              }}>
              <Text style={LoginStyle.textStyle}>Aceptar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const LoginForm = ({navigation}: any) => {
  const {submitForm} = useFormikContext();

  return (
    <>
      <Text variant="primary">Bienvenid@</Text>
      <Text variant="secondary">
        Usa tus credenciales para ingresar a tu cuenta
      </Text>

      <MyInput fieldName="email" placeholder="Ingrese su email" />

      <MyPassInput
        fieldName="password"
        placeholder="Confirme su contraseña"
        style={{flex: 6}}
      />
      <Text
        variant="normal"
        style={{color: '#368BFB'}}
        onPress={() => {
          // TODO: Implementar flujo para cuando el usuario se olvida la contraseña
          console.warn('Por implementar');
        }}>
        ¿Olvidó la contraseña?
      </Text>
      <TouchableOpacity onPress={submitForm} style={LoginStyle.LoginButton}>
        <Text style={{fontSize: 20, color: 'white'}}>Ingresar a mi cuenta</Text>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', margin: 15}}>
        <Text variant="normal">¿No tiene una cuenta?</Text>
        <Text
          variant="normal"
          style={{color: '#368BFB'}}
          onPress={() => {
            navigation.navigate('RegisterView');
          }}>
          Registrese aquí
        </Text>
      </View>
    </>
  );
};

const MyInput = ({fieldName, ...props}) => {
  const [field, meta] = useField(fieldName);
  return (
    <>
      <TextInput
        value={field.value}
        onChangeText={field.onChange(fieldName)}
        style={LoginStyle.textInput}
        onBlur={field.onBlur(fieldName)}
        {...props}
      />
      {meta.error && meta.touched && (
        <Text style={{color: '#fc4737'}}>{meta.error}</Text>
      )}
    </>
  );
};

const MyPassInput = ({fieldName, ...props}) => {
  const [field, meta] = useField(fieldName);

  const [isHiddenPass, setIsHiddenPass] = useState(true);
  const [eyeColor, setEyeColor] = useState('gray');

  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#F4F3F3',
          width: 300,
          height: 40,
          borderRadius: 15,
          paddingStart: 15,
          margin: 10,
        }}>
        <TextInput
          value={field.value}
          onChangeText={field.onChange(fieldName)}
          onBlur={field.onBlur(fieldName)}
          secureTextEntry={isHiddenPass}
          {...props}
        />
        <TouchableOpacity
          style={{alignSelf: 'center', flex: 1}}
          onPress={() => {
            setIsHiddenPass(!isHiddenPass);
            if (eyeColor == 'black') setEyeColor('gray');
            else setEyeColor('black');
          }}>
          <MaterialCommunityIcons name="eye" size={30} color={eyeColor} />
        </TouchableOpacity>
      </View>
      {meta.error && meta.touched && (
        <Text style={{color: '#fc4737', marginBottom: 10}}>{meta.error}</Text>
      )}
    </>
  );
};

export default LoginView;

const LoginStyle = StyleSheet.create({
  ImageBackground: {
    flex: 1,
    alignItems: 'center',
  },
  Container: {
    alignItems: 'center',
    backgroundColor: 'white',
    flex: 1,
  },
  text: {
    paddingRight: 35,
    width: 120,
    color: 'red',
    fontSize: 19,
    fontFamily: 'Satisfy-Regular',
  },
  textInput: {
    backgroundColor: '#F4F3F3',
    width: 300,
    height: 40,
    borderRadius: 15,
    paddingStart: 15,
    margin: 10,
  },

  LoginButton: {
    borderRadius: 15,
    backgroundColor: '#31E981',
    alignItems: 'center',
    padding: 6,
    margin: 7,
    marginTop: 30,
    height: 40,
    width: 300,
  },
  RegistrationButton: {
    borderRadius: 15,
    backgroundColor: '#7e4a35',
    alignItems: 'center',
    padding: 10,
    margin: 7,
    marginTop: 70,
  },

  // MODAL:
  // -----------------------------------------------------
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
