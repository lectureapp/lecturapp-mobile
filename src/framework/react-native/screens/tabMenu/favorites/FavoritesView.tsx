import React, {useState, useEffect} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {FlatList, ImageBackground} from 'react-native';

//Navigation TS
//import {StackNavigationProp} from '@react-navigation/stack';
import {
  TalesParamList,
  ITales,
  TalesProps,
} from '../../utilities/TalesTypes/TalesTypes.ts';

import {View, Text} from 'dripsy';

// REDUX
import {useSelector, useDispatch} from 'react-redux';
import {
  bringFavoriteTales,
  setTaleToWork,
} from '../../../../redux/actions/talesActions';

import TaleListItem from '../../../components/TaleListItem';

interface TalesViewProps {}

const FavoritesView: React.FC<TalesViewProps> = ({
  navigation,
}: TalesProps<'DailyView'>) => {
  const talesState = useSelector((state: any) => state.talesReducer);
  const authState = useSelector((state: any) => state.AuthReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(bringFavoriteTales(authState.session_token));
  }, []);

  const goTaleView = (taleSelected: ITales) => {
    dispatch(setTaleToWork(taleSelected));
    dispatch(bringFavoriteTales(authState.session_token));

    navigation.navigate('TalesStack', {
      screen: 'TaleView',
      params: {
        taleSelected: taleSelected,
        comeFromFavorites: true,
      },
    });
  };

  return (
    <ImageBackground
      source={require('../../../../../domain/assets/images/fondo1.png')}
      style={{flex: 1}}>
      <View style={{flex: 1}}>
        <Text testID="probando" variant="primary" style={{color: 'white'}}>
          Cuentos favoritos
        </Text>
        <FlatList
          data={talesState.favorites_tales}
          extraData={talesState.favorites_tales}
          renderItem={({item}) => (
            <TaleListItem item={item} goTaleView={goTaleView} />
          )}
          contentContainerStyle={{paddingBottom: 50}}
          keyExtractor={(item) => item._id + ''}
          testID="favorites_flatlist"
        />
      </View>
    </ImageBackground>
  );
};

export default FavoritesView;
