import React from 'react';

import {Provider} from 'react-redux';
import {
  makeMockStore,
  NavAndReduxWrapper,
} from '../../../../../../../__mocks__/utils';

// react native testing library:
import {render, cleanup} from '@testing-library/react-native';
import {NavigationContext} from '@react-navigation/native';
import renderer from 'react-test-renderer';

// Use with React Native <= 0.63
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock'),
); // This is needed so that <TabMenuContainer /> don't throw an error.

let component: any;

const navigation = {
  navigate: jest.fn(),
  setParams: jest.fn(),
  getParam: (myParam, defaultValue) => {
    return defaultValue;
  },
  useIsFocused: true,
};
// ---------------------------------------------------------------

import FavoritesView from '../FavoritesView';

describe('<FavoritesView />', () => {
  let store: any;

  beforeEach(() => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'idle',
        tales: [
          {_id: 1, title: 1},
          {_id: 2, title: 2},
          {_id: 3, title: 3},
          {_id: 4, title: 4},
        ],
        favorites_tales: [{_id: 1}, {_id: 2}],
      },
      AuthReducer: {
        session_token: '123',
      },
      clothesReducer: {
        current_cloth: 'DEFAULT',
      },
      foxCommentsReducer: {
        current_fox_comment: 'hola',
        fox_comments: [],
      },
    });
    // NOTA: si los datos de los reducers cambian el test
    // de snapshot de este elemento botará error.

    cleanup; // método que borra todos los render();
  });

  it('<FavoritesView /> render', async () => {
    const component = (
      <NavAndReduxWrapper store={store}>
        <FavoritesView />
      </NavAndReduxWrapper>
    );

    const {findByText, findByTestId, findAllByTestId} = render(component);
    const title = await findByText('Cuentos favoritos');
    const favorites_flatlist = await findByTestId('favorites_flatlist');
    const tale_list_item_id = await findAllByTestId('tale_list_item_id');

    expect(title).toBeTruthy();
    expect(favorites_flatlist).toBeTruthy();
    expect(tale_list_item_id).toHaveLength(2);

    // NOTA: en esta vista nos envía a TaleView de TalesStack pero todas
    // estas vistas ya están testeadas por lo que creo que
    // no es necesario hacer más.
  });

  it('snapshot <FavoritesView />', () => {
    const navContext = {
      isFocused: () => true,
      // addListener returns an unscubscribe function.
      addListener: jest.fn(() => jest.fn()),
    };

    const tree = renderer
      .create(
        <NavigationContext.Provider value={navContext}>
          <Provider store={store}>
            <FavoritesView navigation={navigation} />
          </Provider>
        </NavigationContext.Provider>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
