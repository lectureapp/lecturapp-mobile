import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
// import * as data from '../../../../assets/data/marketItems.json';
import * as data from '../../../../../domain/assets/data/marketItems.json';

interface MarketViewProps {}

const screen = Dimensions.get('screen');
var listaMensajes: [string] = [
  'Bienvenido de nuevo Luis, ¿Estas listo para tus divertidos cuentos diarios.',
  '¿Que deseas comprar hoy?',
  'Tengo muchos items que te gustarán!',
  'Hoy traje cuentos muy interesantes.',
];

const MarketView: React.FC<MarketViewProps> = ({}) => {
  const [marketItems, setMarketItems] = useState(data.marketItems);
  var randomMessage: string;
  randomMessage =
    listaMensajes[Math.floor(Math.random() * listaMensajes.length)];

  useEffect(() => {
    return () => {};
  }, []);

  const goTaleView = (item: ITales) => {
    navigation.navigate('TalesStack', {
      screen: 'TaleView',
      params: {item},
    });
  };

  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <ImageBackground
        style={Styles.fondo}
        source={require('../../../../assets/images/fondo1.png')}>
        <Image
          style={Styles.mascota}
          testID="pet_image_id"
          source={require('../../../../assets/images/mascota.png')}
        />
        <Image
          style={Styles.mensaje}
          source={require('../../../../assets/images/mensajeVacio.png')}
        />
        <Text
          style={{
            position: 'absolute',
            left: 225,
            top: 40,
            width: 180,
            fontSize: 20,
          }}>
          {randomMessage}
        </Text>
        <ImageBackground
          source={require('../../../../assets/images/estante1.png')}
          style={{
            height: screen.height / 2.75,
            resizeMode: 'stretch',
          }}>
          <FlatList
            data={marketItems}
            style={{marginHorizontal: 40, marginTop: -80}}
            renderItem={({item}) => (
              <>
                <TouchableOpacity
                  onPress={() => {
                    goTaleView(item);
                  }}>
                  {/* <Text>{item.titulo}</Text>*/}
                  <Image
                    source={{uri: item.path}}
                    style={{
                      width: screen.width / 6,
                      height: screen.height / 6,
                      resizeMode: 'stretch',
                      margin: 20,
                    }}
                  />
                </TouchableOpacity>
              </>
            )}
            contentContainerStyle={{paddingBottom: 50}}
            //keyExtractor={(item) => item.id.toString()}
            numColumns={3}
          />
        </ImageBackground>
      </ImageBackground>
    </View>
  );
};

const Styles = StyleSheet.create({
  fondo: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  mascota: {
    //marginTop: 100,
    //marginLeft: 50,
    position: 'absolute',
    left: 50,
    top: 100,
    //height: 0,
  },
  mensaje: {
    position: 'absolute',
    left: 180,
    top: 10,
    //marginTop: 0,
    //marginLeft: 200,
  },
  button: {
    borderRadius: 15,
    backgroundColor: '#368BFB',
    alignItems: 'center',
    marginTop: 30,
    padding: 10,
    marginHorizontal: 70,
  },
});

export default MarketView;
