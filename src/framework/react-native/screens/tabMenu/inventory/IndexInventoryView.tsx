import React, {useState, useEffect} from 'react';

import {TouchableOpacity, Image, StyleSheet} from 'react-native';

//Navigation TS
//import {StackNavigationProp} from '@react-navigation/stack';
//import {TalesParamList, ITales} from '../../utilities/TalesTypes/TalesTypes.ts';

//import {TalesProps} from '../../utilities/TalesTypes/TalesTypes';
import {View, Text} from 'dripsy';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {coinColor} from '../../../globalStyles/globalStyles';

import {useSelector} from 'react-redux';

interface InventoryViewProps {}

const IndexInventoryView: React.FC<InventoryViewProps> = ({navigation}) => {
  const walletState = useSelector((state: any) => state.walletReducer);

  const goAvailableViedosView = () => {
    navigation.navigate('InventoryStack', {
      screen: 'Videos disponibles',
    });
  };

  const goDresserView = () => {
    navigation.navigate('InventoryStack', {
      screen: 'Vestidor',
    });
  };

  return (
    <View style={{flex: 1}}>
      <Text variant="primary">Inventario</Text>
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <Text variant="secondary">Monedas: {walletState.total_coins}</Text>
        <Icon name="coins" size={30} color={coinColor} />
      </View>
      <TouchableOpacity
        style={{
          flex: 1,
          margin: 15,
          backgroundColor: '#C4C4C4',
          borderRadius: 15,
        }}
        onPress={goDresserView}>
        <Image source={require('./imgs/closet.png')} style={styles.image} />
        <Text variant="secondary">Vestidor</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          flex: 1,
          margin: 15,
          backgroundColor: '#C4C4C4',
          borderRadius: 15,
        }}
        onPress={goAvailableViedosView}>
        <Image source={require('./imgs/video.png')} style={styles.image} />
        <Text variant="secondary">Videos disponibles</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    alignSelf: 'center',
    margin: 10,
  },
});

export default IndexInventoryView;
