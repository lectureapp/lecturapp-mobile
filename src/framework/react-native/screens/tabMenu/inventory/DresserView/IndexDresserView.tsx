import React, {useState, useEffect} from 'react';

import {View, Text} from 'dripsy';
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  Modal,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {coinColor} from '../../../../globalStyles/globalStyles';
import ClothingItem from './ClothingItem';

import {getAllClothesThatExist} from '../../../../../redux/actions/clothesActions';

// REDUX
import {useDispatch, useSelector} from 'react-redux';

const IndexDresserView = () => {
  const cloth_cowboy: any = require('../../../../../../domain/assets/images/clothing/cowboy/ropa_vaquero3.png');
  const cloth_suit: any = require('../../../../../../domain/assets/images/clothing/suit/ropa_terno.png');
  const cloth_spaceman: any = require('../../../../../../domain/assets/images/clothing/spaceman/ropa_astronauta.png');

  // bringing states and actions from redux:
  const clothesState = useSelector((state: any) => state.clothesReducer);
  const walletState = useSelector((state: any) => state.walletReducer);
  const foxCommentsState = useSelector(
    (state: any) => state.foxCommentsReducer,
  );

  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    dispatch(getAllClothesThatExist());
  }, []);

  // Extrayendo id y precios de ropas:
  // ---------------------------------------------------
  let cowbow_price: number = 0;
  let tuxedo_price: number = 0;
  let astronaut_price: number = 0;

  let cowbow_id: string = '';
  let tuxedo_id: string = '';
  let astronaut_id: string = '';

  clothesState.all_clothes_that_exist.forEach((element: any) => {
    if (element.outfit_name == 'COWBOY') {
      cowbow_price = element.price;
      cowbow_id = element._id;
    } else if (element.outfit_name == 'TUXEDO') {
      tuxedo_price = element.price;
      tuxedo_id = element._id;
    } else if (element.outfit_name == 'ASTRONAUT') {
      astronaut_price = element.price;
      astronaut_id = element._id;
    }
  });
  //
  // ---------------------------------------------------

  let imagePath = getClothAndAvatarPosition(clothesState);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text variant="primary">Monedas: {walletState.total_coins}</Text>
          <Icon name="coins" size={30} color={coinColor} />
        </View>
        <ImageBackground
          style={styles.dialogBox}
          source={require('../../../../../../domain/assets/images/mensajeVacio.png')}>
          <Text variant="normal" style={styles.foxMessage}>
            {foxCommentsState.current_fox_comment}
          </Text>
        </ImageBackground>
        <Image style={styles.pet} source={imagePath} />
        {clothesState.get_all_clothes_that_exist_state == 'loading' ? (
          <>
            <ActivityIndicator
              size="large"
              color="#00ff00"
              style={{marginTop: 60}}
            />
            <Text variant="normal" style={{textAlign: 'center'}}>
              Cargando vestimentas...
            </Text>
          </>
        ) : (
          <>
            <ClothingItem
              pathToImage={cloth_cowboy}
              title="Vaquero"
              isBuyed={clothesState.all_user_clothes.indexOf('COWBOY') != -1}
              price={cowbow_price}
              outfit_id={cowbow_id}
              setModalVisible={setModalVisible}
            />
            <ClothingItem
              pathToImage={cloth_suit}
              title="Terno"
              isBuyed={clothesState.all_user_clothes.indexOf('TUXEDO') != -1}
              price={tuxedo_price}
              outfit_id={tuxedo_id}
              setModalVisible={setModalVisible}
            />
            <ClothingItem
              pathToImage={cloth_spaceman}
              title="Astronauta"
              isBuyed={clothesState.all_user_clothes.indexOf('ASTRONAUT') != -1}
              price={astronaut_price}
              outfit_id={astronaut_id}
              setModalVisible={setModalVisible}
            />
          </>
        )}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              {clothesState.buy_cloth_state == 'loading' && (
                <ActivityIndicator size="large" color="#00ff00" />
              )}

              <Text style={styles.modalText}>
                {clothesState.buy_cloth_message}
              </Text>
              <TouchableOpacity
                style={[styles.button2, styles.buttonClose]}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.textStyle}>Aceptar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </ScrollView>
    </View>
  );
};

const getClothAndAvatarPosition = (clothesState: any) => {
  const randomNumber: number = Math.random();
  // DEFAULT:
  if (clothesState.current_cloth == 'DEFAULT') {
    if (randomNumber <= 0.33)
      return require('../../../../../../domain/assets/images/clothing/naked/zorro_normal_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../../domain/assets/images/clothing/naked/zorro_normal_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../../domain/assets/images/clothing/naked/zorro_normal_pose2.png');
  }
  // COWBOY:
  else if (clothesState.current_cloth == 'COWBOY') {
    if (randomNumber <= 0.33)
      return require('../../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose3.png');
  }
  // TUXEDO:
  else if (clothesState.current_cloth == 'TUXEDO') {
    if (randomNumber <= 0.33)
      return require('../../../../../../domain/assets/images/clothing/suit/zorro_terno_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../../domain/assets/images/clothing/suit/zorro_terno_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../../domain/assets/images/clothing/suit/zorro_terno_pose2.png');
  }
  // ASTRONAUT:
  else if (clothesState.current_cloth == 'ASTRONAUT') {
    if (randomNumber <= 0.33)
      return require('../../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose3.png');
  } else
    return require('../../../../../../domain/assets/images/clothing/naked/zorro_normal_pose1.png');
  // ---------------------------------------------------
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pet: {
    height: 220,
    width: 250,
    resizeMode: 'contain',
  },
  message: {
    alignSelf: 'flex-end',
    //backgroundColor: 'red',
  },
  dialogBox: {
    width: 230,
    height: 200,
    alignSelf: 'flex-end',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  foxMessage: {
    // fontSize: 17,
    color: 'black',
    marginLeft: 20,
    marginBottom: 30,
    width: 180,
    textAlign: 'center',
    alignSelf: 'center',
  },

  // MODAL:
  // -----------------------------------------------------
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default IndexDresserView;
