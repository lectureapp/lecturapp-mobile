import React, {useState, useEffect} from 'react';

import {TouchableOpacity, Image, StyleSheet, Alert} from 'react-native';
import {View, Text} from 'dripsy';
import {buttonStyle, coinColor} from '../../../../globalStyles/globalStyles';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {useDispatch, useSelector} from 'react-redux';

import {
  buyCloth,
  equipCloth,
} from '../../../../../redux/actions/clothesActions';

const ClothingItem = ({
  pathToImage,
  title,
  isBuyed,
  setModalVisible,
  price,
  outfit_id,
}: any) => {
  //
  const dispatch = useDispatch();
  const clothesState = useSelector((state: any) => state.clothesReducer);
  const AuthState = useSelector((state: any) => state.AuthReducer);

  const onPressBuyButton = () => {
    let alertMessage: string = '';

    if (isBuyed) alertMessage = '¿Desea equiparse esta ropa?';
    else alertMessage = '¿Desea comprar esta ropa?';
    Alert.alert(
      'Importante',
      alertMessage,
      [
        {
          text: 'Cancel',
          onPress: () => {
            // console.log('Cancel Pressed');
          },
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            // console.log('OK Pressed');
            if (isBuyed) {
              // EQUIPAR VESTIMENTA
              // console.log('equipar');
              dispatch(equipCloth(outfit_id, AuthState.session_token));
            } else {
              // COMPRAR VESTIMENTA:
              dispatch(buyCloth(title, outfit_id, AuthState.session_token));
              setModalVisible(true);
            }
          },
        },
      ],
      {cancelable: false},
    );
  };

  return (
    <View style={styles.container}>
      <Image style={styles.cloth} source={pathToImage} />
      <View style={{flex: 2}}>
        <Text variant="primary">{title}</Text>
        <TouchableOpacity
          style={[
            buttonStyle,
            {
              width: 200,
              alignSelf: 'flex-end',
              flexDirection: 'row',
              justifyContent: 'center',
            },
          ]}
          onPress={onPressBuyButton}>
          {isBuyed ? (
            <Text variant="secondary" style={{color: 'white'}}>
              Equipar
            </Text>
          ) : (
            <>
              <Text variant="secondary" style={{color: 'white'}}>
                Comprar por {price}
              </Text>
              <Icon
                name="coins"
                size={30}
                color={coinColor}
                style={{alignSelf: 'center'}}
              />
            </>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
    padding: 20,
    borderTopWidth: 2,
    borderColor: 'gray',
  },
  scroll: {
    //backgroundColor: 'blue',
  },
  cloth: {
    flex: 1,
    height: 150,
    width: 200,
    resizeMode: 'contain',
  },
  message: {
    alignSelf: 'flex-end',
    //backgroundColor: 'red',
  },
});

export default ClothingItem;
