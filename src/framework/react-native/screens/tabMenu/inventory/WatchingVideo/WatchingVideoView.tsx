import React, {useState, useEffect} from 'react';

import {
  TouchableOpacity,
  Image,
  StyleSheet,
  Modal,
  ActivityIndicator,
} from 'react-native';
import {View, Text} from 'dripsy';

import YouTube from 'react-native-youtube';

import {buttonStyle, coinColor} from '../../../../globalStyles/globalStyles';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {useSelector, useDispatch} from 'react-redux';

import {buyVideoTime} from '../../../../../redux/actions/availableVideosActions';

import Toast, {DURATION} from 'react-native-easy-toast';
import {menuColor} from '../../../../globalStyles/globalStyles';

const WatchingVideoView = ({route, navigation}) => {
  const [isReady, setIsReady] = useState(false); // needed for <Youtube />
  const [modalVisible, setModalVisible] = useState(false);

  const dispatch = useDispatch();

  const AuthState = useSelector((state: any) => state.AuthReducer);
  const walletState = useSelector((state: any) => state.walletReducer);
  const availableVideosState = useSelector(
    (state: any) => state.availableVideosReducer,
  );
  const buy_video_time_state = useSelector(
    (state: any) => state.availableVideosReducer.buy_video_time_state,
  );

  let toast: any;

  let toast_color: string = menuColor;
  const [toastColor2, setToastColor2] = useState(menuColor);

  const showToast = (message: string) => {
    toast.show(<Text style={{color: 'white'}}>{message}</Text>, 2000);
  };

  // Showing toast:
  useEffect(() => {
    if (
      availableVideosState.buy_video_time_message ==
      'Video actualizado y Monedas restadas correctamente'
    ) {
      setToastColor2(menuColor);
      showToast(availableVideosState.buy_video_time_message);
    } else if (
      availableVideosState.buy_video_time_message ==
      'No cuenta con las monedas suficientes'
    ) {
      setToastColor2('red');
      showToast(availableVideosState.buy_video_time_message);
    }
  }, [availableVideosState.buy_video_time_state]);
  // -------------------------------------

  return (
    <View style={{alignItems: 'center'}}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text variant="primary">Monedas: {walletState.total_coins}</Text>
        <Icon name="coins" size={30} color={coinColor} />
      </View>
      <YouTube
        apiKey="AIzaSyCPIfNmLKPkYi14dZdfSA4qfUGBrrvvjtI"
        //videoId="ShypI8IYy0g" // The YouTube video ID
        videoId={route.params.videoID} // The YouTube video ID
        loop // control whether the video should loop when ended
        onReady={(e) => setIsReady(true)}
        style={{alignSelf: 'stretch', height: 300}}
      />
      <View style={{flexDirection: 'row'}}>
        <Text variant="secondary">
          Tiempo restante:{' '}
          {availableVideosState.current_video_to_show.time_left}
        </Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text variant="secondary" style={{flex: 3}}>
          Comprar el video por un día más:
        </Text>
        <TouchableOpacity
          style={[
            buttonStyle,
            {
              flexDirection: 'row',
              flex: 3,
              justifyContent: 'center',
              alignItems: 'center',
            },
          ]}
          onPress={() => {
            // dispatch(
            //   buyVideoTime(AuthState.session_token, route.params.video_mongoID),
            // )
            setModalVisible(true);
          }}>
          <Text variant="secondary" style={{color: 'white'}}>
            Comprar por 5
          </Text>
          <Icon name="coins" size={30} color={coinColor} />
        </TouchableOpacity>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <>
              <Text style={styles.modalText}>
                ¿Quieres comprar este video por 24 horas más?
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity
                  style={[styles.button2, styles.buttonClose]}
                  onPress={() => {
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Cancelar</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.button2, styles.buttonClose]}
                  onPress={() => {
                    dispatch(
                      buyVideoTime(
                        AuthState.session_token,
                        route.params.video_mongoID,
                      ),
                    );
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Comprar</Text>
                </TouchableOpacity>
              </View>
            </>
          </View>
        </View>
      </Modal>
      <Toast
        ref={(toast2) => (toast = toast2)}
        style={{backgroundColor: toastColor2}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  // MODAL:
  // -----------------------------------------------------
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    paddingHorizontal: 25,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 15,
  },
});

export default WatchingVideoView;
