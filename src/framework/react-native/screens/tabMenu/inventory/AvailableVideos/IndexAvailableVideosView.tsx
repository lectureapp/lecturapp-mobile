import React, {useState, useEffect} from 'react';

import {StyleSheet, FlatList} from 'react-native';
import {View, Text} from 'dripsy';

import VideoItem from './VideoItem';

// REDUX
import {useDispatch, useSelector} from 'react-redux';
import {getAvailableVideos} from '../../../../../redux/actions/availableVideosActions';

const IndexAvailableVideosView = ({navigation}: any) => {
  // IMPORTANTE: puedo conseguir el thumbnail de cualquier video consultando la
  // la siguiente url: https://img.youtube.com/vi/D84Dn1bzq64/0.jpg
  // Solo debo reemplazar el ID de cada video.

  const authState = useSelector((state: any) => state.AuthReducer);
  const availableVideosState = useSelector(
    (state: any) => state.availableVideosReducer,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAvailableVideos(authState.session_token));
  }, []);

  return (
    <View style={{flex: 1}}>
      <Text variant="primary">Videos disponibles</Text>
      <Text variant="normal">
        Los videos que ganes podrás verlos por un día, para volverlos a ver
        tendrás que volver a leer el cuento o comprarlos más tiempo.
      </Text>
      <FlatList
        data={availableVideosState.all_videos}
        renderItem={({item}) => {
          // TODO: el backend devuelve toda la url, incluido el "www.youtube..."
          // o algo parecido, el backend solo debe enviar el id del cada video
          // para poder eliminar la siguiente linea:
          const id_url: string = item._url.substring(17, item._url.length);

          return (
            <VideoItem
              // TODO: el backend debe enviar los títulos de los videos
              //videoName="los tres chanchitos"
              videoID={id_url}
              navigation={navigation}
              timeLeft={item.time_left}
              state={item.state}
              video_mongoID={item._videoId}
              all_video_object_selected={item}
            />
          );
        }}
        keyExtractor={(item) => item._id}
      />
    </View>
  );
};

export default IndexAvailableVideosView;
