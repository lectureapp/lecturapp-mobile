import React, {useState, useEffect} from 'react';

import {TouchableOpacity, Image, StyleSheet} from 'react-native';
import {View, Text} from 'dripsy';

import {StackNavigationProp} from '@react-navigation/stack';

import {useSelector, useDispatch} from 'react-redux';
import {setCurrentVideoToShow} from '../../../../../redux/actions/availableVideosActions';

const VideoItem = ({
  videoID,
  videoName,
  navigation,
  timeLeft,
  state,
  video_mongoID,
  all_video_object_selected,
}: any) => {
  //NOTA: El siguiente link sirve para solicitar datos de un video desde su ID,
  //hace uso de un APIkey, creo que sería mejor guardar el nombre de cada
  //video que estemos usando en nuestro backend junto a su ID de video:
  //https://www.googleapis.com/youtube/v3/videos?part=snippet&id=ShypI8IYy0g&key=AIzaSyCPIfNmLKPkYi14dZdfSA4qfUGBrrvvjtI

  const dispatch = useDispatch();

  const goWatchingVideoView = () => {
    dispatch(setCurrentVideoToShow(all_video_object_selected));

    navigation.navigate('InventoryStack', {
      screen: 'Video',
      params: {
        videoID,
        timeLeft,
        video_mongoID,
        all_video_object_selected,
      },
    });
  };

  if (state == 'INACTIVE') {
    return <></>;
  } else {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={goWatchingVideoView}>
          <Image
            source={{
              uri: 'https://img.youtube.com/vi/' + videoID + '/0.jpg',
            }}
            //style={styles.imagen}
            style={{flex: 1, height: 300}}
          />
          <Text variant="secondary">{videoName}</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 15,
  },
  imagen: {
    width: 430, // TODO: Mejorar el ancho de la imagen para que trabaje con dripsy y ocupe
    // todo el ancho del dispositivo.
    height: 300,
    alignSelf: 'center',
  },
});

export default VideoItem;
