import React from 'react';

import {Provider} from 'react-redux';
import {
  makeMockStore,
  NavAndReduxWrapper,
} from '../../../../../../../__mocks__/utils';

// react native testing library:
import {render, cleanup} from '@testing-library/react-native';
import {NavigationContext} from '@react-navigation/native';
import renderer from 'react-test-renderer';

// Use with React Native <= 0.63
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock'),
); // This is needed so that <TabMenuContainer /> don't throw an error.

let component: any;

const navigation = {
  navigate: jest.fn(),
  setParams: jest.fn(),
  getParam: (myParam, defaultValue) => {
    return defaultValue;
  },
  useIsFocused: true,
};
// ---------------------------------------------------------------

import IndexInventoryView from '../IndexInventoryView';
import IndexAvailableVideosView from '../AvailableVideos/IndexAvailableVideosView';
import IndexDresserView from '../DresserView/IndexDresserView';
import ClothingItem from '../DresserView/ClothingItem';

describe('<IndexInventoryView />', () => {
  let store: any;

  beforeEach(() => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'idle',
        tales: [
          {_id: 1, title: 1},
          {_id: 2, title: 2},
          {_id: 3, title: 3},
          {_id: 4, title: 4},
        ],
        favorites_tales: [{_id: 1}, {_id: 2}],
      },
      AuthReducer: {
        session_token: '123',
      },
      clothesReducer: {
        current_cloth: 'DEFAULT',
        all_clothes_that_exist: [{_id: 1, outfit_name: 'COWBOY'}],
        all_user_clothes: [],
      },
      foxCommentsReducer: {
        current_fox_comment: 'hola',
        fox_comments: [],
      },
      walletReducer: {
        total_coins: 28,
      },
      availableVideosReducer: {
        all_videos: [
          {_id: 1, _url: 'url de video'},
          {_id: 2, _url: 'url de video'},
          {_id: 3, _url: 'url de video'},
        ],
      },
    });
    // NOTA: si los datos de los reducers cambian el test
    // de snapshot de este elemento botará error.

    cleanup; // método que borra todos los render();
  });

  it('<IndexInventoryView /> render', async () => {
    const component = (
      <NavAndReduxWrapper store={store}>
        <IndexInventoryView />
      </NavAndReduxWrapper>
    );

    const {findByText, findByTestId, findAllByTestId} = render(component);
    const available_videos_title = await findByText('Videos disponibles');

    expect(available_videos_title).toBeTruthy();
    // Nota: esta vista es totalmente estatica, por lo que el snapshot testing sería
    // suficiente.
  });

  it('snapshot <IndexInventoryView />', () => {
    const navContext = {
      isFocused: () => true,
      // addListener returns an unscubscribe function.
      addListener: jest.fn(() => jest.fn()),
    };

    const tree = renderer
      .create(
        <NavigationContext.Provider value={navContext}>
          <Provider store={store}>
            <IndexInventoryView navigation={navigation} />
            <IndexAvailableVideosView navigation={navigation} />
            {
              /*<IndexDresserView navigation={navigation} /> */
              // Nota: No se puede hacer snapshot de este componente
              // por que tiene una función aleatoria.
            }
            <ClothingItem
              pathToImage=""
              title=""
              isBuyed={true}
              setModalVisible={true}
              price={10}
              outfit_id="id"
            />
          </Provider>
        </NavigationContext.Provider>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
