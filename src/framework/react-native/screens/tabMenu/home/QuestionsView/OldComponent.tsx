//
// **************************************************
// IMPORTANT:
// **************************************************
// This is the old component. This component is
// saved only for watched it later.

class QuestionsView2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: [],
    };
    //this.item2 = this.props.route.params.item;
    this.item2 = data.tales[0];

    //console.log('asdasdadadsad', this.item2);
  }

  componentDidMount() {
    // linea que inhabilita backbutton del sistema
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    // linea que inhabilita backbutton del sistema
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  // linea que inhabilita backbutton del sistema
  handleBackButton() {
    return true;
  }

  setItemAnswer = (value, question_id) => {
    //setItemAnswer = (value, _id) => {
    this.item2.preguntas[question_id - 1].answer_mark = value;
    //this.item2.preguntas[1].respuesta_correcta = value;
    //console.log("La pregunta es: "+question_id)
    //console.log("value desde setItemAnswer: "+value);
  };

  goRewardView = () => {
    this.props.navigation.navigate('QualificationView', {
      item2: this.item2,
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Text style={{textAlign: 'center', fontSize: 20, margin: 15}}>
          {this.item2.titulo}
        </Text>
        <FlatList
          //data={item.questions}
          //data={this.item2.questions}
          data={this.item2.preguntas}
          renderItem={({item}) => (
            <View>
              <Text style={{margin: 10, fontSize: 17}}>{item.question}</Text>
              <RadioButtonAlternatives
                item={item}
                testFuncion={this.setItemAnswer}
              />
            </View>
          )}
          contentContainerStyle={{paddingBottom: 50}}
          keyExtractor={(item) => item.id + ''}
        />

        <TouchableOpacity
          style={{
            backgroundColor: '#915B19',
            marginHorizontal: 40,
            marginVertical: 10,
            borderRadius: 20,
            padding: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 7.65,
            elevation: 4,
          }}
          onPress={this.goRewardView}>
          <Text style={{textAlign: 'center', fontSize: 25, color: 'white'}}>
            Terminar
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
