import React, {Component, useState} from 'react';

import RadioForm from // RadioButtonInput, // RadioButton,
// RadioButtonLabel,
'react-native-simple-radio-button';

// Función con estados gracias a useState
//function RadioButtonAnswer(props) {
const RadioButtonAlternatives = (props: any) => {
  //var example_radio_props = [{label: 'param1', value: 0}];

  return (
    <RadioForm
      radio_props={props.item.alternative}
      initial={-1}
      onPress={(valueOfItemSelected: any) => {
        props.setItemAnswer(valueOfItemSelected, props.indexInFlatList); //el 2do parametro es el question_id
      }}
      style={{marginLeft: 20}}
    />
  );
};

export default RadioButtonAlternatives;
