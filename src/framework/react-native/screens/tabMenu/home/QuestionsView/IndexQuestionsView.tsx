import React, {Component, useState, useEffect} from 'react';
import {FlatList, BackHandler, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import RadioButtonAlternatives from './RadioButtonAlternatives';
import {buttonStyle} from '../../../../globalStyles/globalStyles';
import {View, Text} from 'dripsy';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';
import {setAnswerToWorkingTale} from '../../../../../redux/actions/talesActions';

const IndexQuestionsView = ({navigation, route}: any) => {
  // bringing states and actions from redux:
  let talesState = useSelector((state) => state.talesReducer);
  let authState = useSelector((state) => state.AuthReducer);
  const dispatch = useDispatch();

  // TODO: el código para desactivar botón de retroceso no está funcionando,
  // de todas maneras creo que ya no es necesario.
  useEffect(() => {
    // Linea que inhabilita backbutton del sistema
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }, []);
  const handleBackButton = () => {
    return true;
  };

  const [workingTale, setWorkingTale] = useState(talesState.workingTale);

  const setItemAnswer = (valueOfItemSelected: any, indexInFlatList: number) => {
    // This works but it's mutating the tales state, I must change:
    // ---------------------------------------------------
    // talesState.workingTale.questions[indexInFlatList].answer_mark =
    //   valueOfItemSelected;
    // ---------------------------------------------------
    // This new code does the same buy without mute the state:
    let temp_array = talesState.workingTale.questions.slice(); //creates the clone of the state
    temp_array[indexInFlatList].answer_mark = valueOfItemSelected;
    setWorkingTale({...talesState.workingTale, questions: temp_array});
  };

  const goQualificationView = () => {
    dispatch(
      // setAnswerToWorkingTale(talesState.workingTale, authState.session_token),
      setAnswerToWorkingTale(workingTale, authState.session_token),
    );
    navigation.navigate('IndexQualificationView', {});
  };

  return (
    <View style={{flex: 1}}>
      <Text variant="primary" style={{}}>
        {talesState.workingTale.title}
      </Text>
      <Text variant="normal" style={{alignSelf: 'flex-start'}}>
        Responde las siguientes preguntitas:
      </Text>
      <FlatList
        data={talesState.workingTale.questions}
        renderItem={({item, index}) => (
          <View>
            <Text
              variant="normal"
              style={{alignSelf: 'flex-start', margin: 10}}>
              {item.question}
            </Text>
            <RadioButtonAlternatives
              item={item}
              setItemAnswer={setItemAnswer}
              indexInFlatList={index}
            />
          </View>
        )}
        contentContainerStyle={{paddingBottom: 50}}
        keyExtractor={(item) => item._id + ''}
      />

      <TouchableOpacity style={buttonStyle} onPress={goQualificationView}>
        <Text variant="secondary" style={{color: 'white'}}>
          Terminar
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default IndexQuestionsView;
