import React, {Component, useState, useEffect} from 'react';

import {FlatList, TouchableOpacity} from 'react-native';

import RadioButtonAnswered from './RadioButtonAnswered';
import {View, Text} from 'dripsy';
import {buttonStyle} from '../../../../globalStyles/globalStyles';

// REDUX:
import {useSelector} from 'react-redux';
// import {sendQualificationToBackEnd} from '../../../../../redux/actions/talesActions';

const IndexQualificationView = (props) => {
  //
  // bringing states and actions from redux:
  let talesState = useSelector((state: any) => state.talesReducer);

  // const dispatch = useDispatch();

  return (
    <View style={{flex: 1, margin: 15}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          borderBottomWidth: 1,
        }}>
        <Text variant="normal" style={{}}>
          Resultados:
        </Text>
        <View style={{}}>
          <Text variant="secondary" style={{color: 'green', margin: -1}}>
            {talesState.num_of_correct_answers} Correctas
          </Text>
          <Text variant="secondary" style={{color: 'red', margin: -1}}>
            {talesState.num_of_incorrect_answers} Incorrectas
          </Text>
        </View>
      </View>

      <Text variant="primary" style={{}} testID="preguntas">
        Preguntas:
      </Text>
      <FlatList
        data={talesState.workingTale.questions}
        renderItem={({item}) => <QuestionItem item={item} />}
        contentContainerStyle={{paddingBottom: 50}}
        keyExtractor={(item) => item._id + ''}
      />

      <TouchableOpacity
        testID="go_rewardview_button"
        onPress={() => {
          props.navigation.navigate('RewardView', {});
        }}
        style={buttonStyle}>
        <Text
          variant="normal"
          style={{textAlign: 'center', fontSize: 20, color: 'white'}}>
          {talesState.num_of_correct_answers >=
          talesState.num_of_incorrect_answers
            ? 'Ir por el premio'
            : 'Siguiente'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const QuestionItem = ({item}: any) => {
  var answerIndicator = false;
  if (item.answer_mark == item.correct_answer) {
    answerIndicator = true;
  } else {
    answerIndicator = false;
  }

  // TODO: para la 2da versión de la app tratar de hacerlo con useState:
  // const [markedValue, setMarkedValue] = useState(item.answer_mark);
  // useEffect(() => {
  //   if (typeof item.answer_mark == 'undefined') {
  //     setMarkedValue(-1);
  //   }
  // }, []);
  var markedValue = -1;
  if (typeof item.answer_mark == 'undefined') {
    markedValue = -1;
  } else {
    markedValue = item.answer_mark;
  }

  return (
    <View>
      <Text variant="secondary" style={{margin: -1, textAlign: 'left'}}>
        {item.question}
      </Text>

      {answerIndicator && (
        <Text
          variant="normal"
          style={{
            marginLeft: 20,
            color: 'green',
            textAlign: 'left',
          }}>
          Respondiste bien
        </Text>
      )}
      {!answerIndicator && (
        <Text
          variant="normal"
          style={{marginLeft: 20, color: 'red', textAlign: 'left'}}>
          Respondiste mal
        </Text>
      )}

      <RadioButtonAnswered
        item={item}
        // answer_mark={item.answer_mark}
        answer_mark={markedValue}></RadioButtonAnswered>
    </View>
  );
};

export default IndexQualificationView;
