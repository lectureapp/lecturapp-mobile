import React from 'react';

import {Provider} from 'react-redux';
import {
  makeMockStore,
  NavAndReduxWrapper,
} from '../../../../../../../../__mocks__/utils';

// react native testing library:
import {render, cleanup, fireEvent} from '@testing-library/react-native';
import {NavigationContext} from '@react-navigation/native';
import TalesStack from '../../../../../navigation/StackScreens/TalesStack/TalesStack';
import renderer from 'react-test-renderer';

import IndexQualificationView from '../IndexQualificationView';

// Use with React Native <= 0.63
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const navigation = {
  navigate: jest.fn(),
  setParams: jest.fn(),
  getParam: (myParam, defaultValue) => {
    return defaultValue;
  },
  useIsFocused: true,
};

describe('<IndexQualificationView /> ', () => {
  let store: any;

  beforeEach(() => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'idle',
        tales: [
          {_id: 1, title: 'tale 1'},
          {_id: 2, title: 2},
          {_id: 3, title: 3},
          {_id: 4, title: 4},
        ],
        workingTale: {
          content: [],
          added_as_favorite: true,
        },
        num_of_correct_answers: 4,
        num_of_incorrect_answers: 2,
      },
      AuthReducer: {
        session_token: '123',
      },
      configTalesReducer: {
        dotsIsActivated: false,
        dotsPerLine: 3,
        sizeOfDots: 10,
      },
    });

    cleanup;
  });

  it('<IndexQualificationView /> render', async () => {
    const component = (
      <NavAndReduxWrapper store={store}>
        <TalesStack />
      </NavAndReduxWrapper>
    );

    const {findByText, findByTestId, findAllByTestId} = render(component);

    // Entering in: <TaleView />:
    const click_to_go_to_taleview = await findAllByTestId('daily_tale_element'); // return an array with 4 tale buttons
    fireEvent(click_to_go_to_taleview[0], 'press');

    // Entering in: <IndexQuestionView />:
    const click_to_go_to_indexquestionview = await findByText('Ir a preguntas');
    fireEvent(click_to_go_to_indexquestionview, 'press');

    // Entering in: <IndexQualificationView />:
    const click_to_go_to_indexqualificationview = await findByText('Terminar');
    fireEvent(click_to_go_to_indexqualificationview, 'press');

    const title = await findByText('Resultados:');
    const questions_text = await findByText('Preguntas:');
    const approved_exam_test = await findByText('Ir por el premio');

    expect(title).toBeDefined();
    expect(questions_text).toBeDefined();
    expect(approved_exam_test).toBeDefined();
  });

  it('snapshot <IndexQualificationView />', async () => {
    const navContext = {
      isFocused: () => true,
      // addListener returns an unscubscribe function.
      addListener: jest.fn(() => jest.fn()),
    };

    const tree = renderer
      .create(
        <NavigationContext.Provider value={navContext}>
          <Provider store={store}>
            <IndexQualificationView navigation={navigation} />
          </Provider>
        </NavigationContext.Provider>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
