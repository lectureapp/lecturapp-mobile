//
// **************************************************
// IMPORTANT:
// **************************************************
// This is the old component. This component is
// saved only for watched it later.

import React, {Component, useState, useEffect} from 'react';

import {
  View,
  Text,
  Image,
  FlatList,
  InteractionManager,
  TouchableOpacity,
  Button,
  ScrollView,
} from 'react-native';

class QualificationView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const item2 = this.props.route.params.item2;

    return (
      <View style={{flex: 1, backgroundColor: '#8ECB31'}}>
        <View style={{flex: 1, margin: 15}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              borderBottomWidth: 1,
            }}>
            <Text style={{fontSize: 18}}>Resultados:</Text>
            <View style={{}}>
              <Text style={{fontSize: 18, color: 'green'}}>Correctas</Text>
              <Text style={{fontSize: 18, color: 'red'}}>Incorrectas</Text>
            </View>
          </View>

          <Text style={{fontSize: 18}}>Preguntas:</Text>
          <FlatList
            data={item2.preguntas}
            renderItem={({item}) => {
              var answerIndicator = false;
              if (item.answer_mark == item.correct_answer) {
                answerIndicator = true;
              } else {
                answerIndicator = false;
              }
              return (
                <View>
                  <Text style={{margin: 10}}>{item.question}</Text>

                  {answerIndicator && (
                    <Text style={{marginLeft: 20, color: 'green'}}>
                      Respondiste bien
                    </Text>
                  )}
                  {!answerIndicator && (
                    <Text style={{marginLeft: 20, color: 'red'}}>
                      Respondiste mal
                    </Text>
                  )}

                  <RadioButtonAnswered
                    item={item}
                    answer_mark={item.answer_mark}></RadioButtonAnswered>
                </View>
              );
            }}
            contentContainerStyle={{paddingBottom: 50}}
            keyExtractor={(item) => item.id + ''}
          />

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('RewardView', {})}
            style={{
              backgroundColor: '#3376CE',
              alignSelf: 'flex-end',
              width: '50%',
              borderRadius: 15,
              padding: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              elevation: 4,
            }}>
            <Text style={{textAlign: 'center', fontSize: 20, color: 'white'}}>
              ¡Recoge tu premio!
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default QualificationView;
