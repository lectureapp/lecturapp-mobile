import React from 'react';

import RadioForm from 'react-native-simple-radio-button';

// Función con estados gracias a useState
//function RadioButtonAnswer(props) {
const RadioButtonAnswered = (props) => {
  // var radio_props_structure_example = [
  //   {label: 'param1', value: 0},
  //   {label: 'param2', value: 1},
  // ];

  return (
    <RadioForm
      //radio_props={props.item.alternatives}
      radio_props={props.item.alternative}
      initial={props.answer_mark}
      onPress={(value) => {}}
      style={{marginLeft: 20}}
      disabled={true}
    />
  );
};

export default RadioButtonAnswered;
