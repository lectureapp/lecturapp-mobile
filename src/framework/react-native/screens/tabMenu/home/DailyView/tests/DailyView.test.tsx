import React from 'react';

import {Provider} from 'react-redux';
import {
  makeMockStore,
  NavAndReduxWrapper,
} from '../../../../../../../../__mocks__/utils';

// react native testing library:
import {render, cleanup} from '@testing-library/react-native';
import {NavigationContext} from '@react-navigation/native';

import renderer from 'react-test-renderer';

import TalesStack from '../../../../../navigation/StackScreens/TalesStack/TalesStack';
import DailyView from '../DailyView';

let component: any;

// Use with React Native <= 0.63
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const navigation = {
  navigate: jest.fn(),
  setParams: jest.fn(),
  getParam: (myParam, defaultValue) => {
    return defaultValue;
  },
  useIsFocused: true,
};

describe('<DailyView />', () => {
  let store: any;

  beforeEach(() => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'idle',
        tales: [
          {_id: 1, title: 1},
          {_id: 2, title: 2},
          {_id: 3, title: 3},
          {_id: 4, title: 4},
          // {_id: 5, title: 5},
        ],
      },
      AuthReducer: {
        session_token: '123',
      },
    });

    cleanup; // método que borra todos los render();
  });

  it('<DailyView /> idle', async () => {
    const component = (
      <NavAndReduxWrapper store={store}>
        <TalesStack />
      </NavAndReduxWrapper>
    );

    const {findByText, findAllByTestId} = render(component);

    const title = await findByText('Cuentos para hoy');
    const other_tales_button = await findByText('Otros cuentos');
    const dailyTaleElement = await findAllByTestId('daily_tale_element');

    expect(title).toBeTruthy();
    expect(other_tales_button).toBeTruthy();
    expect(dailyTaleElement.length).toBe(4);
  });

  it('<DailyView /> loading', async () => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'loading',
      },
      AuthReducer: {
        session_token: '123',
      },
    });

    const component = (
      <NavAndReduxWrapper store={store}>
        <TalesStack />
      </NavAndReduxWrapper>
    );
    const {findByTestId} = render(component);

    const spinner = await findByTestId('spinner');
    expect(spinner).toBeDefined();
  });

  it('<DailyView /> error', async () => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'error',
      },
      AuthReducer: {
        session_token: '123',
      },
    });

    const component = (
      <NavAndReduxWrapper store={store}>
        <TalesStack />
      </NavAndReduxWrapper>
    );
    const {findByTestId} = render(component);

    const error_message = await findByTestId('error_message');
    expect(error_message).toBeDefined();
  });

  it('snapshot <DailyView />', () => {
    const navContext = {
      isFocused: () => true,
      // addListener returns an unscubscribe function.
      addListener: jest.fn(() => jest.fn()),
    };

    const tree = renderer
      .create(
        <NavigationContext.Provider value={navContext}>
          <Provider store={store}>
            <DailyView navigation={navigation} />
          </Provider>
        </NavigationContext.Provider>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
