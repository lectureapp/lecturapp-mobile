import React, {useEffect} from 'react';
import {FlatList, ImageBackground, ActivityIndicator} from 'react-native';

//Navigation
// import {ITales, TalesProps} from '../../../utilities/TalesTypes/TalesTypes';

//Componentes
import ListItem from './ListItem';

import {View, Text} from 'dripsy';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';
import {
  setTaleToWork,
  bringTalesInDailyView,
  bringTalesFromOtherTalesButton,
} from '../../../../../redux/actions/talesActions';

import {buttonStyle2} from '../../../../globalStyles/globalStyles';

import {useIsFocused} from '@react-navigation/native';

// const DailyView = ({navigation}: TalesProps<'DailyView'>) => {
const DailyView = ({navigation}) => {
  // bringing states and actions from redux:
  const talesState = useSelector((state: any) => state.talesReducer);
  const AuthState = useSelector((state: any) => state.AuthReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      bringTalesInDailyView(AuthState.session_token, talesState.currentPage),
    );
  }, []);

  const screenIsFocused = useIsFocused();
  useEffect(() => {
    dispatch(
      bringTalesInDailyView(AuthState.session_token, talesState.currentPage),
    );
  }, [screenIsFocused]);

  const goTaleView = (taleSelected: ITales) => {
    dispatch(setTaleToWork(taleSelected));
    navigation.navigate('TaleView', {
      //taleSelected,
    });
  };

  return (
    <ImageBackground
      source={require('../../../../../../domain/assets/images/fondo1.png')}
      style={{flex: 1}}>
      <View>
        <View style={{flexDirection: 'row'}}>
          <Text
            variant="primary"
            style={{color: 'white', flex: 5, marginTop: 10}}>
            Cuentos para hoy
          </Text>
          <View style={buttonStyle2}>
            <Text
              variant="normal"
              style={{color: 'white'}}
              onPress={() => {
                dispatch(
                  bringTalesFromOtherTalesButton(
                    AuthState.session_token,
                    talesState.currentPage,
                    talesState.isLastPage,
                  ),
                );
              }}>
              Otros cuentos
            </Text>
          </View>
        </View>

        {talesState.traer_cuentos_state == 'loading' && (
          <ActivityIndicator size="large" color="#00ff00" testID="spinner" />
        )}
        {talesState.traer_cuentos_state == 'error' && (
          <Text
            style={{color: 'green', textAlign: 'center', fontSize: 20}}
            testID="error_message">
            Comprueba tu internet...
          </Text>
        )}
        {talesState.traer_cuentos_state == 'idle' && (
          <FlatList
            data={talesState.tales}
            extraData={talesState.tales}
            renderItem={({item}) => (
              <ListItem item={item} goTaleView={goTaleView} />
            )}
            contentContainerStyle={{paddingBottom: 50}}
            keyExtractor={(item) => item._id + ''}
          />
        )}
      </View>
    </ImageBackground>
  );
};

//const styles = StyleSheet.create({});

export default DailyView;
