import React, {useEffect} from 'react';
// import {TouchableOpacity} from 'react-native-gesture-handler';
import {StyleSheet, TouchableOpacity} from 'react-native';

//Navigation TS
import {ITales} from '../../../../../../domain/assets/utilities/TalesTypes';

//import {buttonColor} from '../../../globalStyles/globalStyles';
import {buttonColor} from '../../../../globalStyles/globalStyles';
import {View, Text} from 'dripsy';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

interface TaleItemProps {
  item: ITales;
  goTaleView: (item: ITales) => void;
}

const ListItem = ({item, goTaleView}: TaleItemProps) => {
  useEffect(() => {
    // console.log('estado: ', item);
  }, []);

  return (
    <TouchableOpacity
      style={styles.taleButton}
      testID="daily_tale_element"
      onPress={() => {
        goTaleView(item);
      }}>
      <Text
        variant="secondary"
        style={{color: 'white', flex: 6, textAlign: 'left', marginLeft: 20}}>
        {item.title}
      </Text>

      {item.times_read && ( // esto permitirá indicar que cuentos diarios están terminados
        <View style={styles.completeTag}>
          <Text variant="normal" style={{color: 'white'}}>
            Completado
          </Text>
        </View>
      )}
      {item.added_as_favorite ? (
        <MaterialCommunityIcons
          name="star"
          size={30}
          color="yellow"
          style={{margin: 5}}
        />
      ) : (
        <View style={{flex: 1.25}} />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  completeTag: {
    flex: 3,
    backgroundColor: '#469A51',
    height: 35,
    borderRadius: 8,
    // marginRight: 5,
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  taleButton: {
    marginHorizontal: 15,
    marginVertical: 15,
    backgroundColor: buttonColor,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    height: 72,
  },
});

export default ListItem;
