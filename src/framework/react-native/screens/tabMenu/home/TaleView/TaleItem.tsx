import React, {useEffect} from 'react';
import {StyleSheet, Image} from 'react-native';
import {View, Text} from 'dripsy';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector} from 'react-redux';

const IndexItem = ({taleContentList}: TaleViewProps) => {
  const configTalesState = useSelector(
    (state: any) => state.configTalesReducer,
  );

  return (
    <View style={myStyles.container}>
      {taleContentList.substr(0, 4) != 'http' ? (
        <View>
          <LineOfCircles
            numOfCircles={configTalesState.dotsPerLine}
            size={configTalesState.sizeOfDots}
          />
          <Text variant="normal" style={myStyles.paragraph}>
            {taleContentList}
          </Text>
        </View>
      ) : (
        <Image source={{uri: taleContentList}} style={myStyles.image} />
      )}
    </View>
  );
};

const LineOfCircles = ({numOfCircles, size}) => {
  const myLoop = [];

  const configTalesState = useSelector(
    (state: any) => state.configTalesReducer,
  );

  if (configTalesState.dotsIsActivated) {
    for (let i = 0; i < numOfCircles; i++) {
      myLoop.push(
        <MaterialCommunityIcons
          key={i}
          name="circle"
          size={size}
          color="red"
        />,
      );
    }
  }

  return <View style={myStyles.lineOfCirclesStyle}>{myLoop}</View>;
};

const myStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  paragraph: {
    marginVertical: 10,
    marginHorizontal: 25,
  },
  image: {
    width: 200,
    height: 200,
    margin: 10,
  },
  lineOfCirclesStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: -15,
    flexWrap: 'nowrap',
  },
});

export default IndexItem;
