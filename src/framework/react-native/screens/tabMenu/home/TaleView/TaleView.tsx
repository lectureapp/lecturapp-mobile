import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  //FlatList,
} from 'react-native';
import {ITales, TalesProps} from '../../../utilities/TalesTypes/TalesTypes';

import IndexItem from './TaleItem';
import {buttonStyle, buttonStyle2} from '../../../../globalStyles/globalStyles';
import {Text} from 'dripsy';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import IndexGuidePointsModal from './guidePointsModal/IndexGuidePoints';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';

import {
  setFavoriteTale,
  removeFavoriteTale,
  bringFavoriteTales,
} from '../../../../../redux/actions/talesActions';

import Toast from 'react-native-easy-toast';
import {menuColor} from '../../../../globalStyles/globalStyles';

interface TaleViewProps {
  tale: string;
}

const TaleView = ({navigation, route}: TalesProps<'TaleView'>) => {
  //
  const talesState = useSelector((state: any) => state.talesReducer);
  const authState = useSelector((state: any) => state.AuthReducer);

  const dispatch = useDispatch();
  const comeFromFavorites = route.params?.comeFromFavorites;

  const [pointsModalVisible, setPointsModalVisible] = useState(false);

  const goQuestionsView = () => {
    navigation.navigate('QuestionsView', {
      //taleSelected: route.params?.taleSelected as ITales,
      //taleSelected: talesState.workingTale as ITales,
    });
  };

  const listItems = talesState.workingTale.content.map(
    (taleContentList: any) => (
      <IndexItem key={Math.random()} taleContentList={taleContentList} />
    ),
  );

  useEffect(() => {
    return () => {
      dispatch(bringFavoriteTales(authState.session_token));
    };
  }, []);

  let toast: any;

  return (
    <ScrollView nestedScrollEnabled={true}>
      <IndexGuidePointsModal
        pointsModalVisible={pointsModalVisible}
        setPointsModalVisible={setPointsModalVisible}
      />
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={[buttonStyle2, {flex: 1, alignItems: 'center'}]}
          onPress={() => setPointsModalVisible(!pointsModalVisible)}>
          <Text
            variant="normal"
            style={{
              color: 'white',
              marginVertical: 1,
            }}>
            Puntos de guía
          </Text>
          <MaterialCommunityIcons
            name="circle"
            size={20}
            color="red"
            style={{margin: 5}}
          />
        </TouchableOpacity>
        <Text
          variant="primary"
          testID="tale_title"
          style={{
            flex: 2.5,
          }}>
          {talesState.workingTale.title}
        </Text>
        <TouchableOpacity
          style={[buttonStyle2, {flex: 1, alignItems: 'center'}]}
          onPress={() => {
            let message: string = '';
            if (!talesState.workingTale.added_as_favorite) {
              dispatch(setFavoriteTale());
              message = 'Cuento añadido correctamente.';
              // dispatch(bringFavoriteTales(authState.session_token));
            } else {
              dispatch(removeFavoriteTale());
              message = 'Cuento borrado de favoritos.';
            }
            toast.show(<Text style={{color: 'white'}}>{message}</Text>, 2000);
          }}>
          <MaterialCommunityIcons
            name="star"
            size={50}
            color={talesState.workingTale.added_as_favorite ? 'gray' : 'yellow'}
            style={{marginBottom: -15}}
          />
          <Text
            variant="normal"
            style={{
              color: 'white',
            }}>
            {talesState.workingTale.added_as_favorite
              ? 'Borrar favorito'
              : 'Añadir favorito'}
          </Text>
        </TouchableOpacity>
      </View>

      <View>{listItems}</View>
      {!comeFromFavorites && (
        <TouchableOpacity style={buttonStyle} onPress={goQuestionsView}>
          <Text variant="secondary" style={{color: 'white'}}>
            Ir a preguntas
          </Text>
        </TouchableOpacity>
      )}
      <Toast
        ref={(toast2) => (toast = toast2)}
        style={{backgroundColor: menuColor}}
      />
    </ScrollView>
  );
};

// const myStyles = StyleSheet.create({
//   button: buttonStyle,
// });

export default TaleView;
