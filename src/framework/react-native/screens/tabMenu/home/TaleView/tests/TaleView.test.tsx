import React from 'react';

import {Provider} from 'react-redux';
import {
  makeMockStore,
  NavAndReduxWrapper,
} from '../../../../../../../../__mocks__/utils';

// react native testing library:
import {render, cleanup, fireEvent} from '@testing-library/react-native';
import {NavigationContext} from '@react-navigation/native';
import TalesStack from '../../../../../navigation/StackScreens/TalesStack/TalesStack';

import renderer from 'react-test-renderer';
import TaleView from '../TaleView';
import IndexItem from '../TaleItem';

// Use with React Native <= 0.63
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const navigation = {
  navigate: jest.fn(),
  setParams: jest.fn(),
  getParam: (myParam, defaultValue) => {
    return defaultValue;
  },
  useIsFocused: true,
};

describe('<TaleView /> ', () => {
  let store: any;

  beforeEach(() => {
    store = makeMockStore({
      talesReducer: {
        traer_cuentos_state: 'idle',
        tales: [
          {_id: 1, title: 1},
          {_id: 2, title: 2},
          {_id: 3, title: 3},
          {_id: 4, title: 4},
        ],
        workingTale: {
          content: [],
          added_as_favorite: true,
        },
      },
      AuthReducer: {
        session_token: '123',
      },
      configTalesReducer: {
        dotsIsActivated: false,
        dotsPerLine: 3,
        sizeOfDots: 10,
      },
    });

    cleanup; // método que borra todos los render();
  });

  it('<TaleView /> render', async () => {
    const component = (
      <NavAndReduxWrapper store={store}>
        <TalesStack />
      </NavAndReduxWrapper>
    );

    const {findByText, findByTestId, findAllByTestId} = render(component);

    // Entering in: <TaleView />:
    const click_to_go_to_taleview = await findAllByTestId('daily_tale_element'); // return an array with 4 tale buttons
    fireEvent(click_to_go_to_taleview[0], 'press');

    const dots_text = await findByText('Puntos de guía');
    const title_text = await findByTestId('tale_title');
    const remove_favorite_text = await findByText('Borrar favorito');
    const go_questions_button = await findByText('Ir a preguntas');

    expect(dots_text).toBeDefined();
    expect(title_text).toBeDefined();
    expect(remove_favorite_text).toBeDefined();
    expect(go_questions_button).toBeDefined();
  });

  it('snapshot <TaleView />', async () => {
    const navContext = {
      isFocused: () => true,
      // addListener returns an unscubscribe function.
      addListener: jest.fn(() => jest.fn()),
    };

    const route_params = {
      params: {comeFromFavorites: true},
    };

    const tree = renderer
      .create(
        <NavigationContext.Provider value={navContext}>
          <Provider store={store}>
            <TaleView navigation={navigation} route={route_params} />
            <IndexItem taleContentList="example test" />
          </Provider>
        </NavigationContext.Provider>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
