import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, Modal, Alert, TouchableOpacity} from 'react-native';
import {View, Text} from 'dripsy';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {buttonStyle} from '../../../../../globalStyles/globalStyles';

import DropDownPicker from 'react-native-dropdown-picker';
//
//redux:
import {useSelector, useDispatch} from 'react-redux';
import {setDotsPerLineAction} from './../../../../../../redux/actions/configTalesActions';

const DotsDropDown = () => {
  // bringing states and actions from redux:
  const configTalesState = useSelector((state) => state.configTalesReducer);
  const dispatch = useDispatch();

  return (
    <View style={{width: 80}}>
      <DropDownPicker
        items={[
          {
            label: '1',
            value: 1,
          },
          {
            label: '2',
            value: 2,
          },
          {
            label: '3',
            value: 3,
          },
          {
            label: '4',
            value: 4,
          },
        ]}
        defaultValue={configTalesState.dotsPerLine}
        containerStyle={{height: 40}}
        style={{backgroundColor: '#fafafa'}}
        itemStyle={{
          justifyContent: 'flex-start',
        }}
        dropDownStyle={{backgroundColor: '#fafafa'}}
        onChangeItem={(item) => {
          dispatch(setDotsPerLineAction(item.value));
        }}
      />
    </View>
  );
};

export default DotsDropDown;
