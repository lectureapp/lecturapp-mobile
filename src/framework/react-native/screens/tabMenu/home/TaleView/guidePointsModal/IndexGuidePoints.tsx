import React, {useState} from 'react';
import {StyleSheet, Image, Modal, Alert, TouchableOpacity} from 'react-native';
import {View, Text} from 'dripsy';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {buttonStyle} from '../../../../../globalStyles/globalStyles';
import DotsDropDown from './DotsDropDown';
import MyCheckBox from './MyCheckBox';

//redux:
import {useSelector} from 'react-redux';
import SizeDropDown from './SizeDropDown';

const IndexGuidePointsModal = ({pointsModalVisible, setPointsModalVisible}) => {
  // bringing states and actions from redux:
  const configTalesState = useSelector((state) => state.configTalesReducer);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={pointsModalVisible}
      onRequestClose={() => {
        setPointsModalVisible(!pointsModalVisible);
      }}
      style={{
        backgroundColor: 'white',
        marginVertical: 200,
        borderRadius: 8,
        alignItems: 'stretch',
        justifyContent: 'center',
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text variant="primary">Configuración de puntos de guía</Text>
          <View style={{flexDirection: 'row'}}>
            <Text variant="normal">Activar puntos de guía:</Text>
            <MyCheckBox />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text variant="normal">Puntos por fila:</Text>
            <DotsDropDown style={{elevation: 10}} />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text variant="normal">Tamaño de los puntos:</Text>
            <SizeDropDown style={{elevation: 10}} />
          </View>
          <View style={{flexDirection: 'row', zIndex: 0, elevation: 0}}>
            <TouchableOpacity
              style={[buttonStyle, {width: 120, marginVertical: 50}]}
              onPress={() => setPointsModalVisible(!pointsModalVisible)}>
              <Text variant="secondary" style={{color: 'white'}}>
                Aceptar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    //justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    justifyContent: 'space-between',
    marginHorizontal: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default IndexGuidePointsModal;
