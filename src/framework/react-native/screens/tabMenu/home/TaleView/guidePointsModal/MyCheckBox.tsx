import React, {useState, useEffect} from 'react';
//import {View, Text} from 'dripsy';

import CheckBox from 'react-native-check-box';
//
//redux:
import {useSelector, useDispatch} from 'react-redux';
import {changeDotsActivated} from './../../../../../../redux/actions/configTalesActions';

const MyCheckBox = () => {
  // bringing states and actions from redux:
  const configTalesState = useSelector(
    (state: any) => state.configTalesReducer,
  );
  const dispatch = useDispatch();

  const [isChecked, setIsChecked] = useState(configTalesState.dotsIsActivated);

  return (
    <CheckBox
      style={{justifyContent: 'center'}}
      onClick={() => {
        setIsChecked(!isChecked);
        dispatch(changeDotsActivated());
      }}
      isChecked={isChecked}
    />
  );
};

export default MyCheckBox;
