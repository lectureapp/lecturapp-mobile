import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, Modal, Alert, TouchableOpacity} from 'react-native';
import {View, Text} from 'dripsy';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {buttonStyle} from '../../../../../globalStyles/globalStyles';

import DropDownPicker from 'react-native-dropdown-picker';
//
//redux:
import {useSelector, useDispatch} from 'react-redux';
import {changeSizeDots} from './../../../../../../redux/actions/configTalesActions';

const SizeDropDown = () => {
  // bringing states and actions from redux:
  const configTalesState = useSelector((state) => state.configTalesReducer);
  const dispatch = useDispatch();

  return (
    <View style={{width: 80}}>
      <DropDownPicker
        items={[
          {
            label: '5',
            value: 5,
          },
          {
            label: '10',
            value: 10,
          },
          {
            label: '15',
            value: 15,
          },
          {
            label: '20',
            value: 20,
          },
        ]}
        defaultValue={configTalesState.sizeOfDots}
        containerStyle={{height: 40}}
        style={{backgroundColor: '#fafafa'}}
        itemStyle={{
          justifyContent: 'flex-start',
        }}
        dropDownStyle={{backgroundColor: '#fafafa'}}
        onChangeItem={(item) => {
          console.log('AQUÍ: ', configTalesState);
          dispatch(changeSizeDots(item.value));
        }}
      />
    </View>
  );
};

export default SizeDropDown;
