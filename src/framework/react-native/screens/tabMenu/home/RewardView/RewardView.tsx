import React, {useState, useEffect} from 'react';
import {Button, TouchableOpacity, Image} from 'react-native';
import YouTube from 'react-native-youtube';
import {View, Text} from 'dripsy';
import {buttonStyle} from '../../../../globalStyles/globalStyles';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {set24hBooleanToFalse} from '../../../../../redux/actions/talesActions';

interface RewardViewProps {}

const RewardView: React.FC<RewardViewProps> = ({navigation}: any) => {
  const [isReady, setIsReady] = useState(false);

  let talesState = useSelector((state: any) => state.talesReducer);
  let availableVideosState = useSelector(
    (state: any) => state.availableVideosReducer,
  );

  const [idUrl, setIdUrl] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    setIdUrl(
      // el siguiente código extrae el id de la url del video:
      availableVideosState.current_video_to_show._url.substring(
        17,
        availableVideosState.current_video_to_show._url.length,
      ),
    );
    // console.log('24hBoolean: ', talesState.resolved_less_than_24h_ago);
    return () => {
      dispatch(set24hBooleanToFalse());
    };
  }, []);

  return (
    <View style={{flex: 1}}>
      {talesState.num_of_correct_answers >=
      talesState.num_of_incorrect_answers ? (
        <>
          {talesState.resolved_less_than_24h_ago ? (
            <Text variant="primary">¡Muy bien! terminaste el cuento.</Text>
          ) : (
            <Text variant="primary">¡Muy bien! terminaste el cuento.</Text>
          )}

          <YouTube
            apiKey="AIzaSyCPIfNmLKPkYi14dZdfSA4qfUGBrrvvjtI"
            videoId={idUrl} // The YouTube video ID
            //play // control playback of video with true/false
            //fullscreen // control whether the video should play in fullscreen or inline
            loop // control whether the video should loop when ended
            onReady={(e) => setIsReady(true)}
            style={{alignSelf: 'stretch', height: 300}}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              margin: 35,
            }}>
            {talesState.resolved_less_than_24h_ago ? (
              <Text variant="secondary">Ya habías resuelto este cuento :)</Text>
            ) : (
              <>
                <Text variant="secondary" style={{alignSelf: 'center'}}>
                  Ganaste 3 monedas
                </Text>
                <Image
                  source={require('../../../../../../domain/assets/images/monedas.png')}
                  style={{resizeMode: 'stretch', width: 100, height: 100}}
                />
              </>
            )}
          </View>
        </>
      ) : (
        <>
          <Text variant="primary" style={{marginTop: 40, marginHorizontal: 30}}>
            Lo siento, vuelve a intentarlo.
          </Text>
          <MaterialCommunityIcons
            name="emoticon-sad-outline"
            size={200}
            color="green"
            style={{margin: 5, alignSelf: 'center'}}
          />
        </>
      )}
      <TouchableOpacity
        style={buttonStyle}
        onPress={() => {
          navigation.navigate('TabMenuContainer');
        }}>
        <Text variant="secondary" style={{color: 'white'}}>
          Terminar
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default RewardView;
