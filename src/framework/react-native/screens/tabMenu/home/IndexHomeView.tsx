import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

interface HomeViewProps {}
import {buttonColor} from '../../../globalStyles/globalStyles';
import {setCurrentFoxComment} from '../../../../redux/actions/foxCommentsActions';

// REDUX
import {useDispatch, useSelector} from 'react-redux';

const device_dimensions = Dimensions.get('window');

const IndexHomeView: React.FC<HomeViewProps> = ({navigation}: any) => {
  const clothesState = useSelector((state: any) => state.clothesReducer);

  const talesState = useSelector((state: any) => state.talesReducer); // temp

  const foxCommentsState = useSelector(
    (state: any) => state.foxCommentsReducer,
  );

  // Determinando que ropa mostrar:
  let imagePath: any = getClothAndAvatarPosition(clothesState);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setCurrentFoxComment());
  }, []);

  return (
    <View style={{flex: 1}}>
      <Image
        style={Styles.fondo}
        source={require('../../../../../domain/assets/images/fondo1.png')}
      />
      <ImageBackground
        style={Styles.dialogBox}
        source={require('../../../../../domain/assets/images/mensajeVacio.png')}>
        <Text style={Styles.message}>
          {foxCommentsState.current_fox_comment}
        </Text>
      </ImageBackground>
      <Image style={Styles.mascota} source={imagePath} />

      <TouchableOpacity
        onPress={() => navigation.navigate('TalesStack')}
        style={Styles.button}>
        <Text style={{fontSize: 20, color: 'white'}}>Cuentos Diarios</Text>
      </TouchableOpacity>
    </View>
  );
};

const getClothAndAvatarPosition = (clothesState: any) => {
  const randomNumber: number = Math.random();
  if (clothesState.current_cloth == 'DEFAULT') {
    if (randomNumber <= 0.33)
      return require('../../../../../domain/assets/images/clothing/naked/zorro_normal_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../domain/assets/images/clothing/naked/zorro_normal_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../domain/assets/images/clothing/naked/zorro_normal_pose2.png');
  } else if (clothesState.current_cloth == 'COWBOY') {
    if (randomNumber <= 0.33)
      return require('../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../domain/assets/images/clothing/cowboy/zorro_vaquero_pose3.png');
  }
  //
  else if (clothesState.current_cloth == 'TUXEDO') {
    if (randomNumber <= 0.33)
      return require('../../../../../domain/assets/images/clothing/suit/zorro_terno_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../domain/assets/images/clothing/suit/zorro_terno_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../domain/assets/images/clothing/suit/zorro_terno_pose2.png');
    //
  } else if (clothesState.current_cloth == 'ASTRONAUT') {
    if (randomNumber <= 0.33)
      return require('../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose1.png');
    else if (randomNumber <= 0.66)
      return require('../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose2.png');
    else if (randomNumber > 0.66)
      return require('../../../../../domain/assets/images/clothing/spaceman/zorro_astronauta_pose3.png');
  } else
    return require('../../../../../domain/assets/images/clothing/naked/zorro_normal_pose1.png');
  // ---------------------------------------------------
};

const Styles = StyleSheet.create({
  fondo: {
    position: 'absolute',
    flex: 1,
    margin: 0,
    width: device_dimensions.width,
    height: device_dimensions.height - 120,
  },
  mascota: {
    height: 220,
    width: 250,
    resizeMode: 'contain',
  },
  dialogBox: {
    width: 230,
    height: 200,
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  message: {
    width: 180,
    fontSize: 17,
    color: 'black',
    marginBottom: 30,
    marginLeft: 20,
  },
  button: {
    borderRadius: 15,
    backgroundColor: buttonColor,
    alignItems: 'center',
    marginTop: 30,
    padding: 10,
    marginHorizontal: 70,
  },
});

export default IndexHomeView;
