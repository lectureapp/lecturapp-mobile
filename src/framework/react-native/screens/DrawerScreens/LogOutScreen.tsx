import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {logout} from '../../../redux/actions/AuthActions';

//REDUX:
import {useDispatch} from 'react-redux';

const LogOutScreen = ({navigation}) => {
  // Declaración de una variable de estado que llamaremos "count"  const [count, setCount] = useState(0);

  const dispatch = useDispatch();

  return (
    <View>
      <TouchableOpacity
        style={{
          borderRadius: 15,
          backgroundColor: '#368BFB',
          alignItems: 'center',
          marginTop: 30,
          padding: 10,
          marginHorizontal: 70,
        }}
        onPress={() => {
          navigation.navigate('LoginView');
          // console.log('cerrando sesión');
          dispatch(logout());
        }}>
        <Text>Cerrar sesión</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LogOutScreen;
