import React, {useState} from 'react';
import {
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Text, View} from 'dripsy';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/FontAwesome';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';

const windowHeight = Dimensions.get('window').height;

const IndexDrawerNavigator = (props) => {
  const AuthState = useSelector((state: any) => state.AuthReducer);

  return (
    <DrawerContentScrollView>
      <ImageBackground
        style={styles.container}
        source={require('../../../../domain/assets/images/fondoDrawer.png')}>
        <View style={styles.drawerItemList}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('ProfileView', {});
            }}>
            <Icon name="user" size={80} color="black" style={styles.icon} />
            <Text variant="secondary">{AuthState.account_name}</Text>
          </TouchableOpacity>
          <DrawerItemList style={styles.drawerItemList} {...props} />
        </View>
      </ImageBackground>
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: -10,
    flex: 1,
    height: windowHeight,
  },
  drawerItemList: {
    justifyContent: 'center',
    flex: 1,
  },
  icon: {
    alignSelf: 'center',
  },
});

export default IndexDrawerNavigator;
