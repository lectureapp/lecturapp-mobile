import React, {useState} from 'react';
import {
  Button,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  StyleSheet,
} from 'react-native';
import {View, Text} from 'dripsy';
import {logout} from '../../../redux/actions/AuthActions';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';

interface ProfileViewProps {}

const ProfileView: React.FC<ProfileViewProps> = ({navigation}: any) => {
  const AuthState = useSelector((state: any) => state.AuthReducer);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        marginVertical: 50,
      }}>
      <Image
        source={require('../../../../domain/assets/images/perfil_img2.png')}
      />

      <Text style={{marginTop: 30, fontSize: 20}}>
        Usuario: {AuthState.account_name}
      </Text>
      <Text style={{marginTop: 30, fontSize: 20}}>
        Email: {AuthState.email}
      </Text>
      {/*
        <Text style={{marginTop: 30, fontSize: 20}}>Niños registrados:</Text>
        <Text variant="primary" style={{color: 'red'}}>
          TODO: Definir bien cual será el flujo de esta vista.
        </Text>
        <FlatList
          data={listaNiños}
          renderItem={({item}) => (
            <Text style={{fontSize: 20}}>{item.nombre}</Text>
          )}
          contentContainerStyle={{paddingBottom: 50}}
          keyExtractor={(item) => item.id}
        />
      */}
      <TouchableOpacity
        style={{
          borderRadius: 15,
          backgroundColor: '#368BFB',
          alignItems: 'center',
          marginTop: 30,
          padding: 10,
          // marginHorizontal: 70,
        }}
        onPress={() => {
          // navigation.navigate('LoginView');
          // dispatch(logout());
          setModalVisible(!modalVisible);
        }}>
        <Text variant="normal" style={{color: 'white'}}>
          Cerrar sesión
        </Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <>
              <Text style={styles.modalText}>¿Deseas cerrar la sesión?</Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <TouchableOpacity
                  style={[styles.button2, styles.buttonClose]}
                  onPress={() => {
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Cancelar</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.button2, styles.buttonClose]}
                  onPress={() => {
                    dispatch(logout());

                    navigation.navigate('LoginView');
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>¡Cerrar sesión!</Text>
                </TouchableOpacity>
              </View>
            </>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  // MODAL:
  // -----------------------------------------------------
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    paddingHorizontal: 25,
    margin: 15,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 15,
  },
});

export default ProfileView;
