import React from 'react';
import {View, Text, Button} from 'react-native';

interface StadisticsRateViewProps {}

const StadisticsRateView: React.FC<StadisticsRateViewProps> = ({navigation}: any) => {
  return (
    <View>
      <Text>StadisticsRateView</Text>
      <Button title="Ir a Home" onPress={() => navigation.navigate("TabMenuContainer")}/>
    </View>
  );
};

export default StadisticsRateView;
