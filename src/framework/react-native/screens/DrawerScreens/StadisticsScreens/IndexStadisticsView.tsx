import React, {useEffect} from 'react';
import {
  StyleSheet,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

// Chart Component
//import StackedBarChart from '../../../components/ChartsComponents/Stackedbarchart';

// Datos de prueba
//import * as datos from '../../../data/datos_stadistics.json'
import * as datos from '../../../../../domain/assets/data/datos_stadistics.json';

import {Text, View} from 'dripsy';
//import {buttonStyle} from '../../../globalStyles/globalStyles';

// REDUX:
import {useSelector, useDispatch} from 'react-redux';
import {getStadistics} from '../../../../redux/actions/stadisticsActions';

interface StadisticsViewProps {}

//const screenWidth = Dimensions.get('window').width;

//const chartConfig = {
//// backgroundGradientFrom: "#FFB500",
//backgroundGradientFromOpacity: 0,
//// backgroundGradientTo: "#000",
//backgroundGradientToOpacity: 0.1,
//color: (opacity = 1) => `rgba(0, 0, 0,${opacity})`,
//barPercentage: 1,
//useShadowColorFromDataset: false, // optional
//style: {
//borderRadius: 16,
//marginVertical: 8,
//},
//};

let fechas = datos.datos_stadistics.map((obj) => {
  let obj_registro_lecturas = obj.registro_lecturas;
  let dia = obj_registro_lecturas.map((d) => {
    return d.fecha;
  });

  return dia;
});

function porcentaje(cuentos: number, preguntas: number, aciertos: number) {
  let porcentaje = (aciertos * 100) / preguntas;
  let rpta = (cuentos * porcentaje) / 100;
  return Number(rpta.toFixed(2));
}

let cuentos_por_dia = [];
let porcentaje_dia = [];
let dia: number;
let i: number;

for (dia = 0; dia < 7; dia++) {
  let cuentos_al_dia =
    datos.datos_stadistics[0].registro_lecturas[dia].cuentos_leidos.length;
  let obj_cuentos =
    datos.datos_stadistics[0].registro_lecturas[dia].cuentos_leidos;
  let suma_aciertos = 0;
  for (i = 0; i < obj_cuentos.length; i++) {
    suma_aciertos = suma_aciertos + obj_cuentos[i].aciertos;
  }
  let suma_preguntas = 0;
  for (i = 0; i < obj_cuentos.length; i++) {
    suma_preguntas = suma_preguntas + obj_cuentos[i].preguntas;
  }

  let final = porcentaje(cuentos_al_dia, suma_preguntas, suma_aciertos);

  cuentos_por_dia.push(cuentos_al_dia);
  porcentaje_dia.push(final);
}

let resp_final = [];
let d: number;

for (d = 0; d < 7; d++) {
  resp_final.push([cuentos_por_dia[d], porcentaje_dia[d]]);
}

const data = {
  labels: fechas[0],
  data: resp_final,
  barColors: ['#4da1ff', '#03751a'],
};

const IndexStadisticsView: React.FC<StadisticsViewProps> = ({
  navigation,
}: any) => {
  const dispatch = useDispatch();
  const stadisticsState = useSelector((state: any) => state.stadisticsReducer);

  useEffect(() => {
    dispatch(getStadistics());
  }, []);

  //today_tales_readed: action.payload.today_tales_readed,
  //hit_percentaje_today: action.payload.hit_percentaje_today,

  //hit_percentaje_week: action.payload.hit_percentaje_week,
  //week_tales_readed: action.payload.week_tales_readed,
  //total_tales: action.payload.total_tales,

  return (
    <View style={styles.container}>
      <Text variant="primary">Estadisticas</Text>
      <View style={styles.subContainer}>
        <View style={styles.horizontalContainer}>
          <Text variant="normal">Cuentos leídos hoy: </Text>
          <Text variant="normal" style={{color: 'green'}}>
            {stadisticsState.today_tales_readed}
          </Text>
        </View>
        <View style={styles.horizontalContainer}>
          <Text variant="normal">Porcentaje de aciertos hoy: </Text>
          <Text variant="normal" style={{color: 'green'}}>
            {stadisticsState.hit_percentaje_today
              ? stadisticsState.hit_percentaje_today + '%'
              : '0%'}
          </Text>
        </View>
      </View>
      <View style={styles.subContainer}>
        <View style={styles.horizontalContainer}>
          <Text variant="normal">Cuentos leídos esta semana: </Text>
          <Text variant="normal" style={{color: 'green'}}>
            {stadisticsState.week_tales_readed}
          </Text>
        </View>
        <View style={styles.horizontalContainer}>
          <Text variant="normal">Porcentaje de aciertos de la semana: </Text>
          <Text variant="normal" style={{color: 'green'}}>
            {stadisticsState.hit_percentaje_week}%
          </Text>
        </View>
      </View>
      <View style={styles.horizontalContainer}>
        <Text variant="normal">Cuentos leídos en total: </Text>
        <Text variant="normal" style={{color: 'green'}}>
          {stadisticsState.total_tales}
        </Text>
      </View>
    </View>
    //<StackedBarChart
    //style={styles.graph}
    //data={data}
    //width={screenWidth - 10}
    //height={300}
    //chartConfig={chartConfig}
    //withVerticalLabels
    //withHorizontalLabels
    //navigation={navigation}
    ///>
    //<View style={styles.container_buttons}>
    //<TouchableOpacity style={[buttonStyle, styles.button]}>
    //<Text style={{color: 'white'}}>Semana Anterior</Text>
    //</TouchableOpacity>
    //<TouchableOpacity style={[buttonStyle, styles.button]}>
    //<Text style={{color: 'white'}}>Semana Anterior</Text>
    //</TouchableOpacity>
    //</View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    //justifyContent: 'space-evenly',
  },
  subContainer: {
    borderWidth: 3,
    borderRadius: 12,
    margin: 5,
    width: 350,
  },
  horizontalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignSelf: 'stretch',
  },

  //----------------------------------------------------------------
  //Configuraciones para los items comentados:
  //----------------------------------------------------------------
  graph: {
    borderWidth: 2,
  },
  button: {
    height: 50,
  },
  container_buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});

export default IndexStadisticsView;
