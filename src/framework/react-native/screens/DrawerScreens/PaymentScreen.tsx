import React, {useState} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';

import {GooglePay} from 'react-native-google-pay';

const allowedCardNetworks = ['VISA', 'MASTERCARD'];
const allowedCardAuthMethods = ['PAN_ONLY', 'CRYPTOGRAM_3DS'];

const requestData = {
  cardPaymentMethod: {
    tokenizationSpecification: {
      type: 'PAYMENT_GATEWAY',
      // stripe (see Example):
      gateway: 'stripe',
      gatewayMerchantId: '',
      stripe: {
        publishableKey: 'pk_test_TYooMQauvdEDq54NiTphI7jx',
        version: '2018-11-08',
      },
      // other:
      gateway: 'example',
      gatewayMerchantId: 'exampleGatewayMerchantId',
    },
    allowedCardNetworks,
    allowedCardAuthMethods,
  },
  transaction: {
    totalPrice: '10',
    totalPriceStatus: 'FINAL',
    currencyCode: 'PEN',
  },
  merchantName: 'Example Merchant',
};

// Set the environment before the payment request
GooglePay.setEnvironment(GooglePay.ENVIRONMENT_TEST);

const PaymentScreen = () => {
  // Check if Google Pay is available
  GooglePay.isReadyToPay(allowedCardNetworks, allowedCardAuthMethods).then(
    (ready) => {
      if (ready) {
        // Request payment token
        GooglePay.requestPayment(requestData)
          .then((token: string) => {
            // Send a token to your payment gateway
          })
          .catch((error) => console.log(error.code, error.message));
      }
    },
  );

  return (
    <View style={{alignItems: 'center'}}>
      <Image
        //source={require('../../assets/logo.png')}
        source={require('../../../../domain/assets/images/logo.png')}
        style={{
          width: 300,
          height: 300,
          resizeMode: 'stretch',
          alignSelf: 'center',
        }}
      />
      <Text
        style={{
          fontSize: 25,

          fontFamily: 'Satisfy-Regular',
        }}>
        Su hijo se lo agradecerá.
      </Text>
      <TouchableOpacity
        onPress={PaymentScreen}
        style={{
          backgroundColor: 'green',
          margin: 10,
          borderRadius: 20,
          padding: 20,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.3,
          shadowRadius: 7.65,
          elevation: 4,
        }}>
        <Text style={{fontSize: 25}}>Pagar</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PaymentScreen;
