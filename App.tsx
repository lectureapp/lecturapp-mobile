/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';

//import App2 from './src/navigation/MainStackContainer';
//import MainStackContainer from './src/framework/navigation/MainStackContainer';
import MainStackContainer from './src/framework/react-native/navigation/MainStackContainer';

// importaciones para redux:
import {Provider} from 'react-redux';

// importaciones para redux-persist
import {PersistGate} from 'redux-persist/integration/react';
//import {store, persistor} from './src/redux/store';
import {store, persistor} from './src/framework/redux/store';
import DripsyImplementation from './src/framework/dripsy/config';

const App = () => {
  return (
    <>
      <DripsyImplementation>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <MainStackContainer />
          </PersistGate>
        </Provider>
      </DripsyImplementation>
    </>
  );
};

export default App;
